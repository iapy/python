def test_sum_0(*args):
    return sum(args)

def test_sum_1(a, *args):
    return a + sum(args)
