#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(Set, python::tester::Test)

BOOST_AUTO_TEST_CASE_TEMPLATE(ReturnIntegral, T, python::tester::Integers)
{
    auto mod = python::import("test_simple_set");
    BOOST_TEST(mod);

    auto get_int = python::getattr<std::set<T>(std::size_t)>(mod, "integers");
    BOOST_TEST(get_int);

    auto set = get_int(5);

    BOOST_TEST(5 == set.size());
    for(T i{0}; i < T{5}; ++i)
    {
        BOOST_TEST(set.contains(i));
    }
    for(T j{5}; j < T{10}; ++j)
    {
        BOOST_TEST(!set.contains(j));
    }

    auto make_set = [](T size) {
        std::set<T> result;
        for(T i{0}; i < size; ++i)
            result.insert(i);
        return result;
    };

    auto test_set = [&](T size) {
        auto const expected = make_set(size);

        std::set<T> v1;
        for(T const value : set)
        {
            v1.insert(value);
        }
        BOOST_TEST(v1 == expected);

        std::set<T> v2;
        for(auto it = std::begin(set); it != std::end(set); ++it)
        {
            v2.insert(*it);
        }
        BOOST_TEST(v2 == expected);
    };

    test_set(5);

    for(T j{5}; j < T{10}; ++j)
    {
        set.insert(j);
        BOOST_TEST(set.contains(j));
    }
    BOOST_TEST(10 == set.size());
    test_set(10);

    set.insert(T{2});
    BOOST_TEST(10 == set.size());
    test_set(10);

    BOOST_TEST(set.erase(T{2}));
    BOOST_TEST(9 == set.size());

    BOOST_TEST(set.erase(T{1}));
    BOOST_TEST(8 == set.size());
    BOOST_TEST(!set.erase(T{1}));

    set.clear();
    BOOST_TEST(0 == set.size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(CreateIntegral, T, python::tester::Integers)
{
    auto mod = python::import("test_simple_set");
    BOOST_TEST(mod);

    python::set<T> set1{T{0}, T{1}, T{2}, T{3}, T{4}};

    auto check_int = python::getattr<bool(std::set<T>)>(mod, "check_integers");
    BOOST_TEST(check_int);
    BOOST_TEST(check_int(set1));

    python::set<T> set2;
    set2.insert(T{2});
    set2.insert(T{0});
    set2.insert(T{1});
    BOOST_TEST(check_int(set2));
}

BOOST_AUTO_TEST_CASE(ReturnString)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_set");
    BOOST_TEST(mod);

    auto get_string = python::getattr<std::set<std::string>(std::size_t)>(mod, "strings");
    BOOST_TEST(get_string);

    auto set = get_string(3);

    BOOST_TEST(3 == set.size());
    for(std::size_t i = 0; i < 3; ++i)
    {
        BOOST_TEST(set.contains(std::to_string(i) + "-value"));
    }
    for(std::size_t i = 3; i < 10; ++i)
    {
        BOOST_TEST(!set.contains(std::to_string(i) + "-value"));
    }

    auto make_set = [](std::size_t size) {
        std::set<std::string> result;
        for(std::size_t i = 0; i < size; ++i)
            result.insert(std::to_string(i) + "-value");
        return result;
    };

    auto test_set = [&](std::size_t size) {
        auto const expected = make_set(size);

        std::set<std::string> v1;
        for(auto const value : set)
        {
            v1.insert(value);
        }
        BOOST_TEST(v1 == expected);

        std::set<std::string> v2;
        for(auto it = std::begin(set); it != std::end(set); ++it)
        {
            v2.insert(*it);
        }
        BOOST_TEST(v2 == expected);
    };

    test_set(3);
    set.insert("2-value"s);
    test_set(3);
    set.insert("3-value"s);
    test_set(4);
    set.insert("4-value"s);
    test_set(5);
    BOOST_TEST(set.erase("4-value"s));
    test_set(4);
    BOOST_TEST(!set.erase("4-value"s));
    test_set(4);

    set.clear();
    BOOST_TEST(0 == set.size());
}

BOOST_AUTO_TEST_CASE(CreateString)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_set");
    BOOST_TEST(mod);

    python::set<std::string> set1{"value-1"s, "value-0"s, "value-2"s};

    auto check_str = python::getattr<bool(std::set<std::string>)>(mod, "check_strings");
    BOOST_TEST(check_str);
    BOOST_TEST(check_str(set1));

    python::set<std::string> set2;
    set2.insert("value-2"s);
    set2.insert("value-0"s);
    set2.insert("value-1"s);
    BOOST_TEST(check_str(set2));
}

BOOST_AUTO_TEST_SUITE_END()
