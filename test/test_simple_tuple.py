def check_tuple_1(t):
    return len(t) == 3 and t[0] == 10 and t[1] == "foo" and t[2] == [1,2,3,4,5]

def check_tuple_2(t):
    return len(t) == 3 and t[0] == 42 and t[1] == "baz" and t[2] == [1,2,3,4]

def get_tuple_1():
    return (42, "baz")

def get_tuple_2():
    return (42, "baz", [1,2,3])
