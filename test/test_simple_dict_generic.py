class Test(object):
    def __init__(self, value = "I'm test"):
        self._value = value 

    def get_value(self):
        return self._value

    def set_value(self, value):
        self._value = value

def int_function():
    return 42

def int_function_int(a):
    return 42 + a

def str_function():
    return "Hello there"

def str_function_str(a):
    return "Hello " + a

def str_function_str_2(a):
    return "Aloha " + a

def make():
    return dict(
        int_value=-10,
        int_function=int_function,
        int_function_int=int_function_int,
        str_function=str_function,
        str_function_str=str_function_str,
        int_list=list(range(3)),
        test=Test()
    )

def test(d):
    return d['str_function_str']('coder') == 'Aloha coder' \
            and d['test'].get_value() == "Changed"  \
            and d['added'].get_value() == "Added" \
            and d['int_value'] == -246

