class Test(object):
    def __init__(self):
        self.entered = False
        self.exited = False

    def __enter__(self):
        self.entered = True
        return self

    def test(self, name, throw):
        if throw:
            raise Exception(name)

    def is_entered(self):
        value = self.entered
        self.entered = False
        return value

    def is_exited(self):
        value = self.exited
        self.exited = False
        return value

    def __exit__(self, a, b, c):
        self.exited = True
