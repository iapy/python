#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ExportState, python::tester::Metacg)
using namespace std::string_literals;

struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        struct State
        {
            BOOST_HANA_DEFINE_STRUCT(State,
                (std::string, value)
            );

            std::string get_value()
            {
                return value;
            }

            CONFIG_DECLARE_STATE_BASE_CONSTRUCTORS(State, value) {}
        };

        PYTHON_EXPORT(test_metacg_export_state_c);
        PYTHON_IMPORT(test_metacg_export_state, (
            (std::string(State *), get_value),
            (std::string(State *), get_value2)
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                mod.def<typename Base::State>()
                    .def("get_value", python::bind<&Base::State::get_value>)
                ;
            }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([&, this]{
                    auto mod = python::import(this);
                    typename Base::State &pystate = this->state;

                    BOOST_TEST("foobar"s == mod.get_value(&pystate));
                    BOOST_TEST("foobarfoobar"s == mod.get_value2(&pystate));
                });
                return 0;
            }
        };
    };

    template<typename Types>
    struct State : Types::State
    {
        CONFIG_DECLARE_STATE_BASE(Types::State) {}
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>(
        cg::Args<Component>(nlohmann::json{
            {"value", "foobar"}
        })
    );
}

BOOST_AUTO_TEST_SUITE_END()
