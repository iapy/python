class LT(object):
    n = 0

    def __init__(self, value):
        self._value = value

    def __lt__(self, other):
        LT.n += 1
        return self._value < other._value

    def lt_called(self):
        return LT.n

class GT(object):
    n = 0

    def __init__(self, value):
        self._value = value

    def __gt__(self, other):
        GT.n += 1
        return self._value > other._value

    def gt_called(self):
        return GT.n

class LE(object):
    n = 0

    def __init__(self, value):
        self._value = value

    def __le__(self, other):
        LE.n += 1
        return self._value <= other._value

    def le_called(self):
        return LE.n

class GE(object):
    n = 0

    def __init__(self, value):
        self._value = value

    def __ge__(self, other):
        GE.n += 1
        return self._value >= other._value

    def ge_called(self):
        return GE.n

class EQ(object):
    n = 0

    def __init__(self, value):
        self._value = value

    def __eq__(self, other):
        EQ.n += 1
        return self._value == other._value

    def eq_called(self):
        return EQ.n

class NE(object):
    n = 0

    def __init__(self, value):
        self._value = value

    def __ne__(self, other):
        NE.n += 1
        return self._value != other._value

    def ne_called(self):
        return NE.n

class NEQ(object):
    m, n = 0, 0

    def __init__(self, value):
        self._value = value

    def __eq__(self, other):
        NEQ.m += 1
        return self._value == other._value

    def __ne__(self, other):
        NEQ.n += 1
        return self._value != other._value

    def eq_called(self):
        return NEQ.m
    def ne_called(self):
        return NEQ.n
