#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(Dict, python::tester::Test)
static bool is_key_error(python::exception const &ex)
{
    return ex.type() == "KeyError";
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ReturnIntegral, T, python::tester::Integers)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_dict");
    BOOST_TEST(mod);

    auto get_int = python::getattr<std::map<T, std::string>(std::size_t)>(mod, "integers");
    BOOST_TEST(get_int);

    auto ints = get_int(3);
    BOOST_TEST(ints);
    BOOST_TEST(ints[0] == "0-test"sv);
    BOOST_TEST("1-test"sv == ints[1]);
    BOOST_TEST(ints[2] == "2-test"sv);
    BOOST_TEST(!ints[3]);

    auto check = [&ints](T elements) {
        std::size_t i{0};
        BOOST_TEST(elements == ints.size());
        for(auto [k, v] : ints)
        {
            if(k < elements) BOOST_TEST(v == std::to_string(k) + "-test");
            else BOOST_TEST(false, "Unexpected key " << k << " max " << elements);
            ++i;
        }
        BOOST_TEST(i == ints.size());

        i = 0;
        for(auto it = std::begin(ints); it != std::end(ints); ++it)
        {
            if((*it).first < elements) BOOST_TEST((*it).second == std::to_string((*it).first) + "-test");
            else BOOST_TEST(false, "Unexpected key " << (*it).first << " max " << elements);
            ++i;
        }
        BOOST_TEST(i == ints.size());

        i = 0;
        for(auto it = std::begin(ints); it != std::end(ints); it++)
        {
            if((*it).first < elements) BOOST_TEST((*it).second == std::to_string((*it).first) + "-test");
            else BOOST_TEST(false, "Unexpected key " << (*it).first << " max " << elements);
            ++i;
        }
        BOOST_TEST(i == ints.size());
    };

    check(3);
    ints.set(T{3}, "3-test"s);
    check(4);
    ints.set(T{3}, "3-test"s);
    check(4);
    ints.set(T{4}, "4-test"s);
    check(5);
    ints.erase(T{4});
    check(4);
    BOOST_REQUIRE_EXCEPTION(ints.erase(T{4}), python::exception, is_key_error);

    ints.clear();
    BOOST_TEST(0 == ints.size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(CreateIntegral, T, python::tester::Integers)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_dict");
    BOOST_TEST(mod);

    auto check_int = python::getattr<bool(std::map<std::string, T>)>(mod, "check_integers");
    BOOST_TEST(check_int);

    auto dict1 = python::dict<std::string, T>();
    dict1.set("0-value"s, T{0});
    dict1.set("1-value"s, T{1});

    BOOST_TEST(check_int(dict1));

    dict1.set("2-value"s, T{2});
    BOOST_TEST(check_int(dict1));

    dict1.erase("2-value"s);
    BOOST_TEST(check_int(dict1));

    dict1.clear();
    BOOST_TEST(0 == dict1.size());

    auto dict2 = python::dict<std::string, T>{
        {"2-value"s, T{2}},
        {"1-value"s, T{1}},
        {"3-value"s, T{3}},
        {"0-value"s, T{0}}
    };

    BOOST_TEST(check_int(dict2));

    dict2.erase("3-value"s);
    BOOST_TEST(check_int(dict2));

    dict2.set("3-value"s, T{3});
    BOOST_TEST(check_int(dict2));

    auto dict3 = python::dict<std::string, T>{std::map<std::string, T>{
        {"2-value"s, T{2}},
        {"1-value"s, T{1}},
        {"0-value"s, T{0}}
    }};

    BOOST_TEST(check_int(dict3));

    dict3.update(dict2);
    BOOST_TEST(check_int(dict3));
    BOOST_TEST(4 == dict3.size());

    dict2.erase("2-value"s);
    BOOST_TEST(check_int(dict3));

    dict2.set("2-value"s, T{2});
    BOOST_TEST(check_int(dict3));
}

BOOST_AUTO_TEST_CASE(ReturnString)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_dict");
    BOOST_TEST(mod);

    auto get_str = python::getattr<std::map<std::string, std::string>(std::size_t)>(mod, "strings");
    BOOST_TEST(get_str);

    auto strings = get_str(4);
    BOOST_TEST(strings);
    BOOST_TEST(4 == strings.size());
    BOOST_TEST("0-value"sv == strings["0-key"sv]);
    BOOST_TEST(strings["1-key"sv] == "1-value"sv);
    BOOST_TEST("2-value"sv == strings["2-key"s]);
    BOOST_TEST(strings["3-key"s] == "3-value"sv);

    auto check = [&](std::size_t N) {
        std::map<std::string, std::string> copy;
        std::copy(std::begin(strings), std::end(strings), std::inserter(copy, copy.end()));

        std::map<std::string, std::string> m;
        for(std::size_t i = 0; i < N; ++i)
        {
            m[std::to_string(i) + "-key"] = std::to_string(i) + "-value";
        }
        BOOST_TEST(copy == m);
    };

    check(4);
    strings.set("4-key"s, "4-value"s);
    check(5);
    strings.set("4-key"s, "4-value"s);
    check(5);
    strings.set("5-key"s, "5-value"s);
    check(6);
    strings.erase("5-key"s);
    check(5);
    BOOST_REQUIRE_EXCEPTION(strings.erase("5-key"s), python::exception, is_key_error);

    strings.clear();
    BOOST_TEST(0 == strings.size());
}

BOOST_AUTO_TEST_CASE(CreateString)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_dict");
    BOOST_TEST(mod);

    auto check_str = python::getattr<bool(std::map<std::string, std::string>)>(mod, "check_strings");
    BOOST_TEST(check_str);

    auto dict1 = python::dict<std::string, std::string>();
    dict1.set("0-key"s, "0-value"s);
    dict1.set("1-key"s, "1-value"s);
    BOOST_TEST(check_str(dict1));

    dict1.set("2-key"s, "2-value"s);
    BOOST_TEST(check_str(dict1));

    dict1.erase("2-key"s);
    BOOST_TEST(check_str(dict1));

    dict1.clear();
    BOOST_TEST(0 == dict1.size());

    auto dict2 = python::dict<std::string, std::string>{
        {"2-key"s, "2-value"s},
        {"1-key"s, "1-value"s},
        {"3-key"s, "3-value"s},
        {"0-key"s, "0-value"s}
    };

    BOOST_TEST(check_str(dict2));

    dict2.erase("3-key"s);
    BOOST_TEST(check_str(dict2));

    dict2.set("3-key"s, "3-value"s);
    BOOST_TEST(check_str(dict2));

    dict2.clear();
    BOOST_TEST(0 == dict2.size());

    auto dict3 = python::dict<std::string, std::string>{std::map<std::string, std::string>{
        {"1-key"s, "1-value"s},
        {"0-key"s, "0-value"s}
    }};

    BOOST_TEST(check_str(dict3));

    dict3.erase("1-key"s);
    BOOST_TEST(check_str(dict3));

    dict3.set("1-key"s, "1-value"s);
    BOOST_TEST(check_str(dict3));

    dict3.update(python::dict<std::string, std::string>{
        {"1-key"s, "1-value"s},
        {"3-key"s, "3-value"s},
        {"2-key"s, "2-value"s}
    });
    BOOST_TEST(check_str(dict3));
    BOOST_TEST(4 == dict3.size());

    dict3.clear();
    BOOST_TEST(0 == dict3.size());
}

BOOST_AUTO_TEST_SUITE_END()
