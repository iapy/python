#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ExportClassContainer, python::tester::Metacg)
using namespace std::string_literals;

struct Component : cg::Component
{
    static int count;

    template<typename>
    struct Types
    {
        class T
        {
        public:
            T(int v) : v{v} { ++count; }
            T(T &&t) : v{t.v} { ++count; }
            T(T const &t) : v{t.v} { ++count; }
            ~T() { --count; }

            int v;
        };

        PYTHON_EXPORT(test_metacg_export_class_container_c);
        PYTHON_IMPORT(test_metacg_export_class_container, (
            (int(int), sum_values)
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                mod.def<typename Base::T>()
                    .def("v", python::bind<&Base::T::v>)
                ;
                mod.def("make", python::bind<&Interface<Base>::make>);
            }

            python::object make(int count)
            {
                using namespace python;
                list<obj> result;
                for(int i = 0; i < count; ++i)
                {
                    result.append(native( std::make_unique<typename Base::T>(i + 1) ));
                }
                return result;
            }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([&, this]{
                    auto mod = python::import(this);
                    BOOST_TEST(55 == mod.sum_values(10));
                    BOOST_TEST(66 == mod.sum_values(11));
                    BOOST_TEST(78 == mod.sum_values(12));
                    BOOST_TEST(91 == mod.sum_values(13));
                });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

int Component::count{0};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
    BOOST_TEST(0 == Component::count);
}

BOOST_AUTO_TEST_SUITE_END()
