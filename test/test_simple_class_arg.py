class A(object):
    def __init__(self, value):
        self._value = value

    def get_value(self):
        return self._value

def get_value(a):
    return a.get_value()
