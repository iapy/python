#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(Export, python::tester::Metacg)
using namespace std::string_literals;

struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_EXPORT(test_metacg_export_c);
        PYTHON_IMPORT(test_metacg_export, (
            (std::string(), sum),
            (std::string(), get)
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                using Self = Interface<Base>;
                mod.def("bar", python::bind<&Self::bar>);
                mod.def("foo", python::bind<&Self::foo>);
                mod.def("baz", python::bind<&Self::baz>);
            }

            std::string bar() { return this->state.bar; }
            std::string foo() { return this->state.foo; }
            std::string baz()
            {
                return python::import(this).get();
            }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                this->state.bar = "Bar";
                this->state.foo = "Foo";
                python::lock([module=python::import(this)]{
                    BOOST_TEST("BarFoo_"s == module.sum());
                });

                this->state.bar = "X";
                this->state.foo = "Y";
                python::lock([module=python::import(this)]{
                    BOOST_TEST("XY_"s == module.sum());
                });
                return 0;
            }
        };
    };

    template<typename>
    struct State
    {
        std::string bar;
        std::string foo;
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
}

BOOST_AUTO_TEST_SUITE_END()
