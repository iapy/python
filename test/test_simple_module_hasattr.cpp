#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(Hasattr, python::tester::Test)
static bool check_exception(python::exception const &ex)
{
    return ex.type() == "AttributeError";
}

BOOST_AUTO_TEST_CASE(Module)
{
    using namespace std::string_literals;

    auto mod = python::import("test_simple_module_hasattr");

    BOOST_TEST(python::hasattr(mod, "main"));
    BOOST_TEST(0 == python::getattr<int()>(mod, "main")());

    BOOST_TEST(!python::hasattr(mod, "get"));
    BOOST_REQUIRE_EXCEPTION(python::getattr<int()>(mod, "get"), python::exception, check_exception);
}

BOOST_AUTO_TEST_SUITE_END()

