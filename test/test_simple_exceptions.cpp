#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(Exceptions, python::tester::Test)

static bool check_exception(python::exception const &ex)
{
    return ex.type() == "Exception" && ex.what() == "Exception('Sample exception')";
}

static bool check_import(python::exception const &ex)
{
    return ex.type() == "ModuleNotFoundError" && ex.what() == R"(ModuleNotFoundError("No module named 'undefined_module'"))";
}

static bool check_attribute(python::exception const &ex)
{
    return ex.type() == "AttributeError";
}

BOOST_AUTO_TEST_CASE(TryCatchException)
{
    auto mod = python::import("test_simple_exceptions");
    BOOST_TEST(mod);

    using namespace std::string_literals;

    auto test = python::getattr<int(std::string)>(mod, "test");
    BOOST_TEST(test);
    BOOST_REQUIRE_EXCEPTION(test("Sample exception"s), python::exception, check_exception);
}

BOOST_AUTO_TEST_CASE(ModuleNotFoundError)
{
    BOOST_REQUIRE_EXCEPTION(python::import("undefined_module"), python::exception, check_import);
}

BOOST_AUTO_TEST_CASE(AttributeError)
{
    auto mod = python::import("test_simple_exceptions");
    BOOST_TEST(mod);

    BOOST_REQUIRE_EXCEPTION(python::getattr<void()>(mod, "undefined_function"), python::exception, check_attribute);
}

BOOST_AUTO_TEST_SUITE_END()
