def integers(n):
    return set(range(n))

def check_integers(n):
    return all(p in n for p in range(len(n)))

def strings(n):
    return set(str(i) + "-value" for i in range(n))

def check_strings(n):
    return all("value-" + str(p) in n for p in range(len(n)))
