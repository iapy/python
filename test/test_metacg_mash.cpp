#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(Mash, python::tester::Metacg)

struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_EXPORT(test_metacg_mash_c);
        PYTHON_IMPORT(test_metacg_mash, (
            (std::string(), test)
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                using Self = Interface<Base>;
                mod.def("get", python::bind<&Self::get>);
            }

            python::dict<python::str, python::tuple<std::size_t, bool>> get()
            {
                using namespace std::string_literals;
                python::dict<python::str, python::tuple<std::size_t, bool>> result;
                result.set("foo"s, python::tuple<std::size_t, bool>(0, true));
                result.set("bar"s, python::tuple<std::size_t, bool>(1, false));
                return result;
            }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                using namespace std::string_literals;
                python::lock([this]{
                    auto module = python::import(this);
                    BOOST_TEST("bar1Falsefoo0True"s == module.test());
                });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
}

BOOST_AUTO_TEST_SUITE_END()
