#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ExportClassArithm, python::tester::Metacg)
using namespace std::string_literals;

struct Component : cg::Component
{
    static int count;

    template<typename>
    struct Types
    {
        struct X
        {
            BOOST_HANA_DEFINE_STRUCT(X,
                (int, value)
            );

            X(int value) : value{value} { ++Component::count; }
            X(X &&other) : value{other.value} { ++Component::count; }
            X(X const &other) : value{other.value} { ++Component::count; }

            ~X() { --Component::count; }

            X operator + (X const &other) const
            {
                return X{value + other.value};
            }

            X operator - (X const &other) const
            {
                return X{value - other.value};
            }

            X operator * (X const &other) const
            {
                return X{value * other.value};
            }

            X operator / (X const &other) const
            {
                return X{value / other.value};
            }
        };

        PYTHON_EXPORT(test_metacg_export_class_arithm_c);
        PYTHON_IMPORT(test_metacg_export_class_arithm, (
            (int(X*, X*), add),
            (int(X*, X*), sub),
            (int(X*, X*), mul),
            (int(X*, X*), div)
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                mod.def<typename Base::X>();
            }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([&, this]{
                    auto a = typename Base::X{10};
                    auto b = typename Base::X{15};

                    auto mod = python::import(this);
                    BOOST_TEST( 25 == mod.add(&a, &b));
                    BOOST_TEST( -5 == mod.sub(&a, &b));
                    BOOST_TEST(  5 == mod.sub(&b, &a));
                    BOOST_TEST(150 == mod.mul(&a, &b));
                    BOOST_TEST(  0 == mod.div(&a, &b));
                    BOOST_TEST(  1 == mod.div(&b, &a));
                });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

int Component::count{0};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
    BOOST_TEST(0 == Component::count);
}

BOOST_AUTO_TEST_SUITE_END()
