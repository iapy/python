def integer_yilder(n):
    while n > 0:
        yield n
        n -= 1

def string_yilder():
    for v in ["a", "b", "c"]:
        yield v
