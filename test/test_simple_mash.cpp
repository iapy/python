#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(Mash, python::tester::Test)

BOOST_AUTO_TEST_CASE(ListSetStringPyPy)
{
    using namespace std::string_literals;

    auto mod = python::import("test_simple_mash");
    auto make = python::getattr<python::list<python::set<python::str>>(std::size_t)>(mod, "make_list_set_string");
    auto test = python::getattr<bool(python::list<python::set<python::str>>)>(mod, "test_list_set_string");

    auto a = make(3);
    BOOST_TEST(3 == a.size());
    BOOST_TEST(0 == a[0].size());
    BOOST_TEST(1 == a[1].size());
    BOOST_TEST(a[1].contains("0value"s));
    BOOST_TEST(2 == a[2].size());
    BOOST_TEST(a[2].contains("0value"s));
    BOOST_TEST(a[2].contains("1value"s));

    auto b = python::list<python::set<python::str>>{
        {}, {"0value"s}, {"1value"s, "0value"s}
    };
    BOOST_TEST(test(a));
    BOOST_TEST(test(b));
}

BOOST_AUTO_TEST_CASE(ListSetStringStdPy)
{
    using namespace std::string_literals;

    auto mod = python::import("test_simple_mash");
    auto make = python::getattr<std::vector<std::set<std::string>>(std::size_t)>(mod, "make_list_set_string");
    auto test = python::getattr<bool(python::list<python::set<python::str>>)>(mod, "test_list_set_string");

    auto a = make(3);
    BOOST_TEST(3 == a.size());
    BOOST_TEST(0 == a[0].size());
    BOOST_TEST(1 == a[1].size());
    BOOST_TEST(a[1].contains("0value"s));
    BOOST_TEST(2 == a[2].size());
    BOOST_TEST(a[2].contains("0value"s));
    BOOST_TEST(a[2].contains("1value"s));

    auto b = python::list<python::set<python::str>>{
        {}, {"0value"s}, {"1value"s, "0value"s}
    };
    BOOST_TEST(test(a));
    BOOST_TEST(test(b));
}

BOOST_AUTO_TEST_CASE(ListSetStringPyStd)
{
    using namespace std::string_literals;

    auto mod = python::import("test_simple_mash");
    auto make = python::getattr<python::list<python::set<python::str>>(std::size_t)>(mod, "make_list_set_string");
    auto test = python::getattr<bool(std::vector<std::set<std::string>>)>(mod, "test_list_set_string");

    auto a = make(3);
    BOOST_TEST(3 == a.size());
    BOOST_TEST(0 == a[0].size());
    BOOST_TEST(1 == a[1].size());
    BOOST_TEST(a[1].contains("0value"s));
    BOOST_TEST(2 == a[2].size());
    BOOST_TEST(a[2].contains("0value"s));
    BOOST_TEST(a[2].contains("1value"s));

    auto b = python::list<python::set<python::str>>{
        {}, {"0value"s}, {"1value"s, "0value"s}
    };
    BOOST_TEST(test(a));
    BOOST_TEST(test(b));
}

BOOST_AUTO_TEST_CASE(ListSetStringStdStd)
{
    using namespace std::string_literals;

    auto mod = python::import("test_simple_mash");
    auto make = python::getattr<std::vector<std::set<std::string>>(std::size_t)>(mod, "make_list_set_string");
    auto test = python::getattr<bool(std::vector<std::set<std::string>>)>(mod, "test_list_set_string");

    auto a = make(3);
    BOOST_TEST(3 == a.size());
    BOOST_TEST(0 == a[0].size());
    BOOST_TEST(1 == a[1].size());
    BOOST_TEST(a[1].contains("0value"s));
    BOOST_TEST(2 == a[2].size());
    BOOST_TEST(a[2].contains("0value"s));
    BOOST_TEST(a[2].contains("1value"s));

    auto b = python::list<python::set<python::str>>{
        {}, {"0value"s}, {"1value"s, "0value"s}
    };
    BOOST_TEST(test(a));
    BOOST_TEST(test(b));
}

BOOST_AUTO_TEST_SUITE_END()

