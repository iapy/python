#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(Simple, python::tester::Metacg)

struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_IMPORT(test_metacg_simple, (
            (std::string(), test)
        ));
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                using namespace std::string_literals;
                python::lock([this]{
                    auto module = python::import(this);
                    BOOST_TEST("works"s == module.test());
                });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Main>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::Graph<Component>>();
}

BOOST_AUTO_TEST_SUITE_END()
