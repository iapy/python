#include <python/detail/config.hpp>
#include <python/detail/pylock.hpp>
#include <python/metacg.hpp>
#include <python/python.hpp>
#include <tester/metacg.hpp>

#include <boost/mpl/list.hpp>
#include <memory>

namespace python::tester {

class ThreadTest
{
public:
    ThreadTest()
    {
        Py_InitializeFromConfig(python::config
            .path(PYTHONPATH)
            .byte(false)
        );
        python::detail::Lock::init();
    };

    ~ThreadTest()
    {
        python::detail::Lock::done();
        Py_FinalizeEx();
    }
};

class Test : ThreadTest
{
public:
    Test() : ThreadTest()
    {
        lock.reset(new python::detail::Lock);
    }

    ~Test()
    {
        lock.reset();
    }

private:
    std::unique_ptr<python::detail::Lock> lock;
};

struct Metacg : ::tester::Metacg
{
    template<typename G, typename ...Args>
    void test_graph(Args&&... args)
    {
        ::tester::Metacg::test_graph<G>(
            cg::Args<python::Python>(nlohmann::json{
                {"path", PYTHONPATH},
                {"byte", false}
            }), std::forward<Args>(args)...
        );
    }
};

using Doubles = boost::mpl::list<float, double>;
using Integers = boost::mpl::list<std::int16_t, std::int32_t, std::int64_t, std::uint16_t, std::uint32_t, std::uint64_t>;

template<typename ...Components>
using Graph = cg::Graph<python::Python, Components...>;

template<typename ...Components>
using ModulesGraph = cg::Graph<cg::Connect<python::Python, Components, python::tag::Module>...>;

} // namespace python::tester
