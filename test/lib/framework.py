import xml.etree.ElementTree as ET
import io, os

def save_report(target, output):
    if not os.path.exists(target):
        with open(target, 'wb') as fd:
            fd.write(output.getvalue())
    else:
        suite = [r for r in ET.fromstring(output.getvalue().decode('UTF-8'))]
        assert 1 == len(suite)
    
        file = next(iter(suite[0])).attrib['file']

        report = ET.parse(target)
        for s in report.getroot():
            if next(iter(s)).attrib['file'] == file:
                for c in list(report.getroot()):
                    report.getroot().remove(c)
                for c in list(suite):
                    report.getroot().append(c)
                break

        with open(target, 'wb') as fd:
            report.write(fd)

