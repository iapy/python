import test_metacg_class_arg_c

class A(object):
    def __init__(self, value):
        self._value = value

    def get_value(self):
        return self._value

def get_value0():
    return test_metacg_class_arg_c.get_value(A("value0"))

def get_value1(a):
    return test_metacg_class_arg_c.get_value(a)

def make_proxy():
    return test_metacg_class_arg_c.make("py-value")
