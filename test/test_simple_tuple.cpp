#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(Tuple, python::tester::Test)
static bool is_out_of_range(python::exception const &ex)
{
    return ex.type() == "IndexError" && ex.what() == "'tuple index out of range'";
}
static bool is_assignment(python::exception const &ex)
{
    return ex.type() == "SystemError";
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Create, T, python::tester::Integers)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_tuple");
    BOOST_TEST(mod);

    auto check_1 = python::getattr<bool(python::object)>(mod, "check_tuple_1");

    auto t1 = python::tuple<T, std::string, std::vector<T>>(T{10}, "foo"s, std::vector{
        T{1}, T{2}, T{3}, T{4}, T{5}
    });
    BOOST_TEST(check_1(t1));

    auto t2 = python::tuple<T, python::str, python::list<T>>(T{10}, "foo"s, std::vector{
        T{1}, T{2}, T{3}, T{4}, T{5}
    });
    BOOST_TEST(check_1(t2));

    auto t3 = python::tuple<T, std::string, std::vector<T>>(T{10}, "foo"s, python::list<T>{
        T{1}, T{2}, T{3}, T{4}, T{5}
    });
    BOOST_TEST(check_1(t3));

    auto t4 = python::tuple<T, python::str, python::list<T>>(T{10}, "foo"s, python::list<T>{
        T{1}, T{2}, T{3}, T{4}, T{5}
    });
    BOOST_TEST(check_1(t4));

    auto u1 = python::tuple<T, std::string, std::vector<T>>();
    u1.template set<0>(T{10});
    u1.template set<1>("foo"s);
    u1.template set<2>(std::vector<T>{T{1}, T{2}, T{3}, T{4}, T{5}});
    BOOST_TEST(check_1(u1));

    auto u3 = python::tuple<T, python::str, python::list<T>>();
    u3.template set<0>(T{10});
    u3.template set<1>("foo"s);
    u3.template set<2>(std::vector<T>{T{1}, T{2}, T{3}, T{4}, T{5}});
    BOOST_TEST(check_1(u3));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Sum, T, python::tester::Integers)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_tuple");
    BOOST_TEST(mod);

    auto check_1 = python::getattr<bool(python::object)>(mod, "check_tuple_1");

    auto u1 = python::tuple<T, std::string>(T{10}, "foo"s);
    auto t1 = python::tuple<std::vector<T>>(python::list<T>{T{1}, T{2}, T{3}, T{4}, T{5}});
    BOOST_TEST(check_1(u1 + t1));

    auto u2 = python::tuple<T>(T{10});
    auto t2 = python::tuple<python::str, std::vector<T>>("foo"s, python::list<T>{T{1}, T{2}, T{3}, T{4}, T{5}});
    BOOST_TEST(check_1(u2 + t2));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Return, T, python::tester::Integers)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_tuple");
    BOOST_TEST(mod);

    auto get_1 = python::getattr<python::object()>(mod, "get_tuple_1");
    auto p1 = python::cast<std::tuple<T, std::string>>(get_1());

    BOOST_TEST(T{42} == python::get<0>(p1));
    BOOST_TEST(python::get<1>(p1) == "baz"s);
    static_assert(p1.size() == 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Errors, T, python::tester::Integers)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_tuple");
    BOOST_TEST(mod);

    auto get_1 = python::getattr<python::object()>(mod, "get_tuple_1");
    auto get_2 = python::getattr<python::object()>(mod, "get_tuple_2");

    auto p1 = python::cast<std::tuple<T, std::string, std::vector<T>>>(get_1());
    static_assert(p1.size() == 3);
    BOOST_TEST(T{42} == python::get<0>(p1));
    BOOST_TEST(python::get<1>(p1) == "baz"s);
    BOOST_REQUIRE_EXCEPTION(python::get<2>(p1), python::exception, is_out_of_range);
    BOOST_REQUIRE_EXCEPTION(p1.template set<0>(T{10}), python::exception, is_assignment);
    BOOST_REQUIRE_EXCEPTION(p1.template set<1>("foo"s), python::exception, is_assignment);
    BOOST_REQUIRE_EXCEPTION(p1.template set<2>(std::vector<T>{}), python::exception, is_assignment);

    auto p2 = python::cast<std::tuple<T, std::string, std::vector<T>>>(get_2());
    static_assert(p1.size() == 3);
    BOOST_TEST(T{42} == python::get<0>(p2));
    BOOST_TEST(python::get<1>(p2) == "baz"s);

    auto l = python::get<2>(p2);
    BOOST_TEST(l.size() == 3);
    BOOST_TEST(l[0] == T{1});
    BOOST_TEST(l[1] == T{2});
    BOOST_TEST(l[2] == T{3});
    l.append(T{4});

    BOOST_REQUIRE_EXCEPTION(p2.template set<0>(T{10}), python::exception, is_assignment);
    BOOST_REQUIRE_EXCEPTION(p2.template set<1>("foo"s), python::exception, is_assignment);
    BOOST_REQUIRE_EXCEPTION(p2.template set<2>(std::vector<T>{}), python::exception, is_assignment);

    auto check_2 = python::getattr<bool(python::object)>(mod, "check_tuple_2");
    BOOST_TEST(check_2(p2));
}

BOOST_AUTO_TEST_SUITE_END()
