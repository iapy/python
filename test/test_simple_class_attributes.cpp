#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(ClassAttributes, python::tester::Test)

PYTHON_MODULE_HEADER(Impl,
(
    PYTHON_CLASS(A, (
        (int, aint),
        (std::set<std::string>, values),
        (std::string(), get_value)
    )),
    PYTHON_CLASS(B, (
        (class A, a)
    )),
    PYTHON_CLASS(C, (
        (std::optional<class A>, a),
        (std::optional<int>, aint)
    ))
),
(
    (class A(std::string), A),
    (class B(class A), B),
    (class C(std::optional<class A>), C)
));

BOOST_AUTO_TEST_CASE(Simple)
{
    using namespace python;
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = cast<Impl>(import("test_simple_class_attributes"));

    auto a = mod.A("my instance"s);
    BOOST_TEST("my instance"s == cast<str>(a["value"]));

    a["value"] = str("new instance"s);
    BOOST_TEST("new instance"s == cast<str>(a["value"]));
    BOOST_TEST("new instance"s == a.get_value());
    BOOST_TEST(11 == cast<int>(a["aint"]));
    BOOST_TEST(11 == a["aint"_a]);

    BOOST_TEST(1 == cast<set<str>>(a["values"]).size());

    auto s = cast<set<str>>(a["values"]);
    s.insert("1"s);

    auto t = a["values"_a];
    s.insert("2"s);

    BOOST_TEST(3 == cast<set<str>>(a["values"]).size());
    BOOST_TEST(3 == a["values"_a].size());

    a["values"] = set<str>{
        "a"_s, "b"_s, "c"_s, "d"_s
    };
    BOOST_TEST(4 == cast<set<str>>(a["values"]).size());

    a["values"_a] = set<str>{
        "a"_s, "b"_s, "c"_s, "d"_s, "e"_s, "f"_s
    };
    BOOST_TEST(6 == cast<set<str>>(a["values"]).size());
    BOOST_TEST(6 == a["values"_a].size());

    a["aint"] = object::from(555);
    BOOST_TEST(555 == cast<int>(a["aint"]));

    a["aint"_a] = 444;
    BOOST_TEST(444 == a["aint"_a]);

    a["aint"_a] += 1;
    BOOST_TEST(445 == a["aint"_a]);

    a["aint"_a] -= 40;
    BOOST_TEST(405 == a["aint"_a]);
}

BOOST_AUTO_TEST_CASE(Layered)
{
    using namespace python;
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = cast<Impl>(import("test_simple_class_attributes"));
    auto a = mod.A("foobar"s);
    auto b = mod.B(a);

    BOOST_TEST(6 == b["a"_a]["aint"_a]);
    BOOST_TEST(1 == b["a"_a]["values"_a].size());

    a["values"_a].insert("1"s);
    BOOST_TEST(2 == b["a"_a]["values"_a].size());

    b["a"_a] = mod.A("foo"s);
    BOOST_TEST(1 == b["a"_a]["values"_a].size());
}

BOOST_AUTO_TEST_CASE(Optional)
{
    using namespace python;
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = cast<Impl>(import("test_simple_class_attributes"));
    auto a = mod.A("foobar"s);

    auto c1 = mod.C(std::nullopt);
    BOOST_TEST(!c1["a"_a]);

    c1["a"_a] = a;
    BOOST_TEST(!!c1["a"_a]);
    BOOST_TEST("foobar"s == c1["a"_a]->get_value());
    BOOST_TEST(6 == c1["a"_a]->at("aint"_a));

    c1["a"_a] = std::nullopt;
    BOOST_TEST(!c1["a"_a]);

    BOOST_TEST(!c1["aint"_a]);

    c1["aint"_a] = int{10};
    BOOST_TEST(10 == *c1["aint"_a]);

    auto c2 = mod.C(a);
    BOOST_TEST(!!c2["a"_a]);
    BOOST_TEST("foobar"s == c2["a"_a]->get_value());
    BOOST_TEST(6 == c2["a"_a]->at("aint"_a));
}

BOOST_AUTO_TEST_SUITE_END()
