def get_string(s):
    if s == None:
        return "null"
    if s != "null":
        return s

def get_vector():
    return ["foo", None, "bar"]

def get_integer(s):
    if s == None:
        return 42
    if s != "null":
        return len(s)
