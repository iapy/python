#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ExportClass, python::tester::Metacg)
using namespace std::string_literals;

struct Component : cg::Component
{
    static int count;

    template<typename>
    struct Types
    {
        class E1
        {
        public:
            int v;
            int foo()
            {
                return v;
            }
            std::string bar(python::str value)
            {
                return "cpp-" + std::string(value);
            }
            operator bool () const
            {
                return v != 0;
            }
            operator std::string () const
            {
                return std::to_string(v);
            }
        };

        class E2
        {
        public:
            E2() { ++count; }
            E2(E2 &&t) : v(t.v) { ++count; }
            E2(E2 const &t) : v(t.v) { ++count; }
            ~E2() { --count; }

            int v;
            int operator () ()
            {
                return v;
            }
        };

        PYTHON_EXPORT(test_metacg_export_class_c);
        PYTHON_IMPORT(test_metacg_export_class, (
            (std::string(), get_e1_name),
            (std::string(), get_e2_name),

            (int(E2*), call_e2),
            (int(E1*), call_e1_foo),
            (std::string(E1*), call_e1_bar),
            (std::string(python::list<python::object>), as_list),
            (std::string(python::dict<std::string, python::object>), as_dict),

            (std::string(E1*), to_string),
            (bool(E1*), to_bool),
            (int(python::list<python::object>), sum_e2)
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                mod.def<typename Base::E1>()
                    .def("bar", python::bind<&Base::E1::bar>)
                    .def("foo", python::bind<&Base::E1::foo>)
                    .def("__str__", python::bind<&Base::E1::operator std::string>)
                ;
                mod.def<typename Base::E2>()
                    .def("__call__", python::bind<&Base::E2::operator ()>)
                ;
            }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([&, this]{
                    auto mod = python::import(this);
                    BOOST_TEST("E1"s == mod.get_e1_name());
                    BOOST_TEST("E2"s == mod.get_e2_name());

                    typename Base::E1 e1;
                    typename Base::E2 e2;
                    e1.v = 42;
                    e2.v = 24;

                    BOOST_TEST(24 == mod.call_e2(&e2));
                    BOOST_TEST(42 == mod.call_e1_foo(&e1));
                    BOOST_TEST("cpp-value"s == mod.call_e1_bar(&e1));

                    --e1.v;
                    ++e2.v;

                    BOOST_TEST("4125"s == mod.as_list(python::list<python::object>{
                        python::native(&e1), python::native(&e2)
                    }));
                    BOOST_TEST("4125"s == mod.as_dict(python::dict<std::string, python::object>{
                        {"e1"s, python::native(&e1)},
                        {"e2"s, python::native(&e2)}
                    }));
                    BOOST_TEST("41"s == mod.to_string(&e1));
                    BOOST_TEST(mod.to_bool(&e1));

                    e1.v = 0;
                    BOOST_TEST(!mod.to_bool(&e1));

                    python::list<python::object> e2sp;
                    python::list<python::object> e2sup;

                    std::vector<typename Base::E2> e2s;
                    e2s.resize(20);
                    for(std::size_t i = 0; i < e2s.size(); ++i)
                    {
                        e2s[i].v = i + 1;
                        e2sp.append(python::native(&e2s[i]));
                        e2sup.append(python::native(std::make_unique<typename Base::E2>(e2s[i])));
                    }
                    BOOST_TEST(210 == mod.sum_e2(e2sp));
                    BOOST_TEST(210 == mod.sum_e2(e2sup));
                });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

int Component::count{0};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
    BOOST_TEST(0 == Component::count);
}

BOOST_AUTO_TEST_SUITE_END()
