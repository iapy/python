import test_metacg_args_c

def list_proxy():
    return test_metacg_args_c.list_join(["ab", "cd", "ef", "gh"])

def dict_proxy():
    return test_metacg_args_c.dict_join({1: 'a', 2: 'b', 3: 'c'})

def string_list_proxy():
    return test_metacg_args_c.string_join_list("yz", ["ab", "cd", "ef", "gh"])

def string_dict_proxy():
    return test_metacg_args_c.string_join_dict("12345_", {1: 'a', 2: 'b', 3: 'c'})

def set_value_proxy():
    test_metacg_args_c.set_value("Hello from python!")
