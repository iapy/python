def integers(count):
    return list(range(count)) 

def integers_check(r):
    return list(range(len(r), 0, -1)) == r

def integers_check_reversed(r):
    return list(range(len(r), 0, -1)) == list(reversed(r))

def strings(count):
    return ["foobar"] * count

def strings_check(r):
    return (["barfoo"] * (len(r)//2) + ["foobar"] * (len(r)//2)) == r

def strings_check_reversed(r):
    return (["foobar"] * (len(r)//2) + ["barfoo"] * (len(r)//2)) == r

def booleans():
    return [True, False]

def booleans_check(vs, x):
    return all(v == x for v in vs)

def add_list(l, v):
    l.append(v)

def check_equal(a, b):
    return a == b
