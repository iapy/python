def make_list_set_string(v):
    return [set(str(i) + "value" for i in range(n)) for n in range(v)]

def test_list_set_string(v):
    return v == make_list_set_string(len(v))
