#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ExportStruct, python::tester::Metacg)
using namespace std::string_literals;

struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        struct Foo
        {
            BOOST_HANA_DEFINE_STRUCT(Foo,
                (python::dict<int, python::str>, dict), 
                (std::string, name),
                (int, value)
            );

            std::string info()
            {
                return name + std::to_string(value);
            }
        };

        PYTHON_EXPORT(test_metacg_export_struct_c);
        PYTHON_IMPORT(test_metacg_export_struct, (
            (std::string(Foo*), get_name),
            (int(Foo*), get_value),
            (void(Foo*, std::string), set_name),
            (void(Foo*, int), set_value),
            (void(Foo*, int, std::string), add_dict),
            (void(Foo*), set_dict),
            (std::string(Foo*), get_info)
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                mod.def<typename Base::Foo>()
                    .def("info", python::bind<&Base::Foo::info>)
                ;
            }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([&, this]{
                    auto mod = python::import(this);

                    typename Base::Foo foo;
                    foo.name = "test";
                    foo.value = 12345;

                    BOOST_TEST("test"s == mod.get_name(&foo));
                    BOOST_TEST(12345 == mod.get_value(&foo));

                    mod.set_name(&foo, "foo"s);
                    mod.set_value(&foo, 42);

                    BOOST_TEST("foo"s == foo.name);
                    BOOST_TEST(42 == foo.value);

                    BOOST_TEST("foo42"s == mod.get_info(&foo));

                    mod.add_dict(&foo, 1, "one"s);
                    mod.add_dict(&foo, 2, "two"s);

                    BOOST_TEST(2 == foo.dict.size());
                    BOOST_TEST(foo.dict.contains(1));
                    BOOST_TEST("one"s == foo.dict[1]);
                    BOOST_TEST(foo.dict.contains(2));
                    BOOST_TEST("two"s == foo.dict[2]);

                    mod.set_dict(&foo);
                    BOOST_TEST(3 == foo.dict.size());
                    BOOST_TEST(foo.dict.contains(1));
                    BOOST_TEST("a"s == foo.dict[1]);
                    BOOST_TEST(foo.dict.contains(2));
                    BOOST_TEST("b"s == foo.dict[2]);
                    BOOST_TEST(foo.dict.contains(3));
                    BOOST_TEST("c"s == foo.dict[3]);
                });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
}

BOOST_AUTO_TEST_SUITE_END()
