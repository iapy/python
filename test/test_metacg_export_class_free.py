import test_metacg_export_class_free_c

def get_code(a):
    return a.code

def get_test(v):
    x = test_metacg_export_class_free_c.Test(v)
    if bytes(x) != b'foobar':
        return 0
    y = test_metacg_export_class_free_c.TestFromString(str(v))
    if bytes(y) != b'foobar':
        return 0
    return x.code + y.code

def get_bytes(a):
    return len(bytes(a)) == len("foobar")

def test_code(a):
    return a.test(a.code)

def test_code_str(a):
    return a.test_str(str(a.code))
