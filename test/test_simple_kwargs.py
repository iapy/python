def only_kwargs(**kwargs):
    return ",".join(f'{k}={kwargs[k]}' for k in sorted(kwargs.keys()))

def one_arg_and_kwargs(a, **kwargs):
    return str(a) + only_kwargs(**kwargs)

def args_and_kwargs(*args, **kwargs):
    return ','.join(str(x) for x in args) + only_kwargs(**kwargs)
