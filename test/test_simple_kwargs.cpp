#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(KWargs, python::tester::Test)

BOOST_AUTO_TEST_CASE(Dict)
{
    using namespace python;
    auto m = import("test_simple_kwargs");

    auto only_kwargs = getattr<str(kwargs)>(m, "only_kwargs");
    BOOST_TEST(""_s == only_kwargs());
    BOOST_TEST("key=42,value=foo"_s == only_kwargs(**dict<str, obj>({
        {"key"_s, obj::from(42)},
        {"value"_s, "foo"_s}
    })));

    auto one_arg_and_kwargs = getattr<str(str, kwargs)>(m, "one_arg_and_kwargs");
    BOOST_TEST("prefix_"_s == one_arg_and_kwargs("prefix_"_s));
    BOOST_TEST("prefix_key=11,value=bar"_s == one_arg_and_kwargs("prefix_"_s, **dict<str, obj>({
        {"key"_s, obj::from(11)},
        {"value"_s, "bar"_s}
    })));

    auto args_and_kwargs = getattr<str(args, kwargs)>(m, "args_and_kwargs");
    BOOST_TEST(""_s == args_and_kwargs());
    BOOST_TEST("1,2,3"_s == args_and_kwargs(1, 2, 3));
    BOOST_TEST("key=42,value=foo"_s == args_and_kwargs(**dict<str, obj>({
        {"key"_s, obj::from(42)},
        {"value"_s, "foo"_s}
    })));
    BOOST_TEST("1,2,3key=42,value=foo"_s == args_and_kwargs(1, 2, 3, **dict<str, obj>({
        {"key"_s, obj::from(42)},
        {"value"_s, "foo"_s}
    })));
    BOOST_TEST("1,2,3key=42,value=foo"_s == args_and_kwargs(*list<int>{1, 2, 3}, **dict<str, obj>({
        {"key"_s, obj::from(42)},
        {"value"_s, "foo"_s}
    })));
}

BOOST_AUTO_TEST_SUITE_END()
