#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(Callback, python::tester::Metacg)
using namespace std::string_literals;

struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        using Callback = std::function<void(python::dict<std::string, std::string>)>;

        PYTHON_EXPORT(test_metacg_callback_c);
        PYTHON_IMPORT(test_metacg_callback, (
            (std::string(Callback *), name),
            (void(python::dict<std::string, python::object>), test)
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                mod.def<typename Base::Callback>()
                    .def("__call__", python::bind<&Base::Callback::operator()>)
                    = "Callback"
                ;
            }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([&, this]{
                    auto mod = python::import(this);

                    bool called{false};
                    typename Base::Callback cb = [&called](python::dict<std::string, std::string> d) -> void {
                        BOOST_TEST(2 == d.size());
                        BOOST_TEST(d.contains("a"s));
                        BOOST_TEST("foo"s == d["a"s]);
                        BOOST_TEST(d.contains("b"s));
                        BOOST_TEST("bar"s == d["b"s]);
                        called = true;
                    };

                    mod.test(python::dict<std::string, python::object>{
                        {"callback"s, python::native(&cb)}
                    });

                    BOOST_TEST(called);
                    BOOST_TEST("Callback"s == mod.name(&cb));
                });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
}

BOOST_AUTO_TEST_SUITE_END()
