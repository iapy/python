#include <cg/bind.hpp>
#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(Args, python::tester::Metacg)
using namespace std::string_literals;

struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_EXPORT(test_metacg_args_c);
        PYTHON_IMPORT(test_metacg_args, (
            (std::string(), dict_proxy),
            (std::string(), list_proxy),
            (std::string(), string_dict_proxy),
            (std::string(), string_list_proxy),
            (void(), set_value_proxy)
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                using Self = Interface<Base>;
                mod.def("dict_join", python::bind<&Self::dict_join>);
                mod.def("list_join", python::bind<&Self::list_join>);
                mod.def("string_join_list", python::bind<&Self::string_join_list>);
                mod.def("string_join_dict", python::bind<&Self::string_join_dict>);
                mod.def("set_value", python::bind<&Self::set_value>);
            }

            std::string dict_join(python::dict<int, python::str> dict)
            {
                std::ostringstream ss;
                for(auto const [k, v] : dict)
                {
                    ss << k << ':' << v;
                }
                return ss.str();
            }

            std::string list_join(python::list<python::str> list)
            {
                std::ostringstream ss;
                for(auto const &s : list)
                {
                    ss << s;
                }
                return ss.str();
            }

            std::string string_join_list(python::str v, python::list<python::str> list)
            {
                std::ostringstream ss;
                ss << v << list_join(list);
                return ss.str();
            }

            std::string string_join_dict(python::str v, python::dict<int, python::str> dict)
            {
                std::ostringstream ss;
                ss << v << dict_join(dict);
                return ss.str();
            }

            void set_value(python::str v)
            {
                this->state.value = v;
            }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([this]{
                    auto module = python::import(this); 
                    BOOST_TEST("abcdefgh"s == module.list_proxy());
                    BOOST_TEST("1:a2:b3:c"s == module.dict_proxy());
                    BOOST_TEST("yzabcdefgh"s == module.string_list_proxy());
                    BOOST_TEST("12345_1:a2:b3:c"s == module.string_dict_proxy());
                    module.set_value_proxy();
                    BOOST_TEST("Hello from python!"s == this->state.value);

                    auto self = this->local(python::tag::Module{});
                    BOOST_TEST("abcdefgh"s == self.list_join(python::list<python::str>{"abcd"s, "efgh"s}));
                    BOOST_TEST("__abcdefgh"s == self.string_join_list("__"s, python::list<python::str>{"abcd"s, "efgh"s}));

                    self.set_value("Hello from c++!"s);
                    BOOST_TEST("Hello from c++!"s == this->state.value);
                });
                return 0;
            }
        };
    };

    template<typename>
    struct State
    {
        std::string value;
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
}

BOOST_AUTO_TEST_SUITE_END()
