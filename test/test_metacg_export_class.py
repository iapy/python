import test_metacg_export_class_c

def get_e1_name():
    return test_metacg_export_class_c.E1.__name__

def get_e2_name():
    return test_metacg_export_class_c.E2.__name__

def call_e2(e2):
    return e2()

def call_e1_foo(e1):
    return e1.foo()

def call_e1_bar(e1):
    return e1.bar("value")

def as_list(l):
    return str(l[0].foo()) + str(l[1]())

def as_dict(d):
    return str(d["e1"].foo()) + str(d["e2"]())

def to_string(s):
    return str(s)

def to_bool(s):
    return not (not s)

def sum_e2(l):
    s = 0
    for e in l:
        s += e()
    return s
