#include <python/common.hpp>
#include "lib/framework.hpp"
#include <jsonio/deserialize.hpp>

#include <string>

BOOST_FIXTURE_TEST_SUITE(Library, python::tester::Test)

PYTHON_MODULE_HEADER(Json, (
    (std::string(python::object), dumps),
    (python::object(std::string), loads)   
));

BOOST_AUTO_TEST_CASE(JsonModule)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto json = python::cast<Json>(python::import("json"));
    auto data = json.dumps(python::list<std::string>{
        "foo"s, "bar"s
    });

    auto const vector_strings = std::vector<std::string>{"foo"s, "bar"s};
    BOOST_TEST(vector_strings == jsonio::from<std::vector<std::string>>(
        nlohmann::json::parse(static_cast<std::string_view>(data))
    ));

    auto original = static_cast<python::list<std::string>>(json.loads(data));
    BOOST_TEST(2 == original.size());
    BOOST_TEST("foo"sv == original[0]);
    BOOST_TEST("bar"sv == original[1]);
}

BOOST_AUTO_TEST_SUITE_END()
