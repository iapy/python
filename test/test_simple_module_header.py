def get_integer():
    return 42

def get_string():
    return "Hello world"

def sum_integers(a, b):
    return a + b

class A(object):
    def __init__(self):
        self.value = 10

    def set_int(self, a):
        self.value = a

    def get_int(self):
        return self.value

class B(object):
    def __init__(self, value):
        self.value = value

    def set_string(self, a):
        self.value = a

    def get_string(self):
        return self.value
