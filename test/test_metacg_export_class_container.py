import test_metacg_export_class_container_c

def sum_values(n):
    result = 0
    for o in test_metacg_export_class_container_c.make(n):
        result += o.v
    return result
