#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(ModuleHeader, python::tester::Test)

PYTHON_MODULE_HEADER(Functions,
(
    (int(), get_integer),
    (std::string(), get_string),
    (int(int, int), sum_integers)
));

PYTHON_MODULE_HEADER(FunctionsAndClasses,
(
    PYTHON_CLASS(A, (
        (void(int), set_int),
        (int(void), get_int)
    )),
    PYTHON_CLASS(B, (
        (void(std::string), set_string),
        (std::string(void), get_string)
    ))
),
(
    (class A(), A),
    (class B(std::string), B),

    (int(), get_integer),
    (std::string(), get_string),
    (int(int, int), sum_integers)
));

BOOST_AUTO_TEST_CASE(ImportFunctions)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::cast<Functions>(python::import("test_simple_module_header"));
    BOOST_TEST(mod.__name__() == "test_simple_module_header");
    BOOST_TEST(mod.__file__().filename() == "test_simple_module_header.py");

    BOOST_TEST(42 == mod.get_integer());
    BOOST_TEST(24 == mod.sum_integers(11, 13));
    BOOST_TEST("Hello world"sv == mod.get_string());
}

BOOST_AUTO_TEST_CASE(ImportFunctionsAndClasses)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::cast<FunctionsAndClasses>(python::import("test_simple_module_header"));
    BOOST_TEST(mod.__name__() == "test_simple_module_header");
    BOOST_TEST(mod.__file__().filename() == "test_simple_module_header.py");

    BOOST_TEST(42 == mod.get_integer());
    BOOST_TEST(24 == mod.sum_integers(11, 13));
    BOOST_TEST("Hello world"sv == mod.get_string());

    auto a = mod.A();
    BOOST_TEST(10 == a.get_int());

    a.set_int(12);
    BOOST_TEST(12 == a.get_int());

    a.set_int(mod.get_integer());
    BOOST_TEST(42 == a.get_int());

    a.set_int(mod.sum_integers(10, 11));
    BOOST_TEST(21 == a.get_int());

    auto b = mod.B("foo"s);
    BOOST_TEST("foo"sv == b.get_string());

    b.set_string("bar"s);
    BOOST_TEST("bar"sv == b.get_string());

    b.set_string(mod.get_string());
    BOOST_TEST("Hello world"sv == b.get_string());
}

BOOST_AUTO_TEST_SUITE_END()
