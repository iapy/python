#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(DictGeneric, python::tester::Test)
PYTHON_MODULE_HEADER(Impl,
(
    PYTHON_CLASS(Test, (
        (std::string(), get_value),
        (void(std::string), set_value)
    ))
),
(
    (class Test(std::string), Test),
    (std::map<std::string, python::object>(), make),
    (bool(std::map<std::string, python::object>), test)
));

BOOST_AUTO_TEST_CASE(Test)
{
    using namespace std::string_view_literals;
    using namespace std::string_literals;

    auto mod = python::cast<Impl>(python::import("test_simple_dict_generic"));

    auto d = mod.make();
    BOOST_TEST(d.contains("int_value"s));
    BOOST_TEST(-10 == python::cast<int>(d["int_value"s]));

    BOOST_TEST(d.contains("int_function"s));
    BOOST_TEST(42 == python::cast<int()>(d["int_function"s])());

    BOOST_TEST(d.contains("int_function_int"s));
    BOOST_TEST(0 == python::cast<int(int)>(d["int_function_int"s])(-42));

    BOOST_TEST(d.contains("str_function"s));
    BOOST_TEST("Hello there"s == python::cast<std::string()>(d["str_function"s])());

    BOOST_TEST(d.contains("str_function_str"s));
    BOOST_TEST("Hello coder"sv == python::cast<std::string(std::string)>(d["str_function_str"s])("coder"s));

    BOOST_TEST(d.contains("int_list"s));
    BOOST_TEST(3 == python::cast<std::vector<int>>(d["int_list"s]).size());

    BOOST_TEST(d.contains("test"s));
    BOOST_TEST("I'm test"sv == python::cast<class Impl::Test>(d["test"s]).get_value());

    python::cast<class Impl::Test>(d["test"s]).set_value("Changed"s);
    d.set("added"s, mod.Test("Added"s));
    d.set("int_value"s, PyLong_FromLongLong(-246));
    d.set("str_function_str"s, python::getattr<python::object>(mod, "str_function_str_2"));
    BOOST_TEST(mod.test(d));
}

BOOST_AUTO_TEST_SUITE_END()
