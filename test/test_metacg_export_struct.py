def get_name(foo):
    return foo.name

def get_value(foo):
    return foo.value

def set_name(foo, name):
    foo.name = name

def set_value(foo, value):
    foo.value = value

def add_dict(foo, k, v):
    foo.dict[k] = v

def set_dict(foo):
    foo.dict = dict({1: 'a', 2: 'b', 3: 'c'})

def get_info(foo):
    return foo.info()
