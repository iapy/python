#include <python/metacg.hpp>
#include <python/python.hpp>
#include <config/options.hpp>
#include <cg/service.hpp>

struct Downloader : cg::Component
{
    template<typename>
    struct Types
    {
        using Hook = std::function<void(python::dict<python::str, python::obj>)>;

        PYTHON_EXPORT(yt_dlp_c);
        PYTHON_IMPORT(yt_dlp,
        (
            PYTHON_CLASS(YoutubeDL, (
                (void(python::list<std::string>), download)
            ))
        ),
        (
            (class YoutubeDL(python::dict<python::str, python::obj>), YoutubeDL)
        ));
    };

    struct Module
    {
        template<typename Base>
        struct Interface : Base
        {
            void init(python::moddef &mod)
            {
                mod.def<typename Base::Hook>()
                    .def("__call__", python::bind<&Base::Hook::operator()>)
                    = "Hook"
                ;
            }
        };
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([this]{
                    using namespace python;

                    typename Base::Hook hook = [](dict<str, obj> d) {
                        if(("filename"_s ^ d) && ("status"_s ^ d) && d["status"_s] == "finished"_s)
                        {
                            std::cout << static_cast<str>(d["filename"_s]) << '\n';
                        }
                    };

                    auto downloader = import(this).YoutubeDL(dict<str, obj>{
                        {"paths"_s, dict<str, obj>{
                            {"home"_s, str(this->state.dir.u8string())}
                        }},
                        {"progress_hooks"_s, list<obj>{
                            native(&hook)
                        }},
                        {"quiet"_s,      obj::from(true)},
                        {"noprogress"_s, obj::from(true)},
                        {"outtmpl"_s,    "%(id)s.%(ext)s"_s},
                        {"format"_s,     "mp4/bestvideo/best"_s}
                    });

                    try {
                        with(downloader, [this, &downloader](auto){
                            auto vs = list<str>();
                            for(auto const &v : this->state.videos)
                            {
                                vs.append(v);
                            }
                            downloader.download(vs);
                        });
                    } catch(exception const &e) {
                        std::cerr << e.what() << '\n';
                    }
                });
                return 0;
            }
        };
    };

    template<typename>
    struct State
    {
        std::filesystem::path const dir;
        std::vector<std::string> videos;
    };

    using Ports = ct::map<python::module<Downloader>>;
    using Services = ct::tuple<Service>;
};

int main(int argc, char **argv)
{
    std::vector<std::string> videos;
    config::parse(argc, argv, config::arg(videos, "youtube video ids")); 

    using G = cg::Graph<python::Modules<Downloader>>;
    auto host = cg::Host<G>(
        cg::Args<python::Python>(nlohmann::json{
        }),
        cg::Args<Downloader>(
            std::filesystem::current_path(),
            videos
        )
    );
    return cg::Service(host);
}
