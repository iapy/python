#include <python/metacg.hpp>
#include <python/python.hpp>
#include <cg/service.hpp>

struct Templater : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_MODULE_HEADER(Jinja,
        (
            PYTHON_CLASS(Template,
            (
                (python::str(python::kwargs), render)
            )),
            PYTHON_CLASS(Environment,
            (
                (class Template(python::str), get_template)
            )),
            PYTHON_CLASS(FileSystemLoader)
        ),
        (
            (class Environment(python::kwargs), Environment),
            (class FileSystemLoader(python::list<python::str>), FileSystemLoader)
        ));
    };

    struct Service
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                return python::lock([]{
                    using namespace python;

                    auto jinja = cast<typename Base::Jinja>(import("jinja2"));
                    auto e = jinja.Environment(**dict<str, obj>{
                        {"loader"_s, jinja.FileSystemLoader(list<str>{
                            std::string_view(PYTHONPATH)
                        })}
                    });
                    auto t = e.get_template("demo.jinja2"_s);
                    std::cout << t.render(**dict<str, obj>{
                        {"a"_s, "test"_s},
                        {"b"_s, obj::from(42)}
                    }) << std::endl;

                    return 0;
                });
            }
        };
    };

    using Services = ct::tuple<Service>;
};

int main()
{
    using G = cg::Graph<python::Python, Templater>;
    auto host = cg::Host<G>(
        cg::Args<python::Python>(nlohmann::json{
        })
    );
    return cg::Service(host);
}
