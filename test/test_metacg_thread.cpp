#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(Thread, python::tester::Metacg)

struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_EXPORT(test_metacg_thread_c);
        PYTHON_IMPORT(test_metacg_thread, (
            (int(), get) 
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                using Self = Interface<Base>;
                mod.def("get", python::bind<&Self::get>);
            }

            int get()
            {
                return 42;
            }
        };
    };

    template<int I>
    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                for(std::size_t i = 0; i < 40000; ++i) {
                    python::lock([this]{
                        using namespace python;
                        BOOST_TEST(84 == import(this).get());
                    });
                }
                return 0;
            }
        };
    };

    using Services = ct::tuple<Main<0>, Main<1>, Main<2>, Main<3>, Main<4>>;
    using Ports = ct::map<python::module<Component>>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
}

BOOST_AUTO_TEST_SUITE_END()
