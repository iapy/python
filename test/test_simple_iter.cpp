#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(Iter, python::tester::Test)

BOOST_AUTO_TEST_CASE_TEMPLATE(ReturnIntegral, T, python::tester::Integers)
{
    auto mod = python::import("test_simple_iter");
    BOOST_TEST(mod);

    auto iterate = python::getattr<python::iter<T>(std::size_t)>(mod, "integer_yilder");
    BOOST_TEST(iterate);

    std::vector<T> vector;
    for(T const value : iterate(10))
    {
        vector.push_back(value);
    }

    std::vector<T> const expected{10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    BOOST_TEST(vector == expected);
}

BOOST_AUTO_TEST_CASE(ReturnString)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_iter");
    BOOST_TEST(mod);

    auto iterate = python::getattr<python::iter<std::string>()>(mod, "string_yilder");
    BOOST_TEST(iterate);

    std::vector<std::string> vector;
    for(auto const value : iterate())
    {
        vector.push_back(value);
    }

    std::vector<std::string> const expected{"a"s, "b"s, "c"s};
    BOOST_TEST(vector == expected);
}

BOOST_AUTO_TEST_SUITE_END()
