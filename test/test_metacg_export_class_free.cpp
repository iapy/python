#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ExportClassFree, python::tester::Metacg)
using namespace std::string_literals;

struct Component : cg::Component
{
    static int count;

    template<typename>
    struct Types
    {
        struct Test
        {
            int code;

            Test(int c = 0) : code(c) { ++count; }
            Test(Test &&t) : code(t.code) { ++count; }
            Test(Test const &t) : code(t.code) { ++count; }

            ~Test() { --count; }
        };

        PYTHON_EXPORT(test_metacg_export_class_free_c);
        PYTHON_IMPORT(test_metacg_export_class_free, (
            (int(Test *), get_code),
            (int(int), get_test),
            (bool(Test *), get_bytes),
            (bool(Test *), test_code),
            (bool(Test *), test_code_str)
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                mod.def<typename Base::Test>()
                    .def("code", python::bind<&Base::Test::code>)
                    .def("test", python::bind<&Interface<Base>::test>)
                    .def("test_str", python::bind<&Interface<Base>::test_str>)
                    .def("__bytes__", python::bind<&Interface<Base>::bytes>);

                mod.def("Test", python::bind<&Interface<Base>::make>);
                mod.def("TestFromString", python::bind<&Interface<Base>::make_from_string>);
            }

            std::unique_ptr<typename Base::Test> make(int value)
            {
                return std::make_unique<typename Base::Test>(value + 10);
            }

            std::unique_ptr<typename Base::Test> make_from_string(std::string &&value)
            {
                return std::make_unique<typename Base::Test>(std::atoi(value.c_str()) + 10);
            }

            python::object bytes(typename Base::Test *self) const
            {
                return PyBytes_FromStringAndSize("foobar", 6);
            }

            bool test(typename Base::Test *self, int v) const
            {
                return self->code == v; 
            }

            bool test_str(typename Base::Test *self, std::string &&v) const
            {
                return self->code == std::atoi(v.c_str());
            }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([&, this]{
                    auto mod = python::import(this);

                    typename Base::Test t{200};

                    BOOST_TEST(200 == mod.get_code(&t));
                    BOOST_TEST(100 == mod.get_test(40));
                    BOOST_TEST(mod.get_bytes(&t));
                    BOOST_TEST(mod.test_code(&t));
                    BOOST_TEST(mod.test_code_str(&t));
                    BOOST_TEST(mod.get_bytes(std::make_unique<typename Base::Test>(t)));
                    BOOST_TEST(mod.test_code(std::make_unique<typename Base::Test>(t)));
                    BOOST_TEST(mod.test_code_str(std::make_unique<typename Base::Test>(t)));
                });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

int Component::count{0};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
    BOOST_TEST(0 == Component::count);
}

BOOST_AUTO_TEST_SUITE_END()
