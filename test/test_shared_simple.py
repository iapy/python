import io
import os
import sys
import json
import atexit
import unittest
import xmlrunner
import lib.framework
sys.path[0] = os.getcwd()

import test_shared_simple

class TestSharedSimple(unittest.TestCase):
    def test_get_string(self):
        v = test_shared_simple.get_string([1, 2, 3])
        self.assertEqual([1, 2, 3], json.loads(v))


if __name__ == '__main__':
    output = io.BytesIO()
    atexit.register(lib.framework.save_report, sys.argv.pop(), output)
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output=output))

