class A(object):
    def __init__(self, value):
        self.value = value
        self.values = set([value])
        self.aint = len(value)

    def get_value(self):
        return self.value

class B(object):
    def __init__(self, a):
        self.a = a

class C(object):
    def __init__(self, a):
        self.a = a
        self.aint = None
