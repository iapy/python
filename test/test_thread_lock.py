value = None

def get():
    global value
    return value

def set(a):
    global value
    value = a

def fail(x):
    raise Exception("Failed " + str(x))
