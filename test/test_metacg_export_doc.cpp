#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ExportDoc, python::tester::Metacg)
using namespace std::string_literals;

struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_EXPORT(test_metacg_export_doc_c);
        PYTHON_IMPORT(test_metacg_export_doc, (
            (std::string(int), doc),
            (std::string(), get)
        ));

        class E
        {
        public:
            int v;
            int foo()
            {
                return v;
            }
        };
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                using Self = Interface<Base>;
                mod.def("get", python::bind<&Self::get>, "Gets value");
                mod.def<typename Base::E>("E class")
                    .def("foo", python::bind<&Base::E::foo>, "E::foo method")
                ;
            }

            std::string get() { return this->state.value; }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([module=python::import(this)]{
                    BOOST_TEST("value"s == module.get());

                    BOOST_TEST("Gets value"s    == module.doc(0));
                    BOOST_TEST("E class"s       == module.doc(1));
                    BOOST_TEST("E::foo method"s == module.doc(2));
                });
                return 0;
            }
        };
    };

    template<typename>
    struct State
    {
        std::string const value {"value"};
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
}

BOOST_AUTO_TEST_SUITE_END()
