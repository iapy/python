import sys

class Impl:
    def __call__(self, v):
        return ''.join(v)

sys.modules[__name__] = Impl()
