#include <python/common.hpp>
#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(String, python::tester::Test)

BOOST_AUTO_TEST_CASE(Assign)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_string");
    BOOST_TEST(mod);

    auto get_string = python::getattr<std::string()>(mod, "get_string");
    auto str = get_string();
    BOOST_TEST("foobar"sv == str);

    str = "barfoo"sv;
    BOOST_TEST("barfoo"sv == str);

    auto test_string = python::getattr<bool(std::string, std::string)>(mod, "test_string");
    BOOST_TEST(test_string(str, python::str("barfoo"sv)));

    str = "testme"s;
    BOOST_TEST(test_string(str, "testme"s));

    python::str tr = "testm"sv;
    BOOST_TEST(str <= str);
    BOOST_TEST(str >= str);

    BOOST_TEST(tr < str);
    BOOST_TEST(tr <= str);

    BOOST_TEST(str > tr);
    BOOST_TEST(str >= tr);
}

BOOST_AUTO_TEST_SUITE_END()
