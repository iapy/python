def get_integer():
    return 42

def get_double():
    return 1.5

def get_true():
    return True

def get_false():
    return False

def get_string():
    return "foo"

def add(a, b):
    return a + b

def negate(v):
    return not v
