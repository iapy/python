#include <python/common.hpp>
#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(Optional, python::tester::Test)

BOOST_AUTO_TEST_CASE(Return)
{
    using namespace std::string_literals;

    auto mod = python::import("test_simple_optional");
    BOOST_TEST(mod);

    auto get_string = python::getattr<std::optional<std::string>(std::string)>(mod, "get_string");

    auto str = get_string("null"s);
    BOOST_TEST(!str);

    str = get_string("not-null"s);
    BOOST_TEST(!!str);
    BOOST_TEST("not-null"s == *str);
    BOOST_TEST(str->size() == "not-null"s.size());
}

BOOST_AUTO_TEST_CASE(Invoke)
{
    using namespace std::string_literals;

    auto mod = python::import("test_simple_optional");
    auto get_string = python::getattr<std::string(std::optional<std::string>)>(mod, "get_string");

    auto str = get_string(std::nullopt);
    BOOST_TEST("null"s == str);
}

BOOST_AUTO_TEST_CASE(List)
{
    using namespace std::string_literals;

    auto mod = python::import("test_simple_optional");
    auto get_list = python::getattr<std::vector<std::optional<std::string>>()>(mod, "get_vector");

    auto v =  get_list();
    BOOST_TEST(3 == v.size());
    BOOST_TEST(!!v[0]);
    BOOST_TEST("foo"s == *(v[0]));
    BOOST_TEST(3 == v[0]->size());

    BOOST_TEST(!!v[2]);
    BOOST_TEST("bar"s == *(v[2]));
    BOOST_TEST(3 == v[2]->size());

    BOOST_TEST(!v[1]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ReturnIntegral, T, python::tester::Integers)
{
    using namespace std::string_literals;

    auto mod = python::import("test_simple_optional");
    auto get_int = python::getattr<std::optional<T>(std::string)>(mod, "get_integer");

    auto v = get_int("null"s);
    BOOST_TEST(!v);

    v = get_int("foobar"s);
    BOOST_TEST(6 == *v);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(InvokeIntegral, T, python::tester::Integers)
{
    using namespace std::string_literals;

    auto mod = python::import("test_simple_optional");
    auto get_int = python::getattr<T(std::optional<T>)>(mod, "get_integer");

    auto v = get_int(std::nullopt);
    BOOST_TEST(42 == v);
}

BOOST_AUTO_TEST_SUITE_END()
