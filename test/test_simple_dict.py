def integers(n):
    return dict((k, str(k) + "-test") for k in range(n))

def strings(n):
    return dict((str(k) + "-key", str(k) + "-value") for k in range(n))

def check_integers(d):
    for i in range(len(d)):
        if str(i) + "-value" not in d:
            return False
        if d[str(i) + "-value"] != i:
            return False
    return True

def check_strings(d):
    for i in range(len(d)):
        if str(i) + "-key" not in d:
            return False
        if d[str(i) + "-key"] != str(i) + "-value":
            return False
    return True
