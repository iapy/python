#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(ClassOperators, python::tester::Test)

PYTHON_MODULE_HEADER(Impl,
(
    PYTHON_CLASS(LT, (
        (int(), lt_called),
        (bool(LT const &), __lt__)
    )),
    PYTHON_CLASS(GT, (
        (int(), gt_called),
        (bool(GT const &), __gt__)
    )),
    PYTHON_CLASS(LE, (
        (int(), le_called),
        (bool(LE const &), __le__)
    )),
    PYTHON_CLASS(GE, (
        (int(), ge_called),
        (bool(GE const &), __ge__)
    )),
    PYTHON_CLASS(EQ, (
        (int(), eq_called),
        (bool(EQ const &), __eq__)
    )),
    PYTHON_CLASS(NE, (
        (int(), ne_called),
        (bool(NE const &), __ne__)
    )),
    PYTHON_CLASS(NEQ, (
        (int(), ne_called),
        (int(), eq_called),
        (bool(NEQ const &), __eq__),
        (bool(NEQ const &), __ne__)
    ))
),
(
    (class LT(std::string), LT),
    (class GT(std::string), GT),
    (class LE(std::string), LE),
    (class GE(std::string), GE),
    (class EQ(std::string), EQ),
    (class NE(std::string), NE),
    (class NEQ(std::string), NEQ)
));

BOOST_AUTO_TEST_CASE(TestLT)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::cast<Impl>(python::import("test_simple_class_operators"));
    auto a = mod.LT("bar"s);
    auto b = mod.LT("foo"s);

    BOOST_TEST(a < b);
    BOOST_TEST(!(b < a));
    BOOST_TEST(b >= a);
    BOOST_TEST(!(a >= b));

    BOOST_TEST(4 == a.lt_called());
    BOOST_TEST(4 == b.lt_called());
}

BOOST_AUTO_TEST_CASE(TestGT)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::cast<Impl>(python::import("test_simple_class_operators"));
    auto a = mod.GT("bar"s);
    auto b = mod.GT("foo"s);

    BOOST_TEST(b > a);
    BOOST_TEST(!(a > b));
    BOOST_TEST(a <= b);
    BOOST_TEST(!(b <= a));

    BOOST_TEST(4 == a.gt_called());
    BOOST_TEST(4 == b.gt_called());
}

BOOST_AUTO_TEST_CASE(TestLE)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::cast<Impl>(python::import("test_simple_class_operators"));
    auto a = mod.LE("bar"s);
    auto b = mod.LE("foo"s);

    BOOST_TEST(b > a);
    BOOST_TEST(!(a > b));
    BOOST_TEST(a <= b);
    BOOST_TEST(!(b <= a));

    BOOST_TEST(4 == a.le_called());
    BOOST_TEST(4 == b.le_called());
}

BOOST_AUTO_TEST_CASE(TestGE)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::cast<Impl>(python::import("test_simple_class_operators"));
    auto a = mod.GE("bar"s);
    auto b = mod.GE("foo"s);

    BOOST_TEST(b >= a);
    BOOST_TEST(!(a >= b));
    BOOST_TEST(a < b);
    BOOST_TEST(!(b < a));

    BOOST_TEST(4 == a.ge_called());
    BOOST_TEST(4 == b.ge_called());
}

BOOST_AUTO_TEST_CASE(TestEQ)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::cast<Impl>(python::import("test_simple_class_operators"));
    auto a = mod.EQ("bar"s);
    auto b = mod.EQ("bar"s);

    BOOST_TEST(a == b);
    BOOST_TEST(!(a != b));

    BOOST_TEST(2 == a.eq_called());
    BOOST_TEST(2 == b.eq_called());
}

BOOST_AUTO_TEST_CASE(TestNE)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::cast<Impl>(python::import("test_simple_class_operators"));
    auto a = mod.NE("foo"s);
    auto b = mod.NE("bar"s);

    BOOST_TEST(a != b);
    BOOST_TEST(!(a == b));

    BOOST_TEST(2 == a.ne_called());
    BOOST_TEST(2 == b.ne_called());
}

BOOST_AUTO_TEST_CASE(TestNEQ)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::cast<Impl>(python::import("test_simple_class_operators"));
    auto a = mod.NEQ("foo"s);
    auto b = mod.NEQ("bar"s);

    BOOST_TEST(a != b);
    BOOST_TEST(!(a == b));

    BOOST_TEST(1 == a.eq_called());
    BOOST_TEST(1 == b.ne_called());
}

BOOST_AUTO_TEST_SUITE_END()
