import test_metacg_thread_long_c

GLOBAL_DICT = dict()

def get_dict():
    global GLOBAL_DICT
    return GLOBAL_DICT

def add_value(a, b):
    global GLOBAL_DICT
    GLOBAL_DICT[a] = test_metacg_thread_long_c.calculate_value(a, b)
