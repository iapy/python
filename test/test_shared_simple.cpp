#include <python/metacg.hpp>
#include <python/shared.hpp>

struct TestSharedSimple : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_IMPORT(json, (
            (python::str(python::obj), dumps)
        ))
        PYTHON_EXPORT(test_shared_simple);
    };

    struct Module
    {
        template<typename Base>
        struct Interface : Base
        {
            void init(python::moddef &mod)
            {
                mod.def("get_string", python::bind<&Interface<Base>::get_string>);
            }

        private:
            python::str get_string(python::obj obj)
            {
                return python::import(this).dumps(obj);
            }
        };
    };

    using Ports = ct::map<python::module<TestSharedSimple>>;
};

PyMODINIT_FUNC PyInit_test_shared_simple()
{
    return python::shared<TestSharedSimple>();
}
