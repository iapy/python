#include <python/common.hpp>
#include "lib/framework.hpp"
#include <optional>

BOOST_FIXTURE_TEST_SUITE(With, python::tester::Test)

PYTHON_MODULE_HEADER(Tempfile,
(
    PYTHON_CLASS(TemporaryDirectory)
),
(
    (class TemporaryDirectory(), TemporaryDirectory)
));

PYTHON_MODULE_HEADER(Exceptions,
(
    PYTHON_CLASS(Test, (
        (void(std::string, bool), test),
        (bool(), is_entered),
        (bool(), is_exited)
    ))
),
(
    (class Test(), Test)
));

BOOST_AUTO_TEST_CASE(TempfileTest)
{
    auto m = python::cast<Tempfile>(python::import("tempfile"));

    std::optional<std::filesystem::path> p;
    python::with(m.TemporaryDirectory(), [&p](auto &&tmp){
        p = std::filesystem::path{python::cast<std::string>(tmp)};
        BOOST_TEST(std::filesystem::exists(*p));
        BOOST_TEST(std::filesystem::is_directory(*p));
    });

    BOOST_TEST(!!p);
    BOOST_TEST(!std::filesystem::exists(*p));
}

static bool check_exception(python::exception const &ex)
{
    return ex.type() == "Exception" && ex.what() == "Exception('me')";
}

BOOST_AUTO_TEST_CASE(CustomTest)
{
    using namespace std::string_literals;
    auto m = python::cast<Exceptions>(python::import("test_simple_with"));

    auto d = m.Test();
    d.test("foo"s, false);
    BOOST_TEST(!d.is_entered());
    BOOST_TEST(!d.is_exited());

    python::with(d, [&d](auto){
        d.test("bar"s, false);
    });
    BOOST_TEST(d.is_entered());
    BOOST_TEST(d.is_exited());

    BOOST_TEST(!d.is_entered());
    BOOST_TEST(!d.is_exited());
    BOOST_REQUIRE_EXCEPTION(python::with(d, [&d](auto){
        d.test("me"s, true);
    }), python::exception, check_exception);
    BOOST_TEST(d.is_entered());
    BOOST_TEST(d.is_exited());

    BOOST_TEST(!d.is_entered());
    BOOST_TEST(!d.is_exited());
    BOOST_REQUIRE_EXCEPTION(python::with(d, [](auto &&d){
        python::cast<class Exceptions::Test>(d).test("me"s, true);
    }), python::exception, check_exception);
    BOOST_TEST(d.is_entered());
    BOOST_TEST(d.is_exited());
}

BOOST_AUTO_TEST_SUITE_END()
