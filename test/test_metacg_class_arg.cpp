#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ClassArg, python::tester::Metacg)
using namespace std::string_literals;

struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_EXPORT(test_metacg_class_arg_c);
        PYTHON_IMPORT(test_metacg_class_arg,
        (
            PYTHON_CLASS(A, (
                (std::string(), get_value)
            ))
        ),
        (
            (class A(), make_proxy),
            (class A(std::string), A),
            (std::string(), get_value0),
            (std::string(class A), get_value1)
        ));
    };

    struct Module
    {
        template<typename Base>
        struct Interface : Base
        {
            void init(python::moddef &mod)
            {
                using Self = Interface<Base>;
                mod.def("make", python::bind<&Self::make>);
                mod.def("get_value", python::bind<&Self::get_value>);
            }

            std::string get_value(typename Base::Module::A a)
            {
                return "cpp-" + std::string{a.get_value()};
            }

            typename Base::Module::A make(python::str name)
            {
                auto module = python::import(this);
                return module.A(name);
            }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                using namespace std::string_literals;
                python::lock([this]{
                    auto module = python::import(this);
                    BOOST_TEST("cpp-value0"s == module.get_value0());

                    auto a = module.A("value1"s);
                    BOOST_TEST("cpp-value1"s == module.get_value1(a));
                    BOOST_TEST("cpp-value2"s == module.get_value1(module.A("value2"s)));

                    auto b = module.make_proxy();
                    BOOST_TEST("py-value"s == b.get_value());
                    BOOST_TEST("cpp-py-value"s == module.get_value1(b));
                });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
}

BOOST_AUTO_TEST_SUITE_END()
