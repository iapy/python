#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(ClassArg, python::tester::Test)

PYTHON_MODULE_HEADER(Impl,
(
    PYTHON_CLASS(A, (
        (std::string(), get_value)
    ))
),
(
    (class A(std::string), A),
    (std::string(class A), get_value)
));

BOOST_AUTO_TEST_CASE(TestPassInstance)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::cast<Impl>(python::import("test_simple_class_arg"));

    auto a = mod.A("my instance"s);
    BOOST_TEST("my instance"s == a.get_value());

    auto v = mod.get_value(a);
    BOOST_TEST("my instance"s == v);
}

BOOST_AUTO_TEST_SUITE_END()
