#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(ThreadLong, python::tester::Metacg)

std::mutex mutex;
std::set<std::size_t> sizes;

struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_EXPORT(test_metacg_thread_long_c);
        PYTHON_IMPORT(test_metacg_thread_long, (
            (python::dict<int, python::str>(), get_dict),
            (void(int, int), add_value)
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                using Self = Interface<Base>;
                mod.def("calculate_value", python::bind<&Self::calculate_value>);
            }

            python::str calculate_value(int x, int y)
            {
                return std::to_string(x) + ":" + std::to_string(y);
            }
        };
    };

    template<int I>
    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                for(std::size_t i = 0; i < this->state.count; ++i)
                {
                    python::lock([i, this](){
                        python::import(this).add_value(i, I);
                    });
                }

                std::set<int> tids;
                std::set<int> vals;
                python::lock([&tids, &vals, this]{
                    auto d = python::import(this).get_dict();
                    for(auto [k, v] : d)
                    {
                        auto p = v.find(':');
                        BOOST_TEST(p != std::string::npos);
                        tids.insert(std::atoi(v.c_str() + p + 1));
                        vals.insert(k);
                    }
                });
                BOOST_TEST(1 <= tids.size());
                BOOST_TEST(this->state.count == vals.size());

                std::unique_lock<std::mutex> lock(mutex);
                sizes.insert(tids.size());
                return 0;
            }
        };
    };

    template<typename>
    struct State
    {
        int const count;
    };

    using Services = ct::tuple<Main<0>, Main<1>, Main<2>, Main<3>, Main<4>>;
    using Ports = ct::map<python::module<Component>>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>(
        cg::Args<Component>(102400)
    );

    bool ok{false};
    for(auto s : sizes)
    {
        if(s > 2) ok = true;
    }
    BOOST_TEST(ok);
}

BOOST_AUTO_TEST_SUITE_END()
