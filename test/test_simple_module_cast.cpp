#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(ModuleCast, python::tester::Test)

BOOST_AUTO_TEST_CASE(CastToFunction)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;
    auto mod = python::import("test_simple_module_cast");

    auto fun = python::cast<std::string(std::vector<std::string>)>(mod);
    BOOST_TEST("abracadabra"sv == fun(std::vector<std::string>{
        "abra"s, "cadabra"s
    }));

    BOOST_TEST("abracadabra"sv == fun(python::list<std::string>{
        "abraca"s, "dabra"s
    }));
}

BOOST_AUTO_TEST_SUITE_END()

