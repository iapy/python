#include <python/common.hpp>
#include "lib/framework.hpp"
#include <thread>

BOOST_FIXTURE_TEST_SUITE(Thread, python::tester::ThreadTest)

static bool check_exception(std::exception const &ex)
{
    return !std::strcmp(ex.what(), "Exception");
}

PYTHON_MODULE_HEADER(Impl, (
    (std::string(void), get),
    (void(std::string), set),
    (void(int), fail)
));

BOOST_AUTO_TEST_CASE(Smoke)
{
    using namespace std::string_literals;

    auto m = python::lock([]{
        return python::cast<Impl>(python::import("test_thread_lock"));
    });
    auto t1 = std::thread([&]{
        for(std::size_t i = 0; i <= 100; ++i)
        {
            python::lock([&]{
                m.set(std::to_string(i));
            });
        }
    });
    auto t2 = std::thread([&]{
        for(std::size_t i = 0; i <= 100; ++i)
        {
            python::lock([&]{
                auto value = m.get();
            });
        }
    });

    t1.join();
    t2.join();

    auto v = python::lock([&]{
        return m.get();
    });
    BOOST_TEST("100"s == v);
}

BOOST_AUTO_TEST_CASE(Throw)
{
    auto m = python::lock([]{
        return python::cast<Impl>(python::import("test_thread_lock"));
    });
    auto t1 = std::thread([&]{
        BOOST_REQUIRE_EXCEPTION(python::lock([&]{
            m.fail(1);
        }), std::runtime_error, check_exception);
    });
    auto t2 = std::thread([&]{
        BOOST_REQUIRE_EXCEPTION(python::lock([&]{
            m.fail(2);
        }), std::runtime_error, check_exception);
    });
    t1.join();
    t2.join();
}

BOOST_AUTO_TEST_SUITE_END()
