#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(Define, python::tester::Metacg)
using namespace std::string_literals;

struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_DEFINE(defined, R"(
            from test_metacg_simple import test
            from test_metacg_define import func
        )");

        PYTHON_IMPORT(defined, (
            (std::string(), test),
            (std::string(), func)
        ));
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                using namespace std::string_literals;
                python::lock([this]{
                    auto module = python::import(this);
                    BOOST_TEST("works"s == module.test());
                    BOOST_TEST("indeed"s == module.func());
                });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
}

BOOST_AUTO_TEST_SUITE_END()
