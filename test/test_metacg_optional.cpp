#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(Optional, python::tester::Metacg)
static bool check_type_error(python::exception const &ex)
{
    return ex.type() == "TypeError" && ex.what() == "'Insufficient number of arguments'";
}

using namespace std::string_literals;
struct Component : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_EXPORT(test_metacg_optional_c);
        PYTHON_IMPORT(test_metacg_optional, (
            (std::string(bool), sum),
            (std::string(bool), sum_opt),
            (std::uint32_t(bool), sum_int),
            (std::uint32_t(bool), sum_int_opt),
            (std::string(bool), get)
        ));
    };

    struct Module 
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                using Self = Interface<Base>;
                mod.def("sum", python::bind<&Self::sum>);
                mod.def("sum_opt", python::bind<&Self::sum_opt>);
                mod.def("sum_int", python::bind<&Self::sum_int>);
                mod.def("sum_int_opt", python::bind<&Self::sum_int_opt>);
                mod.def("get", python::bind<&Self::get>);
            }

            std::string sum(python::str a, python::str b)
            {
                return std::string(a) + std::string(b);
            }

            std::string sum_opt(python::str a, python::optional<python::str> b)
            {
                if(!b) return a;
                return std::string(a) + std::string(*b);
            }

            int sum_int(int a, int b)
            {
                return a + b;
            }

            int sum_int_opt(int a, python::optional<int> b)
            {
                if(!b) return a;
                return a + *b;
            }

            std::optional<std::string> get(bool v)
            {
                if(v) return std::nullopt;
                return std::optional<std::string>("test");
            }
        };
    };

    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([module=python::import(this)]{
                    BOOST_TEST("foobar"s == module.sum(true));
                    BOOST_REQUIRE_EXCEPTION(module.sum(false), python::exception, check_type_error);
                    BOOST_TEST("foobar"s == module.sum_opt(true));
                    BOOST_TEST("foo"s == module.sum_opt(false));
                    BOOST_TEST(42 == module.sum_int(true));
                    BOOST_REQUIRE_EXCEPTION(module.sum_int(false), python::exception, check_type_error);
                    BOOST_TEST(42 == module.sum_int_opt(true));
                    BOOST_TEST(24 == module.sum_int_opt(false));
                    BOOST_TEST("null"s == module.get(true));
                    BOOST_TEST("test"s == module.get(false));
                });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Main>;
    using Ports = ct::map<python::module<Component>>;
};

BOOST_AUTO_TEST_CASE(Smoke)
{
    test_graph<python::tester::ModulesGraph<Component>>();
}

BOOST_AUTO_TEST_SUITE_END()
