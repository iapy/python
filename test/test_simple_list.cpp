#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(List, python::tester::Test)

static bool is_out_of_range(python::exception const &ex)
{
    return ex.type() == "IndexError" && ex.what() == "'list index out of range'";
}
static bool is_out_of_range_assignment(python::exception const &ex)
{
    return ex.type() == "IndexError" && ex.what() == "'list assignment index out of range'";
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ReturnIntegral, T, python::tester::Integers)
{
    auto mod = python::import("test_simple_list");
    BOOST_TEST(mod);

    auto get_int = python::getattr<std::vector<T>(std::size_t)>(mod, "integers");

    auto ints = get_int(5);
    BOOST_TEST(ints);
    BOOST_TEST(5 == ints.size());

    for(std::size_t i = 0; i < ints.size(); ++i)
    {
        BOOST_TEST(i == ints[i]);
        ints.set(i, ints.size() - i);
    }

    for(std::size_t i = 0; i < ints.size(); ++i)
    {
        BOOST_TEST((ints.size() - i) == ints[i]);
    }

    auto check_int = python::getattr<bool(std::vector<T>)>(mod, "integers_check");
    BOOST_TEST(check_int(ints));

    ints.insert(0, ints.size() + 1);
    BOOST_TEST(check_int(ints));

    ints.insert(0, ints.size() + 1);
    BOOST_TEST(check_int(ints));

    BOOST_TEST("7654321" == [&ints]{
        std::ostringstream ss;
        for(T const value : ints)
        {
            ss << value;
        }
        return ss.str();
    }());

    BOOST_TEST("76543217654321" == [&ints]{
        std::ostringstream ss;
        for(auto it = std::begin(ints); it != std::end(ints); ++it)
        {
            ss << *it;
        }
        for(auto it = std::begin(ints); it != std::end(ints); it++)
        {
            ss << *it;
        }
        return ss.str();
    }());

    BOOST_TEST("1234567" == [&ints]{
        std::ostringstream ss;
        for(auto it = std::rbegin(ints); it != std::rend(ints); ++it)
        {
            ss << *it;
        }
        return ss.str();
    }());

    python::list<T> alist(10);
    for(std::size_t i = 0; i < 10; ++i)
    {
        alist.set(i, 10 - i);
    }

    BOOST_TEST(check_int(alist));

    alist = python::list<T>();
    BOOST_TEST(alist);
    BOOST_TEST(0 == alist.size());

    alist.insert(0, 2);
    alist.insert(0, 4);
    alist.insert(1, 3);
    alist.append(1);
    BOOST_TEST(4 == alist.size());
    BOOST_TEST(check_int(alist));

    python::list<T> blist{4, 3, 2, 1};
    BOOST_TEST(blist);
    BOOST_TEST(check_int(blist));

    BOOST_REQUIRE_EXCEPTION(blist[5], python::exception, is_out_of_range);
    BOOST_REQUIRE_EXCEPTION(blist.set(10, 0), python::exception, is_out_of_range_assignment);

    blist.reverse();
    for(std::size_t i = 0; i < blist.size(); ++i)
    {
        BOOST_TEST((i + 1) == blist[i]);
    }

    ints.reverse();

    check_int = python::getattr<bool(std::vector<T>)>(mod, "integers_check_reversed");
    BOOST_TEST(check_int(ints));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ModifyIntegral, T, python::tester::Integers)
{
    auto mod = python::import("test_simple_list");
    BOOST_TEST(mod);

    auto get_int = python::getattr<std::vector<T>(std::size_t)>(mod, "integers");
    auto ints = get_int(2);

    auto add_int = python::getattr<void(std::vector<T>, T)>(mod, "add_list");
    add_int(ints, T{42});

    BOOST_TEST(3 == ints.size());
    BOOST_TEST(42 == ints[2]);

    for(auto it = std::begin(ints); it != std::end(ints); ++it)
    {
        it = (*it + 1);
    }

    auto check_equal = python::getattr<bool(std::vector<T>, std::vector<T>)>(mod, "check_equal");
    BOOST_TEST(check_equal(ints, std::vector<T>{T{1}, T{2}, T{43}}));
}

BOOST_AUTO_TEST_CASE(ReturnString)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_list");
    BOOST_TEST(mod);

    auto get_str = python::getattr<std::vector<std::string>(std::size_t)>(mod, "strings");

    auto strings = get_str(10);
    BOOST_TEST(strings);
    BOOST_TEST(10 == strings.size());

    for(std::size_t i = 0; i < 10; ++i)
    {
        BOOST_TEST("foobar"sv == strings[i]);
        if(i < 5) strings.set(i, "barfoo"s);
    }

    auto check_str = python::getattr<bool(python::list<std::string>)>(mod, "strings_check");
    BOOST_TEST(check_str(strings));

    strings.insert(0, "barfoo"s);
    strings.append("foobar"s);
    BOOST_TEST(12 == strings.size());
    BOOST_TEST(check_str(strings));

    strings.reverse();
    check_str = python::getattr<bool(python::list<std::string>)>(mod, "strings_check_reversed");
    BOOST_TEST(check_str(strings));

    BOOST_REQUIRE_EXCEPTION(strings[20], python::exception, is_out_of_range);
    BOOST_REQUIRE_EXCEPTION(strings.set(20, "garbage"s), python::exception, is_out_of_range_assignment);
}

BOOST_AUTO_TEST_CASE(ReturnBoolean)
{
    auto mod = python::import("test_simple_list");
    BOOST_TEST(mod);

    auto get_booleans = python::getattr<std::vector<bool>()>(mod, "booleans");

    auto booleans = get_booleans();
    BOOST_TEST(2 == booleans.size());
    BOOST_TEST(booleans[0]);
    BOOST_TEST(!booleans[1]);

    booleans.set(0, false);
    auto check_booleans = python::getattr<bool(std::vector<bool>, bool)>(mod, "booleans_check");
    BOOST_TEST(check_booleans(booleans, false));

    booleans.set(0, true);
    booleans.set(1, true);
    booleans.append(true);
    BOOST_TEST(check_booleans(booleans, true));

    booleans.set(2, false);
    BOOST_TEST(3 == booleans.size());
    BOOST_TEST(!check_booleans(booleans, true));
    BOOST_TEST(!check_booleans(booleans, false));
}

BOOST_AUTO_TEST_CASE(ModifyString)
{
    using namespace std::string_literals;
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_list");
    BOOST_TEST(mod);

    auto get_str = python::getattr<std::vector<std::string>(std::size_t)>(mod, "strings");
    auto strings = get_str(1);

    auto add_str = python::getattr<void(std::vector<std::string>, std::string)>(mod, "add_list");
    add_str(strings, "test"s);

    BOOST_TEST(2 == strings.size());
    BOOST_TEST("foobar"sv == strings[0]);
    BOOST_TEST("test"sv == strings[1]);
}

BOOST_AUTO_TEST_CASE(ModifyBoolean)
{
    auto mod = python::import("test_simple_list");
    BOOST_TEST(mod);

    auto get_booleans = python::getattr<std::vector<bool>()>(mod, "booleans");

    auto booleans = get_booleans();
    BOOST_TEST(2 == booleans.size());

    auto add_booleans = python::getattr<void(std::vector<bool>, bool)>(mod, "add_list");
    add_booleans(booleans, false);

    BOOST_TEST(3 == booleans.size());
    BOOST_TEST(booleans[0]);
    BOOST_TEST(!booleans[1]);
    BOOST_TEST(!booleans[2]);
}

BOOST_AUTO_TEST_SUITE_END()
