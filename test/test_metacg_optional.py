import test_metacg_optional_c

def get(x):
    y = test_metacg_optional_c.get(x)
    return "null" if not y else y

def sum(x):
    if x:
        return test_metacg_optional_c.sum("foo", "bar")
    return test_metacg_optional_c.sum("foo")

def sum_opt(x):
    if x:
        return test_metacg_optional_c.sum_opt("foo", "bar")
    return test_metacg_optional_c.sum_opt("foo")

def sum_int(x):
    if x:
        return test_metacg_optional_c.sum_int(12, 30)
    return test_metacg_optional_c.sum_int(42)

def sum_int_opt(x):
    if x:
        return test_metacg_optional_c.sum_int(12, 30)
    return test_metacg_optional_c.sum_int_opt(24)

