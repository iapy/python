#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(Functions, python::tester::Test)

BOOST_AUTO_TEST_CASE_TEMPLATE(ReturnDouble, T, python::tester::Doubles)
{
    auto mod = python::import("test_simple_functions");
    BOOST_TEST(mod);

    auto get_double = python::getattr<T()>(mod, "get_double");
    BOOST_TEST(1.5 == get_double());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(PassDouble, T, python::tester::Doubles)
{
    auto mod = python::import("test_simple_functions");
    BOOST_TEST(mod);

    auto add = python::getattr<T(T, T)>(mod, "add");
    BOOST_TEST(T{10.5f} == add(T{2.6}, T{7.9}));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ReturnInteger, T, python::tester::Integers)
{
    auto mod = python::import("test_simple_functions");
    BOOST_TEST(mod);

    auto get_int = python::getattr<T()>(mod, "get_integer");
    BOOST_TEST(42 == get_int());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(PassInteger, T, python::tester::Integers)
{
    auto mod = python::import("test_simple_functions");

    auto add_int = python::getattr<T(T, T)>(mod, "add");
    BOOST_TEST(T{42} == add_int(T{12}, T{30}));
}

BOOST_AUTO_TEST_CASE(ReturnBoolesn)
{
    auto mod = python::import("test_simple_functions");
    BOOST_TEST(mod);

    auto get_true = python::getattr<bool()>(mod, "get_true");
    BOOST_TEST(true == get_true());

    auto get_false = python::getattr<bool()>(mod, "get_false");
    BOOST_TEST(false == get_false());
}

BOOST_AUTO_TEST_CASE(PassBoolesn)
{
    auto mod = python::import("test_simple_functions");
    BOOST_TEST(mod);

    auto negate = python::getattr<bool(bool)>(mod, "negate");
    BOOST_TEST(true == negate(false));
    BOOST_TEST(false == negate(true));
}

BOOST_AUTO_TEST_CASE(ReturnString)
{
    using namespace std::string_literals;

    auto mod = python::import("test_simple_functions");
    BOOST_TEST(mod);

    auto get_string = python::getattr<std::string()>(mod, "get_string");
    BOOST_TEST("foo"s == [&]{
        std::ostringstream ss;
        ss << get_string();
        return ss.str();
    }());

    auto get_str = python::getattr<python::str()>(mod, "get_string");
    BOOST_TEST("foo"s == [&]{
        std::ostringstream ss;
        ss << get_str();
        return ss.str();
    }());

    static_assert(std::is_same_v<decltype(get_string()), decltype(get_str())>);
}

BOOST_AUTO_TEST_CASE(PassString)
{
    using namespace std::string_literals;

    auto mod = python::import("test_simple_functions");
    BOOST_TEST(mod);

    auto add_string = python::getattr<std::string(std::string, std::string)>(mod, "add");
    BOOST_TEST("foobar"s == [&]{
        std::ostringstream ss;
        ss << add_string("foo"s, "bar"s);
        return ss.str();
    }());

    auto result = add_string("ab"s, "cd"s);
    auto add_str = python::getattr<python::str(std::string, std::string)>(mod, "add");
    BOOST_TEST("abcdabcd"s == [&]{
        std::ostringstream ss;
        ss << add_str(result, result);
        return ss.str();
    }());

    auto add_strstr = python::getattr<python::str(python::str, python::str)>(mod, "add");
    BOOST_TEST("abcdabcd"s == [&]{
        std::ostringstream ss;
        ss << add_strstr(result, result);
        return ss.str();
    }());

    BOOST_TEST("abccba"s == [&]{
        std::ostringstream ss;
        ss << add_strstr("abc"s, "cba"s);
        return ss.str();
    }());

    static_assert(std::is_same_v<decltype(result), python::str>);
}

BOOST_AUTO_TEST_SUITE_END()
