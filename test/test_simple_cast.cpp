#include <python/common.hpp>
#include "lib/framework.hpp"

BOOST_FIXTURE_TEST_SUITE(Cast, python::tester::Test)

BOOST_AUTO_TEST_CASE(List)
{
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_cast");
    BOOST_TEST(mod);

    auto get_list = python::getattr<python::object()>(mod, "get_list");

    auto list1 = static_cast<python::list<std::string>>(get_list());
    BOOST_TEST(2 == list1.size());
    BOOST_TEST("foo"sv == list1[0]);
    BOOST_TEST("bar"sv == list1[1]);

    auto list2 = python::cast<python::list<std::string>>(get_list());
    BOOST_TEST(2 == list2.size());
    BOOST_TEST("foo"sv == list2[0]);
    BOOST_TEST("bar"sv == list2[1]);

    python::object o = get_list();

    list1 = static_cast<python::list<std::string>>(o);
    BOOST_TEST(2 == list1.size());
    BOOST_TEST("foo"sv == list1[0]);
    BOOST_TEST("bar"sv == list1[1]);

    list2 = python::cast<python::list<std::string>>(o);
    BOOST_TEST(2 == list2.size());
    BOOST_TEST("foo"sv == list2[0]);
    BOOST_TEST("bar"sv == list2[1]);
}

BOOST_AUTO_TEST_CASE(ListList)
{
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_cast");
    BOOST_TEST(mod);

    auto get_list = python::getattr<python::object()>(mod, "get_list_list");

    auto list1 = static_cast<python::list<python::list<std::string>>>(get_list());
    BOOST_TEST(2 == list1.size());
    BOOST_TEST(2 == list1[0].size());
    BOOST_TEST("foo"sv == list1[0][0]);
    BOOST_TEST("bar"sv == list1[0][1]);
    BOOST_TEST(3 == list1[1].size());
    BOOST_TEST("a"sv == list1[1][0]);
    BOOST_TEST("b"sv == list1[1][1]);
    BOOST_TEST("c"sv == list1[1][2]);

    auto list2 = python::cast<python::list<python::list<std::string>>>(get_list());
    BOOST_TEST(2 == list2.size());
    BOOST_TEST(2 == list2[0].size());
    BOOST_TEST("foo"sv == list2[0][0]);
    BOOST_TEST("bar"sv == list2[0][1]);
    BOOST_TEST(3 == list2[1].size());
    BOOST_TEST("a"sv == list2[1][0]);
    BOOST_TEST("b"sv == list2[1][1]);
    BOOST_TEST("c"sv == list2[1][2]);

    python::object o = get_list();
    list1 = static_cast<python::list<python::list<std::string>>>(o);

    BOOST_TEST(2 == list1.size());
    BOOST_TEST(2 == list1[0].size());
    BOOST_TEST("foo"sv == list1[0][0]);
    BOOST_TEST("bar"sv == list1[0][1]);
    BOOST_TEST(3 == list1[1].size());
    BOOST_TEST("a"sv == list1[1][0]);
    BOOST_TEST("b"sv == list1[1][1]);
    BOOST_TEST("c"sv == list1[1][2]);

    list2 = python::cast<python::list<python::list<std::string>>>(get_list());
    BOOST_TEST(2 == list2.size());
    BOOST_TEST(2 == list2[0].size());
    BOOST_TEST("foo"sv == list2[0][0]);
    BOOST_TEST("bar"sv == list2[0][1]);
    BOOST_TEST(3 == list2[1].size());
    BOOST_TEST("a"sv == list2[1][0]);
    BOOST_TEST("b"sv == list2[1][1]);
    BOOST_TEST("c"sv == list2[1][2]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ListMixed, T, python::tester::Integers)
{
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_cast");
    BOOST_TEST(mod);

    auto get_list = python::getattr<python::list<python::object>()>(mod, "get_list_mixed");
    
    auto list = get_list();
    BOOST_TEST(3 == list.size());

    auto list1 = static_cast<python::list<std::string>>(list[0]);
    BOOST_TEST(2 == list1.size());
    BOOST_TEST("foo"sv == list1[0]);
    BOOST_TEST("bar"sv == list1[1]);

    auto list2 = python::cast<python::list<std::string>>(list[0]);
    BOOST_TEST(2 == list2.size());
    BOOST_TEST("foo"sv == list2[0]);
    BOOST_TEST("bar"sv == list2[1]);

    auto str1 = static_cast<python::str>(list[1]);
    BOOST_TEST("a"sv == str1);

    auto str2 = python::cast<python::str>(list[1]);
    BOOST_TEST("a"sv == str2);

    BOOST_TEST(42 == python::cast<T>(list[2]));
}

BOOST_AUTO_TEST_CASE(String)
{
    using namespace std::string_view_literals;

    auto mod = python::import("test_simple_cast");
    BOOST_TEST(mod);

    auto get_string = python::getattr<python::object()>(mod, "get_string");
    auto str1 = static_cast<python::str>(get_string());
    BOOST_TEST("foo"sv == str1);

    auto str2 = python::cast<python::str>(get_string());
    BOOST_TEST("foo"sv == str2);

    python::object o = get_string();
    str1 = static_cast<python::str>(o);
    BOOST_TEST("foo"sv == str1);

    str2 = python::cast<python::str>(o);
    BOOST_TEST("foo"sv == str2);
}

BOOST_AUTO_TEST_SUITE_END()
