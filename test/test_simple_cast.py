def get_string():
    return "foo"

def get_list():
    return ["foo", "bar"]

def get_list_list():
    return [get_list(), ['a', 'b', 'c']]

def get_list_mixed():
    return [get_list(), "a", 42]
