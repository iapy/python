#include <python/common.hpp>
#include "lib/framework.hpp"

#include <string>

BOOST_FIXTURE_TEST_SUITE(Vargs, python::tester::Test)

BOOST_AUTO_TEST_CASE_TEMPLATE(List, T, python::tester::Integers)
{
    auto m = python::import("test_simple_vargs");
    auto test_sum_00 = python::getattr<T(python::args)>(m, "test_sum_0");
    BOOST_TEST(10 == test_sum_00(*python::list<T>{
        T{1}, T{2}, T{3}, T{4}
    }));
    BOOST_TEST(10 == test_sum_00(T{1}, T{2}, T{3}, T{4}));

    auto test_sum_01 = python::getattr<T(T, python::args)>(m, "test_sum_0");
    BOOST_TEST(10 == test_sum_01(T{1}, *python::list<T>{
        T{2}, T{3}, T{4}
    }));
    BOOST_TEST(10 == test_sum_01(T{1}, T{2}, T{3}, T{4}));
    BOOST_TEST(1 == test_sum_01(T{1}));

    auto test_sum_02 = python::getattr<T(T, T, python::args)>(m, "test_sum_0");
    BOOST_TEST(10 == test_sum_02(T{1}, T{2}, *python::list<T>{
        T{3}, T{4}
    }));
    BOOST_TEST(10 == test_sum_02(T{1}, T{2}, T{3}, T{4}));
    BOOST_TEST(3 == test_sum_02(T{1}, T{2}));

    auto test_sum_03 = python::getattr<T(T, T, T, python::args)>(m, "test_sum_0");
    BOOST_TEST(10 == test_sum_03(T{1}, T{2}, T{3}, *python::list<T>{
        T{4}
    }));
    BOOST_TEST(6 == test_sum_03(T{1}, T{2}, T{3}));

    auto test_sum_04 = python::getattr<T(T, T, T, T)>(m, "test_sum_0");
    BOOST_TEST(10 == test_sum_04(T{1}, T{2}, T{3}, T{4}));

    auto test_sum_10 = python::getattr<T(python::args)>(m, "test_sum_1");
    BOOST_TEST(10 == test_sum_10(T{1}, T{2}, T{3}, T{4}));
    BOOST_TEST(10 == test_sum_10(*python::list<T>{
        T{1}, T{2}, T{3}, T{4}
    }));

    auto test_sum_11 = python::getattr<T(T, python::args)>(m, "test_sum_1");
    BOOST_TEST(10 == test_sum_11(T{1}, T{2}, T{3}, T{4}));
    BOOST_TEST(10 == test_sum_11(T{1}, *python::list<T>{
        T{2}, T{3}, T{4}
    }));

    auto test_sum_12 = python::getattr<T(T, T, python::args)>(m, "test_sum_1");
    BOOST_TEST(10 == test_sum_12(T{1}, T{2}, T{3}, T{4}));
    BOOST_TEST(10 == test_sum_12(T{1}, T{2}, *python::list<T>{
        T{3}, T{4}
    }));

    auto test_sum_13 = python::getattr<T(T, T, T, python::args)>(m, "test_sum_1");
    BOOST_TEST(10 == test_sum_13(T{1}, T{2}, T{3}, T{4}));
    BOOST_TEST(10 == test_sum_13(T{1}, T{2}, T{3}, *python::list<T>{
        T{4}
    }));

    auto test_sum_14 = python::getattr<T(T, T, T, T)>(m, "test_sum_1");
    BOOST_TEST(10 == test_sum_14(T{1}, T{2}, T{3}, T{4}));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Tuple, T, python::tester::Integers)
{
    auto m = python::import("test_simple_vargs");
    auto test_sum_00 = python::getattr<T(python::args)>(m, "test_sum_0");
    BOOST_TEST(10 == test_sum_00(*python::tuple<T, T, T, T>{
        T{1}, T{2}, T{3}, T{4}
    }));

    auto test_sum_01 = python::getattr<T(T, python::args)>(m, "test_sum_0");
    BOOST_TEST(10 == test_sum_01(T{1}, *python::tuple<T, T, T>{
        T{2}, T{3}, T{4}
    }));

    auto test_sum_02 = python::getattr<T(T, T, python::args)>(m, "test_sum_0");
    BOOST_TEST(10 == test_sum_02(T{1}, T{2}, *python::tuple<T, T>{
        T{3}, T{4}
    }));
}

BOOST_AUTO_TEST_SUITE_END()
