def add(a, b):
    return (a + b).value

def sub(a, b):
    return (a - b).value

def mul(a, b):
    return (a * b).value

def div(a, b):
    return (a / b).value
