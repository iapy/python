import test_metacg_export_doc_c

def get():
    return test_metacg_export_doc_c.get()

def doc(w):
    if w == 0:
        return test_metacg_export_doc_c.get.__doc__
    if w == 1:
        return test_metacg_export_doc_c.E.__doc__
    if w == 2:
        return test_metacg_export_doc_c.E.foo.__doc__
