# Abstract

The `python` library provides a linkage system that connects `metacg` components with  Python modules. This enables developers to incorporate Python code into their `metacg` projects and share C++ code with external Python applications.

# Python from C++

To get start with, we will develop a simple component that imports Python module and calls functions from that module ([python-import.cpp](https://bitbucket.org/iapy/shared/src/master/examples/python/python-import.cpp)):

```c++
#include <python/metacg.hpp>
#include <cg/component.hpp>

struct Main : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
	            python::lock([]{
		            auto m = python::import("math");

		            auto sqrt = python::getattr<float(double)>(m, "sqrt");
		            std::cout << sqrt(2.) << '\n';
		            std::cout << sqrt(5.) << '\n';

					auto floor = python::getattr<int(float)>(m, "floor");
					std::cout << floor(sqrt(2.)) << '\n';
					std::cout << floor(sqrt(5.)) << '\n';
	            });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Impl>;
};

#include <python/python.hpp>
#include <cg/service.hpp>
#include <cg/graph.hpp>
#include <cg/host.hpp>

int main()
{
    using G = cg::Graph<python::Python, Main>;
    auto host = cg::Host<G>(
        cg::Args<python::Python>(nlohmann::json{
        })
    );
    return cg::Service(host);
}
```

In the given code snippet, we import the Python standard module `math` using the `python::import` function. We then resolve the functions `math.sqrt` and `math.floor` defined in that module and call them.

The `python::lock` call acquires and releases Python's Global Interpreter Lock (GIL). Every call to Python from `metacg` must be guarded with this call to ensure thread safety.

Additionally, the `python::getattr` function retrieves an attribute of any Python object, casting it to its template argument (function with given signature in the given example). Note the difference in signatures between `math.sqrt` and `math.floor`:

 - When `float` is used in Python code, both `float` and `double` can be used in C++ code.
 - When `int` is used in Python code, any integer type (signed or unsigned) can be used in C++ code.

The expected output is:

```
1.41421
2.23607
1
2
```

The state of `python::Python` component (when included in the graph, embeds and initialises the Python interpreter) takes in a `nlohmann::json` with the following format as constructor argument:

```JSON
{
	"path": "/opt/my_modules",
	"code": false
}
```

In this structure:

- `path` represents the `PYTHONPATH`.
- `code` is a boolean flag that signifies if byte code should be produced during execution.

The `Main` component of the given example could be refactored as following ([python-header.cpp](https://bitbucket.org/iapy/shared/src/master/examples/python/python-header.cpp))

```c++
struct Main : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_IMPORT(math, (
            (float(double), sqrt),
            (int(float), floor)
        ));
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([this]{
                    auto m = python::import(this);

                    std::cout << m.sqrt(2.) << '\n';
                    std::cout << m.sqrt(5.) << '\n';

                    std::cout << m.floor(m.sqrt(2.)) << '\n';
                    std::cout << m.floor(m.sqrt(5.)) << '\n';
                });
                return 0;
            }
        };
    };

    using Services = ct::tuple<Impl>;
};
```

The `PYTHON_IMPORT` macro acts as a header for Python module. It's first argument is a name Python module being imported, it's second argument is a list of functions and their signatures. Note the usage of `python::import(this)` statement which implies that one `metacg` component may import only one Python module. However there is a workaround for this if importing multiple modules if needed ([python-define.cpp](https://bitbucket.org/iapy/shared/src/master/examples/python/python-define.cpp))

```python
#include <python/metacg.hpp>
#include <cg/component.hpp>

struct Main : cg::Component
{
    template<typename>
    struct Types
    {
        PYTHON_DEFINE(MainProxy, R"(
            from math import sqrt, floor
            from os import getcwd
        )");

        PYTHON_IMPORT(MainProxy, (
            (float(double), sqrt),
            (int(float), floor),
            (std::string(), getcwd)
        ));
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::lock([this]{
                    auto m = python::import(this);

                    std::cout << m.sqrt(2.) << '\n';
                    std::cout << m.sqrt(5.) << '\n';

                    std::cout << m.floor(m.sqrt(2.)) << '\n';
                    std::cout << m.floor(m.sqrt(5.)) << '\n';

                    std::cout << m.getcwd() << '\n';
                });
                return 0;
            }
        };
    };

    using Ports = ct::map<python::module<Main>>;
    using Services = ct::tuple<Impl>;
};

#include <python/python.hpp>
#include <cg/service.hpp>
#include <cg/graph.hpp>
#include <cg/host.hpp>

int main()
{
    using G = cg::Graph<python::Modules<Main>>;
    auto host = cg::Host<G>(
        cg::Args<python::Python>(nlohmann::json{
        })
    );
    return cg::Service(host);
}
```

In this example we have defined a new Python module named `MainProxy` with the following code:

```python
from math import sqrt, floor
from os import getcwd
```

and imported `sqrt`, `floor` and `getcwd` functions from this newly created module.

The `PYTHON_DEFINE` macro is designed to accept two parameters: the identifier for the module and the module’s code, which should exclusively consist of import statements.

Utilising the `PYTHON_DEFINE` macro is an equivalent to crafting a `MainProxy.py` file containing the aforementioned code within the `PYTHONPATH`.

- Regarding the `getcwd` function’s signature, it yields a `std::string`. This implies that the Python string returned is copied into a `std::string`. Alternatively, one could employ the `python::str()` signature. Opting for this would mean the `getcwd` function returns a Python `str` object encapsulated by a C++ wrapper.
- The `Main` component now defines `Ports` and component graph establishes a connection with the `python::Python`component. This connection signifies that the `Main` component is defining a module that should be integrated into the Python interpreter.

## Built-in Python types

Consider the following code ([python-statis.cpp](https://bitbucket.org/iapy/shared/src/master/examples/python/python-statis.cpp)):

```c++
template<typename>
struct Types
{
	PYTHON_IMPORT(statistics, (
		(double(std::vector<double>), mean),
		(double(python::list<double>), median)
	));
};

struct Impl
{
	template<typename Base>
	struct Interface : Base
	{
		int run()
		{
			std::vector<double> numbers{1., 2., 3., 4., 4.};
			python::lock([&, this]{
				auto m = python::import(this);
				std::cout << m.mean(numbers) << '\n';
				std::cout << m.median(numbers) << '\n';
			});
			return 0;
		}
	};
};
```

In this example, it’s important to observe that the `mean` function is defined to take `std::vector<double>` as its argument, whereas the `median` function takes `python::list<double>`. When it comes to passing arguments, these two are interchangeable - the actual generated signature for both functions would be `double(python::list<double>)`. The `python` library establishes the following correspondence between Python and C++ types:

| c++ type           | python library type   | python type | implementation                                                                           |
| ------------------ | --------------------- | ----------- | ---------------------------------------------------------------------------------------- |
| `std::string`      | `python::str`         | `str`       | [str.hpp](https://bitbucket.org/iapy/python/src/master/include/python/types/str.hpp)     |
| `std::vector<T>`   | `python::list<T>`     | `list`      | [list.hpp](https://bitbucket.org/iapy/python/src/master/include/python/types/list.hpp)   |
| `std::set<T>`      | `python::set<T>`      | `set`       | [set.hpp](https://bitbucket.org/iapy/python/src/master/include/python/types/set.hpp)     |
| `std::map<K, V>`   | `python::dict<K, V>`  | `dict`      | [dict.hpp](https://bitbucket.org/iapy/python/src/master/include/python/types/dict.hpp)   |
| `std::tuple<T...>` | `python::tuple<T...>` | `tuple`     | [tuple.hpp](https://bitbucket.org/iapy/python/src/master/include/python/types/tuple.hpp) |

Same rules apply to `T`, `K` and `V` making for example `python::list<python::set<python::str>>` equivalent to `std::vector<std::set<std::string>>`.

The declaration of function arguments, whether in C++ or Python types, does not alter the function signature, which will accept Python types as arguments. Nevertheless, the return type is significant. To facilitate passing the result back to Python without the need for conversion to and from a C++ type, it’s advisable to specify the function’s return type as a Python type. Conversely, if the function’s result is intended for use within C++ code, declaring the return type as a C++ type is more efficient.

## \*args and \*\*kwargs

In the following code ([python-kwargs.cpp](https://bitbucket.org/iapy/shared/src/master/examples/python/python-kwargs.cpp)), we import the `join` function from `os.path`, which is designed to accept `*args`. We also import the `dumps` function from `json`, which is designed to accept an arbitrary Python object, followed by `**kwargs`:

```c++
template<typename>
struct Types
{
	PYTHON_DEFINE(MainProxy, R"(
		from os.path import join
		from json import dumps
	)");

	PYTHON_IMPORT(MainProxy, (
		(std::string(python::args), join),
		(std::string(python::obj, python::kwargs), dumps)
	));
};

struct Impl
{
	template<typename Base>
	struct Interface : Base
	{
		int run()
		{
			python::lock([&, this]{
				auto m = python::import(this);
				using namespace python::literals;

				std::cout << m.join(
					"/etc"_s, "nginx"_s, "nginx.conf"_s
				) << '\n';

				std::cout << m.join(*python::list<python::str>{
					"/etc"_s, "nginx"_s, "nginx.conf"_s
				}) << '\n';

				std::cout << m.dumps(
					python::dict<python::obj, python::obj>{
						{"int"_s, python::obj::from(42)},
						{"str"_s, "Hello, world!"_s},
						{"list"_s, python::list<int>{1, 2, 3, 4, 5}}
					},
					**python::dict<python::str, python::obj>{
						{"indent"_s, python::obj::from(4)},
						{"ensure_ascii"_s, python::obj::from(true)}
					}
				) << '\n';
			});
			return 0;
		}
	};
};
```

Here are a few key points to this code:

- The `*args` can be formed from both C++ arguments and by using the unpack operator on `python::list`or `python::tuple`.
- The `**kwargs` can only be formed by applying the unpack operator on `python::dict<python::str, T>`, where `T` can be any type (in the example above, it’s `python::obj`).
- The namespace `python::literals` defines the `_s` literal, which constructs `python::str` from a string literal.
- `python::obj::from` is used to construct `python::obj` from primitive types when necessary.

## Importing classes

The following code illustrates the process of importing classes from Python modules and utilising them ([python-string.cpp](https://bitbucket.org/iapy/shared/src/master/examples/python/python-string.cpp)):

```c++
template<typename>
struct Types
{
	PYTHON_IMPORT(string,
	(
		PYTHON_CLASS(Formatter, (
			(std::string(
				python::str, python::args, python::kwargs
			), format)
		)),
		PYTHON_CLASS(Template, (
			(std::string(python::kwargs), substitute)
		))
	),
	(
		(class Formatter(), Formatter),
		(class Template(std::string), Template)
	));
};

struct Impl
{
	template<typename Base>
	struct Interface : Base
	{
		int run()
		{
			python::lock([this]{
				auto m = python::import(this);
				using namespace python::literals;				

				auto f = m.Formatter();
				std::cout << f.format(
					"{} is roughtly {}"_s, "pi"_s, 3.14159
				) << '\n';

				std::cout << f.format(
					"{name} is roughtly {value}"_s,
					**python::dict<python::str, python::obj>{
						{"name"_s,  "e"_s},
						{"value"_s, python::obj::from(2.71828)}
					}) << '\n';

				auto t = m.Template("$who likes $what"_s);
				std::cout << t.substitute(
					**python::dict<python::str, python::obj>{
						{"who"_s, "tim"_s},
						{"what"_s, "kung pao"_s}
					}) << '\n';
			});
			return 0;
		}
	};
};
```

The main takeaways are:

- The `PYTHON_IMPORT` could accept two arguments, with the first one being the list of classes defined by the module
- The structure of the `PYTHON_CLASS` macro (which defines the list of methods) is identical to that of the `PYTHON_IMPORT`
- The constructors are defined as methods of the module named after classes they construct. Note the `class` prefix of their return types that is used by C++ compiler to distinguish the C++ class from C++ function.

## Class attributes

Let's create a simple Python module defining the following class ([python_attrib.py](https://bitbucket.org/iapy/shared/src/master/examples/python/python_attrib.py)):

```python
class Demo(object):
    def __init__(self, value):
        self.value = value
        self.generated = [value]
```

This class can be imported as follows ([python-attrib.cpp](https://bitbucket.org/iapy/shared/src/master/examples/python/python-attrib.cpp)):

```c++
struct Types
{
	PYTHON_IMPORT(python_attrib,
	(
		PYTHON_CLASS(Demo, (
			(std::string, value),
			(python::list<python::str>, generated)
        ))
    ),
    (
	    (class Demo(std::string), Demo)
	));
};

struct Impl
{
	template<typename Base>
	struct Interface : Base
	{
		int run()
		{
			python::lock([this]{
				auto m = python::import(this);
				using namespace python::literals;

				auto d = m.Demo("foo"_s);
				std::cout << python::cast<python::str>(
					d["value"]
				) << '\n';

				d["value"_a] = "bar"_s;
				std::cout << d["value"_a] << '\n';

				for(auto const v : d["generated"_a])
					std::cout << v << '\n';

				d["generated"_a].append("bar"_s);
				for(auto const v : d["generated"_a])
					std::cout << v << '\n';
            });
            return 0;
        }
	};
};
```

- Every class that is defined using the `PYTHON_CLASS` macro has an implementation of `operator [] (const char *)` . This operator returns an attribute of the object by its name. The attribute’s type is derived from `python::obj` and it redefines the assignment operator as an attribute setter.
- In the `python::literals` namespace, a constexpr literal `_a` is defined. When `_a` is used for constructing an argument for `operator []`, the latter returns an instance of a type that is derived from the actual attribute type as specified in the `PYTHON_CLASS` macro.
- C++ code can import standard Python class operators such as `__le__`, `__gt__` etc. and use them in familiar way as `a <= b`, `a > b` etc. Check [unit test code](https://bitbucket.org/iapy/python/src/master/test/test_simple_class_operators.cpp) for this functionality as an example.

## Exceptions and with statement

When Python code throws an exception, that exception is automatically converted into `python::exception` and is thrown in C++ code. The `python::exception` class has the following instance functions:

- `python::exception::type()` returning `std::string` containing the name of Python type of the exception thrown
- `python::exception::what()` returning exception string representation.

The `python::with` function implements `with` statement of Python. The following snippets of Python and C++ code are equivalent (`x` is an arbitrary Python statement):

```python
with x as y:
	pass
```

```c++
python::with(x, [](auto y){

});
```

# C++ from Python

The `python` library facilitates the conversion of `metacg` component interfaces into python modules. To showcase this procedure, we will have to implement the subsequent component template. The entire code for this section can be found at [python-export.cpp](https://bitbucket.org/iapy/shared/src/master/examples/python/python-export.cpp).

```c++
#include <python/metacg.hpp>
#include <cg/component.hpp>

struct Export : cg::Component
{
	template<typename>
	struct Types
	{
        PYTHON_EXPORT(python_export_c);
    };

    struct Module
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
            }
        };
    };

    using Ports = ct::map<python::module<Export>>;
};

#include <python/python.hpp>
#include <cg/service.hpp>
#include <cg/graph.hpp>
#include <cg/host.hpp>

int main()
{
    auto json = nlohmann::json{};
    json["path"] = PROJECT_SOURCE_DIR;
    json["code"] = false;

    using G = cg::Graph<python::Modules<Export>>;
    auto host = cg::Host<G>(
        cg::Args<python::Python>(json)
    );
    return cg::Service(host);
}
```

In the provided code snippet:

- The `Export` component establishes the name of the module being exported with `PYTHON_EXPORT(python_export_c)`. The `PYTHON_EXPORT`is a macro that accepts a single argument - the name of the module to be exported.
- The `struct Module` outlines the module interface. This interface name is fixed and it should implement a single method `void init(python::moddef &)` that carries out module initialisation.
- The interface name `Module` is predefined and is recognised by the `Python` framework when `python::Module` is employed to build `metacg`interface mapping.
- The `json["path"] = PROJECT_SOURCE_DIR` is used to allow python to import modules from `CMake` project root directory.

To illustrate the usage of the `python_export_c` module, we will create an additional component and add it to the graph. This component implements the main thread and import a pure Python module, which in turn imports and utilises the `python_export_c` module.

```c++
struct Main : cg::Component
{
    template<typename>
	struct Types
	{
		PYTHON_IMPORT(python_export, (
			(void(), test)
		));
	};

	struct Impl
	{
		template<typename Base>
		struct Interface : Base
		{
			int run()
			{
				python::lock([this]{
					python::import(this).test();
				});
				return 0;
			}
		};
	};
	using Services = ct::tuple<Impl>;
};

// Add to graph:
using G = cg::Graph<Main, python::Modules<Export>>;
```

Additionally we need to implement `python_export` module that implements `test()` function:

```python
import python_export_c

def test():
	pass
```

The following two sections of this document implies that all the C++ code is situated within `Module::Interface`, while all the Python code is located within the `def test()` function.

## Defining functions

We’ll begin by crafting a “Hello World” implementation:

```c++
template<typename Base>
struct Interface : Base
{
	void init(python::moddef &mod)
	{
		mod.def("hello", python::bind<&Interface<Base>::hello>);
	}
private:
	std::string hello()
    {
	    return "Hello world!";
	}
};
```

```python
print(python_export_c.hello())
```

In the code above:

- `python::bind` transforms an Interface method into a Python function and should be employed for any function that requires exporting.
- `mod.def` defines a function, with the name given by its first argument and the implementation provided by its second argument.

The `mod.def` function has a third argument, `const char *doc*`, which defaults to `NULL`. This argument allows for the inclusion of a Docstring for the function.

```c++
mod.def("hello", python::bind<&Interface<Base>::hello>,
		"Returns \"Hello world!\"");
```

```python
print(python_export_c.hello.__doc__)
```

The Python code above prints `"Returns "Hello world!"` to console.

## Passing arguments

In contrast to the bindings from Python to C++, the functions that are exported can only take arguments that are in Python style (for instance, `python::list`rather than `std::vector`). The subsequent code illustrates a function that computes the sum of a `list[int]`:

```c++
void init(python::moddef &mod)
{
	mod.def("sum", python::bind<&Interface<Base>::sum>);
}

std::int64_t sum(python::list<std::int64_t> &&values)
{
	std::int64_t result{0};
	for(auto const v : values) result += v;
	return result;
}
```

```python
print(python_export_c.sum([1,2,3,4,5]))
```

The `sum` function, which is exported, can be defined as `std::int64_t sum(python::list<std::int64_t> values)`. This is because `python::list<std::int64_t>` is a Python reference-counted object that has been wrapped, and using it in this way does not result in any performance drawbacks.

The function that is being exported may not take in variable positional parameters (\*args), variable keyword parameters (\*\*kwargs), or have arguments with default values.

## Exporting classes

We will define a component that exports a `Complex` class to Python, which represents complex numbers [python-struct.cpp](https://bitbucket.org/iapy/shared/src/master/examples/python/python-struct.cpp):

```c++
struct Export : cg::Component
{
    template<typename>
    struct Types
    {
        struct Complex
        {
            BOOST_HANA_DEFINE_STRUCT(Complex,
                (double, re),
                (double, im)
            );

            Complex(double re = 0, double im = 0)
                : re{re}, im{im}
            {}
        };

        PYTHON_EXPORT(python_struct_c);
        PYTHON_IMPORT(python_struct, (
            (void(Complex *), display)
        ))
    };

    struct Module
    {
        template<typename Base>
        struct Interface : Base 
        {
            void init(python::moddef &mod)
            {
                mod.def<typename Base::Complex>();
            }
        };
    };

	struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                using namespace python::literals;
                python::lock([m=python::import(this)]{
	                auto c = typename Base::Complex{1, 1};
	                m.display(&c);
	                m.display(std::make_unique<typename Base::Complex>(
	                    3, 4
	                ));
                });
                return 0;
            }
        };
    };

    using Ports = ct::map<python::module<Export>>;
    using Services = ct::tuple<Impl>;
};
```

This component:

- Exports the module `python_struct_c` and exports the `struct Complex` to that module using the statement `mod.def<typename Base::Complex>()`.
- Imports the module `python_struct`, which defines a function `display` that takes a `Complex` object as an argument. Note that when exporting classes, Python functions that take instances of those classes as arguments must be declared to take pointers to those classes.
- Calls `display` in two different ways: by passing a raw pointer to a `Complex` object and by passing a `std::unique_ptr<Complex>`. The difference between these approaches is that passing the class by pointer does not transfer ownership of the object to the Python code, whereas passing the class wrapped with `std::unique_ptr`moves that pointer into the Python code, transferring ownership and making the underlying object wrapped with a ref-counted Python object.

The `python_struct` module could look like this:

```python
import python_struct_c

def display(f):
	print(f.re, f.im)
	f.re += 1
	f.im -= 1
	print(f.re, f.im)
```

As you can see, the `re` and `im` fields, defined as members of `BOOST_HANA_STRUCT`, are accessible for reading and writing in Python code.

Let's start adding some member functions to `Complex` class:

```c++
struct Complex
{
    operator std::string() const { ... }
};

void init(python::moddef &mod)
{
	mod.def<typename Base::Complex>()
       .def("__str__", python::bind<&Base::Complex::operator std::string>)
    ;
}
```

The code above will export the C++ `operator std::string()` as the `__str__` method of the Python class. There is another approach to defining Python class methods, but it does not work with `__str__` or `__repr__`, which **must** be member functions of the C++ class.

Moving on, we define some additional member functions:

```c++
void init(python::moddef &mod)
{
	mod.def<typename Base::Complex>()
       .def("__str__", python::bind<&Base::Complex::operator std::string>)
       .def("abs", python::bind<&Interface<Base>::abs>)
       .def("conj", python::bind<&Interface<Base>::conj)
    ;
}

double abs(typename Base::Complex *self)
{
	return std::sqrt(self->re * self->re + self->im * self->im);
}

auto conj(typename Base::Complex *self)
{
	return std::make_unique<typename Base::Complex>(self->re, -self->im);
}
```

- The `abs` function takes a single argument of type `typename Base::Complex *`, indicating that `abs` does not take ownership of the underlying object.
- The `conj` function returns a new `Complex` object wrapped in a `std::unique_ptr`. Any C++ function that returns a newly created object of the exported type must return it as a `std::unique_ptr`.

Unfortunately, it is not possible to declare a constructor for an exported type, but the following method can be used to emulate a constructor when necessary:

```c++
void init(python::moddef &mod)
{
	mod.def<typename Base::Complex>()
       .def("__str__", python::bind<&Base::Complex::operator std::string>)
       .def("abs", python::bind<&Interface<Base>::abs>)
       .def("conj", python::bind<&Interface<Base>::conj)
    = "Complex_";

    mod.def("Complex", python::bind<&Interface<Base>::Complex>);
}

auto Complex(double im, double re)
{
	return std::make_unique<typename Base::Complex>(im, re);
}
```

In the code above, we renamed our type to `Complex_` with `= "Complex_"` statement and declared a module-level function `Complex` that returns a new `Complex` object. This function can be used in Python code as normal constructor:

```python
import python_struct_c
c = python_struct_c.Complex(1, 2)
```

Arithmetic operators `+`, `-`, `*`, and `/` are automatically exported to Python if they are defined for a given C++ type, provided they take both arguments of that type and return the same type:

```c++
Complex operator + (Complex const &other) const
{
	return Complex{re + other.re, im + other.im};
}
```

Comparison operators `__eq__`, `__ne__` etc. could be defined either as Component methods or C++ instance methods as follows:

```c++
mod.def<typename Base::Complex>()
     .def("__eq__", python::bind<&Interface<Base>::eq>)

bool eq(typename Base::Complex *self, python::obj obj)
{
	auto other = python::cast<typename Base::Complex*>(obj);
	return self->re == other->re && self->im == other->im;
}
```

Note that the second argument is of type `python::obj`, which is later cast to a specific pointer type. The rationale behind this is that the first argument, being `self`, can be passed by pointer without requiring reference counting, whereas the second argument needs to be reference counted.

**TODO**: Add support for more arithmetic operators ([PyNumberMethods](https://docs.python.org/3/c-api/typeobj.html#c.PyNumberMethods) for a reference).

All functions defined with their first argument as a pointer type are treated as instance functions during compile time, following the corresponding calling conventions. If a function that takes an argument of that type needs to be a module-level function, it **must** be declared as taking a `python::obj` type, and the argument of that type **must** be cast to the specific type:

```c++
mod.def("is_imaginary", python::bind<&Interface<Base>::is_imaginary>);

bool is_imaginary(python::obj obj)
{
	return 0 == python::cast<typename Base::Complex*>(obj)->re;
}
```

### `__call__` operator

C++ can export callbacks to Python code. For example:

```c++
template<typename>                                                       struct Types
{
	using Callback = std::function<void(python::dict<python::str, int>)>;
};

void init(python::moddef &mod)
{
	mod.def<typename Base::Callback>()
		.def("__call__", python::bind<&Base::Callback::operator ()>)
	;
}

python::lock([m=python::import(this)]{
	typename Base::Callback cb = [](
		python::dict<python::str, int> d
	) {
		for(auto const &[k, v] : d)
			std::cout << k << ' ' << v << '\n';
    };
    m.call(&cb);
})
```

The `__call__` method can be defined for any type as a class or component-level function.

# Native Python modules

To create a Python module, compile the following code into a shared library named `myawesomecppmodule.so`:

```c++
#include <python/shared.hpp>

PyMODINIT_FUNC PyInit_myawesomecppmodule()
{
	return python::shared<MyAwesomCppModule
		, cg::Connect<MyAwesomCppModule, A, tag_A>
		, cg::Connect<MyAwesomCppModule, B, tag_B>
	>();
}
```

n the code above:

- `MyAwesomeCppModule` is a component that provides `myawesomecppmodule` by defining the `Types` and `Module`interface.
- `A` and `B` are dependent components that `MyAwesomeCppModule` resolves using `this->remote(...)` within the `Module` interface. Their inclusion is optional and determined by the application logic.