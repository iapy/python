#pragma once
#include <python/detail/import.hpp>
#include <python/detail/object.hpp>
#include <python/detail/module.hpp>
#include <python/detail/pycast.hpp>
#include <python/detail/pylock.hpp>
#include <python/types/dict.hpp>
#include <python/types/func.hpp>
#include <python/types/iter.hpp>
#include <python/types/optional.hpp>
#include <python/types/list.hpp>
#include <python/types/set.hpp>
#include <python/types/str.hpp>
#include <python/types/tuple.hpp>

static_assert(PY_MAJOR_VERSION == 3 && PY_MINOR_VERSION >= 8);

namespace python {

template<typename T, typename F>
void with(T &&o, F &&functor)
{
    try {
        functor(python::getattr<object()>(o, "__enter__")());
        Py_INCREF(Py_None);
        Py_INCREF(Py_None);
        Py_INCREF(Py_None);
        python::getattr<void(object, object, object)>(o, "__exit__")(
            object(Py_None), object(Py_None), object(Py_None)
        );
    } catch(python::exception const &e) {
        throw exception(e, python::getattr<object>(o, "__exit__"));
    }
}

template<typename F>
auto lock(F &&functor)
{
    detail::Lock lock;
    try {
        return functor();
    } catch(python::exception const &e) {
        throw std::runtime_error(e.type());
    }
}

template<typename T>
object object::from(T&& value) 
{
    return detail::argument<std::decay_t<T>>::get(value);
}

namespace literals {
inline str operator ""_s(const char *d, std::size_t s)
{
    return str(std::string_view(d, s));
}

template<typename ch, ch ...values>
constexpr auto operator ""_a ()
{
    return detail::attribute_name<values...>{};
}
} // namespace literals

using namespace literals;

inline bool operator == (object const &v, str const &s) { return cast<str>(v) == static_cast<std::string_view>(s); }
inline bool operator != (object const &v, str const &s) { return cast<str>(v) != static_cast<std::string_view>(s); }

template<typename A>
inline bool operator ^ (A const &a, set<A> const &b) { return b.contains(a); }
template<typename A>
inline bool operator ^ (A const &a, list<A> const &b) { return b.contains(a); }
template<typename A, typename B>
inline bool operator ^ (A const &a, dict<A, B> const &b) { return b.contains(a); }

using obj = object;

} // namespace python
