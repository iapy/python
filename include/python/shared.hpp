#pragma once
#include <python/python.hpp>
#include <cg/service.hpp>
#include <cg/host.hpp>

namespace python {
namespace detail {

struct Exporter : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using Module = typename Resolver::template Types<tag::Module>;
    };

    struct Build
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                python::moddef def(*this->state.def);
                this->state.def->mod = init_module<typename Base::Module>();
                this->remote(tag::Module{}).init(def, std::true_type{});
                return 0;
            }
        };
    };

    template<typename>
    struct State
    {
        detail::moddef *def;
    };

    using Services = ct::tuple<Build>;
};

} // namespace detail

template<typename Component, typename ...Tail, typename ...Args>
PyObject *shared(Args&&... args)
{
    static detail::moddef def;
    static auto host = cg::Host<cg::Graph<
        cg::Connect<detail::Exporter, Component, tag::Module>,
        Tail...
    >>
    {
        cg::Args<detail::Exporter>(&def),
        std::forward<Args>(args)...
    };
    if(0 != cg::Service(host))
    {
        return NULL;
    }
    return def.mod.release();
}

} // namespace python
