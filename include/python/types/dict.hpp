#pragma once
#include <python/detail/except.hpp>
#include <python/detail/object.hpp>
#include <python/detail/result.hpp>
#include <python/detail/pyargs.hpp>
#include <map>

namespace python {
namespace detail {

template<typename K, typename V>
class kwargs_proxy;

template<typename V>
class kwargs_proxy<specific<std::string, object_type::GENERIC>, V>
{
private:
    kwargs_proxy(PyObject *o) : object_(o) {}
    PyObject *object_{nullptr};

    friend class specific<std::map<std::string, V>>;
    friend class specific<std::map<specific<std::string, object_type::GENERIC>, V>>;

public:
    kwargs<std::map<specific<std::string, object_type::GENERIC>, V>> operator * () const
    {
        return kwargs<std::map<specific<std::string, object_type::GENERIC>, V>>(object_);
    }
};

template<typename K, typename V>
class specific<std::map<K, V>, object_type::GENERIC> : public object
{
public:
    using key_type    = decltype(result<K>::convert(nullptr));
    using mapped_type = decltype(result<V>::convert(nullptr));
    using value_type  = std::pair<key_type, mapped_type>;

    class iterator : public std::iterator<std::forward_iterator_tag, value_type>
    {
    public:
        iterator(iterator &&other) = default;
        iterator(iterator const &other) = default;
        iterator& operator = (iterator &&other) = default;
        iterator& operator = (iterator const &other) = default;

        iterator &operator ++ ()
        {
            next();
            return *this;
        }

        iterator operator ++ (int)
        {
            iterator copy(*this);
            next();
            return copy;
        }

        value_type const operator * () const
        {
            return value_type{result<K>::ref(k), result<V>::ref(v)};
        }

        bool operator == (iterator const &other) const
        {
            return this == &other || (object_ == other.object_ && index_ == other.index_);
        }

        bool operator != (iterator const &other) const
        {
            return this != &other && (object_ != other.object_ || index_ != other.index_);
        }

    private:
        iterator(PyObject *object, Py_ssize_t index)
        : k(nullptr), v(nullptr), object_(object), index_{index}
        {
            if(index_ == 0 && !PyDict_Next(object_, &index_, &k, &v))
            {
                index_ = -1;
            }
        }

        inline void next()
        {
            if(!PyDict_Next(object_, &index_, &k, &v))
            {
                index_ = -1;
            }
        }

        PyObject *k, *v;
        PyObject *object_;
        Py_ssize_t index_;
        friend class specific<std::map<K,V>>;
    };

public:
    PYTHON_DECLARE_OBJECT_CONVERTERS(specific, object)

    specific() : object{PyDict_New()}
    {
    }

    specific(std::map<K, V> const &values) :  object{PyDict_New()}
    {
        for(auto const &[key, value] : values)
        {
            if(-1 == PyDict_SetItem(object_, argument<K>::get(key), argument<V>::get(value)))
                throw exception();
        }
    }

    specific(std::initializer_list<value_type> values) : object{PyDict_New()}
    {
        for(auto const &[key, value] : values)
        {
            if(-1 == PyDict_SetItem(object_, argument<K>::get(key), argument<V>::get(value)))
                throw exception();
        }
    }

    void clear()
    {
        PyDict_Clear(object_);
    }

    mapped_type const operator [](key_type const &key) const
    {
        PyObject *object = PyDict_GetItemWithError(object_, argument<K>::get(key));
        if(!object)
        {
            if(PyObject *error = PyErr_Occurred(); error)
            {
                Py_DECREF(error);
                throw exception{};
            }
            else
            {
                return static_cast<mapped_type>(python::object(nullptr));
            }
        }
        return result<V>::ref(object);
    }

    void set(key_type const &key, mapped_type const &value)
    {
        if(-1 == PyDict_SetItem(object_, argument<K>::get(key), argument<V>::get(value)))
            throw exception();
    }

    bool contains(key_type const &key) const
    {
        if(int result = PyDict_Contains(object_, argument<K>::get(key)); result == -1)
            throw exception();
        else
            return 1 == result;
    }

    void erase(key_type const &key)
    {
        if(-1 == PyDict_DelItem(object_, argument<K>::get(key)))
            throw exception();
    }

    void update(specific<std::map<K, V>, object_type::GENERIC> const &other)
    {
        if(-1 == PyDict_Update(object_, other.object_))
            throw exception();
    }

    kwargs_proxy<key_type, V> operator * () const
    {
        return kwargs_proxy<key_type, V>(object_);
    }

    std::size_t size() const
    {
        return PyDict_Size(object_);
    }

    iterator begin() const
    {
        return iterator(object_, 0);
    }

    iterator end() const
    {
        return iterator(object_, -1);
    }
};

template<typename V>
class kwargs<std::map<specific<std::string, object_type::GENERIC>, V>> : private object
{
private:
    kwargs(PyObject *o) : object(o) {}
    friend class kwargs_proxy<specific<std::string, object_type::GENERIC>, V>;

public:
    PyObject *get()
    {
        Py_INCREF(object_);
        return object_;
    };
};

} // namespace detail

template<typename K, typename V>
using dict = detail::specific<std::map<K, V>>;

} // namespace python
