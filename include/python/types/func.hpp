#pragma once
#include <python/detail/except.hpp>
#include <python/detail/object.hpp>
#include <python/detail/pyargs.hpp>
#include <python/detail/result.hpp>

namespace python {
namespace detail {

template<typename...>
struct is_kwargs;

template<>
struct is_kwargs<> : std::false_type {};

template<typename T>
struct is_kwargs<kwargs<T>> : std::true_type {};

template<typename T>
struct is_kwargs<T> : std::is_same<T, python::kwargs> {};

template<typename A, typename ...Args>
struct is_kwargs<A, Args...> : is_kwargs<Args...> {};

} // namespace detail

template<typename R, typename ...As>
class detail::specific<R(As...), detail::object_type::GENERIC> : public object
{
    template<typename T, std::size_t ...Is>
    std::pair<PyObject*, PyObject*> make_kwargs(T &&args, std::index_sequence<Is...>) const
    {
        static_assert(check_args_v<args_signature<std::decay_t<As>...>, args_signature<std::decay_t<decltype(std::get<Is>(args))>...>>);
        return {make_args(std::forward<std::decay_t<decltype(std::get<Is>(args))>>(std::get<Is>(args))...), std::get<sizeof ...(Is)>(args).get()};
    }

    auto return_(PyObject *p) const
    {
        if(PyObject *error = PyErr_Occurred(); error)
        {
            Py_DECREF(error);
            throw exception();
        }
        if constexpr (!std::is_same_v<R, void>)
        {
            return result<R>::convert(p);
        }
        else
        {
            Py_DECREF(p);
        }
    }
public:
    using object::object;

    template<typename ...Bs>
    auto operator () (Bs&& ...args) const
        -> std::conditional_t<std::is_same_v<R, void>, void, decltype(result<R>::convert(nullptr))>
    {
        if constexpr (detail::is_kwargs<As...>::value && detail::is_kwargs<Bs...>::value)
        {
            auto pyargs = make_kwargs(std::forward_as_tuple(args...), std::make_index_sequence<(sizeof ...(Bs)) - 1>{});

            PyObject *p = PyObject_Call(object::object_, pyargs.first, pyargs.second);
            Py_DECREF(pyargs.first);
            Py_DECREF(pyargs.second);
            return return_(p);
        }
        else
        {
            static_assert(check_args_v<args_signature<std::decay_t<As>...>, args_signature<std::decay_t<Bs>...>>);
            PyObject *a = make_args(std::forward<Bs>(args)...);

            PyObject *p = PyObject_CallObject(object::object_, a);
            Py_DECREF(a);
            return return_(p);
        }
    }
};

} // namespace python
