#pragma once
#include <python/detail/except.hpp>
#include <python/detail/object.hpp>
#include <python/detail/result.hpp>
#include <python/detail/pyargs.hpp>
#include <iostream>
#include <vector>

namespace python {

template<typename T>
class detail::specific<std::vector<T>, detail::object_type::GENERIC> : public object
{
public:
    using value_type = decltype(result<T>::convert(nullptr));

private:
    template<bool Reverse>
    class iterator_ : public std::iterator<std::forward_iterator_tag, value_type>
    {
    public:
        iterator_(iterator_ &&other) = default;
        iterator_(iterator_ const &other) = default;
        iterator_& operator = (iterator_ &&other) = default;
        iterator_& operator = (iterator_ const &other) = default;

        iterator_ &operator ++ ()
        {
            next();
            return *this;
        }

        iterator_ operator ++ (int)
        {
            iterator_ copy(*this);
            next();
            return copy;
        }

        iterator_ &operator = (value_type const &value)
        {
            if constexpr (std::is_base_of_v<object, value_type>)
            {
                Py_INCREF(value.object_);
            }
            if(-1 == PyList_SetItem(object_, index_, argument<T>::get(value)))
            {
                if constexpr (std::is_base_of_v<object, value_type>)
                {
                    Py_DECREF(value.object_);
                }
                throw python::exception{};
            }
            return *this;
        }

        bool operator == (iterator_ const &other) const
        {
            return this == &other || (object_ == other.object_ && index_ == other.index_);
        }

        bool operator != (iterator_ const &other) const
        {
            return this != &other && (object_ != other.object_ || index_ != other.index_);
        }

        value_type const operator * () const
        {
            PyObject *object = PyList_GetItem(object_, index_ - static_cast<Py_ssize_t>(Reverse));
            if(!object) throw python::exception{};
            return result<T>::ref(object);
        }

    private:
        iterator_(PyObject *object, std::size_t index)
        : object_(object), index_{index}
        {
        }

        inline void next()
        {
            if constexpr (Reverse) --index_;
            else ++index_;
        }

        PyObject *object_;
        std::size_t index_;
        friend class detail::specific<std::vector<T>>;
    };

public:
    PYTHON_DECLARE_OBJECT_CONVERTERS(specific, object)

    using iterator = iterator_<false>;
    using reverse_iterator = iterator_<true>;

    specific() : object{PyList_New(0)}
    {
    }

    specific(std::size_t size) : object{PyList_New(size)}
    {
    }

    specific(std::vector<T> const &values) : object{PyList_New(values.size())}
    {
        std::size_t index{0};
        for(auto const &value : values)
        {
            PyList_SetItem(object_, index++, argument<T>::get(value));
        }
    }

    specific(std::initializer_list<T> values) : object{PyList_New(values.size())}
    {
        std::size_t index{0};
        for(auto const &value : values)
        {
            PyList_SetItem(object_, index++, argument<T>::get(value));
        }
    }

    value_type const operator [] (std::size_t i) const
    {
        return *iterator(object_, i);
    }

    void set(std::size_t i, value_type const &value)
    {
        iterator(object_, i) = value;
    }

    void insert(std::size_t i, value_type const &value)
    {
        if(-1 == PyList_Insert(object_, i, argument<T>::get(value))) throw python::exception{};
    }

    void append(value_type const &value)
    {
        if(-1 == PyList_Append(object_, argument<T>::get(value))) throw python::exception{};
    }

    void reverse()
    {
        if(-1 == PyList_Reverse(object_)) throw python::exception{};
    }

    detail::vargs<std::vector<T>> operator *() const
    {
        return detail::vargs<std::vector<T>>(object_);
    }

    std::size_t size() const
    {
        return PyList_Size(object_);
    }

    iterator begin() const
    {
        return iterator(object_, 0);
    }

    iterator end() const
    {
        return iterator(object_, size());
    }

    reverse_iterator rbegin() const
    {
        return reverse_iterator(object_, size());
    }

    reverse_iterator rend() const
    {
        return reverse_iterator(object_, 0);
    }
};

template<typename T>
class detail::vargs<std::vector<T>> : private object
{
public:
    vargs(PyObject *o) : object(o) {}

    void set(PyObject *tuple, std::size_t index) const
    {
        for(std::size_t i = 0; i < size(); ++i, ++index)
        {
            PyObject *v = PyList_GetItem(object_, i);
            if(!v) throw python::exception{};
            Py_INCREF(v);
            PyTuple_SetItem(tuple, index, v);
        }
    }

    std::size_t size() const
    {
        return PyList_Size(object_);
    }
};

template<typename T>
using list = detail::specific<std::vector<T>>;

} // namespace python
