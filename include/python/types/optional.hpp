#pragma once
#include <python/detail/except.hpp>
#include <python/detail/object.hpp>
#include <python/detail/result.hpp>
#include <python/detail/pyargs.hpp>
#include <optional>

namespace python {
namespace detail {

template<typename T, bool I = std::is_integral_v<T>>
struct optional_base : std::false_type
{
    using type = decltype(result<T>::convert(nullptr));
};

template<typename T>
struct optional_base<T, true> : std::true_type
{
    using type = python::object;
};

template<typename T, object_type O>
class specific<std::optional<T>, O> : protected optional_base<T>::type
{
    using Base = typename optional_base<T>::type;
public:
    PYTHON_DECLARE_OBJECT_CONVERTERS(specific, Base)

    bool operator ! () const
    {
        return object::operator ! () || Base::object_ == Py_None;
    }

    auto operator * () const
    -> std::conditional_t<optional_base<T>::value, T, Base const&>
    {
        if constexpr (optional_base<T>::value)
            return result<T>::convert(Base::object_);
        else
            return *this;
    }

    Base const *operator -> () const
    {
        return this; 
    }

    template<typename U, bool B> friend struct attribute_proxy;
};

template<>
class specific<std::nullopt_t, object_type::GENERIC> : protected object
{
public:
    inline specific() : object(Py_None) {}
    inline specific(std::nullopt_t) : object(Py_None) {}
    template<typename T, bool, bool, bool> friend class argument;
};

template<typename T>
struct argument<std::optional<T>, false, false, false>
{
    static PyObject *get(std::optional<T> const &value)
    {
        if(!value)
        { 
            Py_INCREF(Py_None);
            return Py_None;
        }
        return argument<specific<T>>::get(*value);
    }
};

template<typename>
struct is_python_optional : std::false_type {};

template<typename T, object_type O>
struct is_python_optional<specific<std::optional<T>, O>> : std::true_type {};

} // namespace detail

template<typename T>
using optional = detail::specific<std::optional<T>>;

} // namespace python
