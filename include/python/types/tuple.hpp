#pragma once
#include <python/detail/except.hpp>
#include <python/detail/object.hpp>
#include <python/detail/result.hpp>
#include <python/detail/pyargs.hpp>
#include <iostream>
#include <tuple>

namespace python {

template<typename ...T>
class detail::specific<std::tuple<T...>, detail::object_type::GENERIC> : public object
{
public:
    PYTHON_DECLARE_OBJECT_CONVERTERS(specific, object)

    specific() : object{PyTuple_New(sizeof ...(T))}
    {
    }

    template<typename ...U>
    specific(U&& ...values) : object{PyTuple_New(sizeof ...(T))}
    {
        static_assert(check_args_v<
            args_signature<std::decay_t<T>...>,
            args_signature<std::decay_t<U>...>
        >);
        make_args_impl<0>(object_, std::forward<U>(values)...);
    }

    template<std::size_t I, typename V>
    void set(V &&value)
    {
        static_assert(check_args_v<
            args_signature<typename std::tuple_element_t<I, std::tuple<T...>>>, 
            args_signature<std::decay_t<V>>
        >);
        if(-1 == PyTuple_SetItem(object_, I, detail::argument<V>::get(value)))
            throw python::exception();
    }

    template<typename ...V>
    auto operator + (detail::specific<std::tuple<V...>, detail::object_type::GENERIC> const &other) const
    {
        return sum(*this, other, std::make_index_sequence<sizeof ...(T)>(), std::make_index_sequence<sizeof ...(V)>());
    }

    template<std::size_t I>
    auto get() const ->
        decltype(detail::result<
            typename std::tuple_element_t<I, std::tuple<T...>>
        >::ref(nullptr))
    {
        static_assert(I < sizeof...(T));
        if(PyObject *result = PyTuple_GetItem(object_, I); result)
        {
            return detail::result<
                typename std::tuple_element_t<I, std::tuple<T...>>
            >::ref(result);
        }
        throw python::exception();
    }

    detail::vargs<std::tuple<T...>> operator *() const
    {
        return detail::vargs<std::tuple<T...>>(object_);
    }

    constexpr std::size_t size() const
    {
        return sizeof ...(T);
    }

private:
    template<typename ...V, size_t ...Is, size_t ...Js>
    static auto sum(
        detail::specific<std::tuple<T...>, detail::object_type::GENERIC> const &l,
        detail::specific<std::tuple<V...>, detail::object_type::GENERIC> const &r,
        std::index_sequence<Is...>,
        std::index_sequence<Js...>)
    {
        return detail::specific<std::tuple<T..., V...>, detail::object_type::GENERIC>(
            l.template get<Is>()..., r.template get<Js>()...
        );
    }
};

template<typename ...T>
class detail::vargs<std::tuple<T...>> : private object
{
public:
    vargs(PyObject *o) : object(o) {}

    void set(PyObject *tuple, std::size_t index) const
    {
        for(std::size_t i = 0; i < size(); ++i, ++index)
        {
            PyObject *v = PyTuple_GetItem(object_, i);
            if(!v) throw python::exception{};
            Py_INCREF(v);
            PyTuple_SetItem(tuple, index, v);
        }
    }

    constexpr std::size_t size() const
    {
        return sizeof ...(T);
    }
};

template<typename ...T>
using tuple = detail::specific<std::tuple<T...>>;

template<std::size_t I, typename ...T>
auto get(tuple<T...> const &tuple) ->
    decltype(detail::result<
        typename std::tuple_element_t<I, std::tuple<T...>>
    >::ref(nullptr))
{
    return tuple.template get<I>();
}

} // namespace python
