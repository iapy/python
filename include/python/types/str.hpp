#pragma once
#include <python/detail/object.hpp>
#include <string_view>
#include <cstring>
#include <string>

namespace python {

template<>
class detail::specific<std::string, detail::object_type::GENERIC> : public object
{
public:
    PYTHON_DECLARE_OBJECT_CONVERTERS(specific, object)

    specific(std::string &&value) : object(PyUnicode_FromStringAndSize(value.c_str(), value.size()))
    {
        Py_INCREF(object_);
    }
    specific(std::string const &value) : object(PyUnicode_FromStringAndSize(value.c_str(), value.size()))
    {
        Py_INCREF(object_);
    }

    specific(std::string_view &&value) : object(PyUnicode_FromStringAndSize(value.data(), value.size()))
    {
        Py_INCREF(object_);
    }
    specific(std::string_view const &value) : object(PyUnicode_FromStringAndSize(value.data(), value.size()))
    {
        Py_INCREF(object_);
    }

    specific& operator = (std::string const &value)
    {
        Py_DECREF(object_);
        object_ = PyUnicode_FromStringAndSize(value.c_str(), value.size());
        Py_INCREF(object_);
        return *this;
    }

    specific& operator = (std::string_view const &value)
    {
        Py_DECREF(object_);
        object_ = PyUnicode_FromStringAndSize(value.data(), value.size());
        Py_INCREF(object_);
        return *this;
    }

    bool operator == (std::string const &p) const
    {
        Py_ssize_t size;
        const char *data = PyUnicode_AsUTF8AndSize(object_, &size);
        return size == p.size() && !std::strncmp(data, p.c_str(), size);
    }

    bool operator != (std::string const &p) const
    {
        Py_ssize_t size;
        const char *data = PyUnicode_AsUTF8AndSize(object_, &size);
        return size != p.size() || std::strncmp(data, p.c_str(), size);
    }

    bool operator == (std::string_view const &p) const
    {
        Py_ssize_t size;
        const char *data = PyUnicode_AsUTF8AndSize(object_, &size);
        return size == p.size() && !std::strncmp(data, p.data(), size);
    }

    bool operator != (std::string_view const &p) const
    {
        Py_ssize_t size;
        const char *data = PyUnicode_AsUTF8AndSize(object_, &size);
        return size != p.size() || std::strncmp(data, p.data(), size);
    }

    bool operator < (specific const &other) const
    {
        return static_cast<std::string_view>(*this) < static_cast<std::string_view>(other);
    }

    bool operator > (specific const &other) const
    {
        return static_cast<std::string_view>(*this) > static_cast<std::string_view>(other);
    }

    bool operator <= (specific const &other) const
    {
        return static_cast<std::string_view>(*this) <= static_cast<std::string_view>(other);
    }

    bool operator >= (specific const &other) const
    {
        return static_cast<std::string_view>(*this) >= static_cast<std::string_view>(other);
    }

    std::size_t find(const char *s, size_t pos = 0) const
    {
        std::string_view sv = *this;
        return sv.find(s, pos);
    }

    std::size_t find(char c, size_t pos = 0) const
    {
        std::string_view sv = *this;
        return sv.find(c, pos);
    }

    const char* c_str() const
    {
        Py_ssize_t size;
        return PyUnicode_AsUTF8AndSize(object_, &size);
    }

    operator std::string () const
    {
        Py_ssize_t size;
        const char *data = PyUnicode_AsUTF8AndSize(object_, &size);
        return std::string(data, size);
    }

    operator std::string_view () const
    {
        Py_ssize_t size;
        const char *data = PyUnicode_AsUTF8AndSize(object_, &size);
        return std::string_view(data, size);
    }

    std::size_t size() const
    {
        return PyUnicode_GET_LENGTH(object_);
    }
};

using str = detail::specific<std::string>;

inline bool operator == (std::string const &lhs, str const &rhs) { return rhs == lhs; }
inline bool operator != (std::string const &lhs, str const &rhs) { return rhs != lhs; }
inline bool operator == (std::string_view const &lhs, str const &rhs) { return rhs == lhs; }
inline bool operator != (std::string_view const &lhs, str const &rhs) { return rhs != lhs; }

std::ostream& operator << (std::ostream &, str const &);

} // namespace python

