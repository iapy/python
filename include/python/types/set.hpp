#pragma once
#include <python/detail/except.hpp>
#include <python/detail/object.hpp>
#include <python/detail/result.hpp>
#include <python/detail/pyargs.hpp>
#include <python/types/iter.hpp>
#include <unordered_set>
#include <set>

namespace python {

template<typename K>
class detail::specific<std::set<K>, detail::object_type::GENERIC> : public object
{
public:
    using value_type = decltype(result<K>::convert(nullptr));

public:
    PYTHON_DECLARE_OBJECT_CONVERTERS(specific, object)

    specific() : object{PySet_New(nullptr)}
    {
    }

    specific(std::set<K> const &values) :  object{PySet_New(nullptr)}
    {
        for(auto const &value : values)
        {
            if(-1 == PySet_Add(object_, argument<K>::get(value)))
                throw exception();
        }
    }

    specific(std::initializer_list<value_type> values) : object{PySet_New(nullptr)}
    {
        for(auto const &value : values)
        {
            if(-1 == PySet_Add(object_, argument<K>::get(value)))
                throw exception();
        }
    }

    void insert(value_type const &value)
    {
        if(-1 == PySet_Add(object_, argument<K>::get(value)))
            throw exception();
    }

    bool erase(value_type const &value)
    {
        int result = PySet_Discard(object_, argument<K>::get(value));
        if(result == -1) throw exception();
        return 1 == result;
    }

    bool contains(value_type const &value) const
    {
        if(int result = PySet_Contains(object_, argument<K>::get(value)); -1 == result)
            throw exception();
        else
            return static_cast<bool>(result);
    }

    void clear()
    {
        PySet_Clear(object_);
    }

    auto begin() const
    {
        PyObject *result = PyObject_GetIter(object_);
        if(!result) throw exception();
        return detail::specific<std::iterator<K, std::forward_iterator_tag>>(result);
    }

    auto end() const
    {
        return detail::specific<std::iterator<K, std::forward_iterator_tag>>(nullptr);
    }

    std::size_t size() const
    {
        return PySet_Size(object_);
    }
};

template<typename K>
using set = detail::specific<std::set<K>>;

} // namespace python
