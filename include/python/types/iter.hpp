#pragma once
#include <python/detail/except.hpp>
#include <python/detail/object.hpp>
#include <python/detail/result.hpp>
#include <python/detail/pyargs.hpp>
#include <iterator>

namespace python {

template<typename T>
class detail::specific<std::iterator<T, std::forward_iterator_tag>, detail::object_type::GENERIC> : private object
{
public:
    using value_type = decltype(detail::result<T>::convert(nullptr));

public:
    specific(PyObject *o) : object(o)
    , item_{o == nullptr ? nullptr : PyIter_Next(o)}
    {
        if(!item_ && o) object::operator=(nullptr);
    }

    specific(specific const &other) = delete;
    specific(specific &&other)
    : item_{other.item_}
    {
        other.item_ = nullptr;
        object::operator=(std::forward<specific>(other));
    }

    specific &operator = (specific const &other) = delete;
    specific &operator = (specific &&other)
    {
        item_ = other.item_;
        other.item_ = nullptr;
        object::operator=(std::forward<specific>(other));
    }

    specific &operator ++ ()
    {
        if(item_)
        {
            Py_DECREF(item_);
            if(!(item_ = PyIter_Next(object_)))
            {
                object::operator=(nullptr);
            }
        }
        return *this;
    }

    value_type const operator * () const
    {
        return detail::result<T>::ref(item_);
    }

    bool operator == (specific const &other) const
    {
        return object_ == other.object_ && item_ == other.item_;
    }

    bool operator != (specific const &other) const
    {
        return object_ != other.object_ || item_ != other.item_;
    }

    ~specific()
    {
        if(item_) Py_DECREF(item_);
    }

private:
    PyObject *item_;
};

template<typename T>
class iter : public object
{
public:
    PYTHON_DECLARE_OBJECT_CONVERTERS(iter, object)

    auto begin() const
    {
        PyObject *result = PyObject_GetIter(object_);
        if(!result) throw exception();
        return detail::specific<std::iterator<T, std::forward_iterator_tag>>(result);
    }

    auto end() const
    {
        return detail::specific<std::iterator<T, std::forward_iterator_tag>>(nullptr);
    }
};

} // namespace python
