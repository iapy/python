#pragma once
#include <python/detail/config.hpp>
#include <python/detail/import.hpp>
#include <python/detail/metacg.hpp>
#include <python/detail/moddef.hpp>
#include <python/detail/pylock.hpp>
#include <cg/component.hpp>
#include <python/tags.hpp>

namespace python {
namespace detail {

template<typename T>
auto define_exists(T *) -> decltype(std::string_view{T::python_defined_module_name}, std::true_type{});

template<typename T>
std::false_type define_exists(...);

template<typename T>
struct make_define_modules
{
    using type = std::conditional_t<decltype(define_exists<T>(std::declval<T*>()))::value, ct::tuple<T>, ct::tuple<>>;
};

inline void define_modules(ct::tuple<>) {}

template<typename Head, typename ...Tail>
void define_modules(ct::tuple<Head, Tail...>)
{
    static std::string code = metacg::align_code(Head::python_defined_module_code);
    PyObject *str = Py_CompileString(code.c_str(), Head::python_defined_module_name, Py_file_input);
    PyImport_ExecCodeModule(Head::python_defined_module_name, str);
    define_modules(ct::tuple<Tail...>{});
}

template<typename T>
auto export_exists(T *) -> decltype(std::string_view{T::python_exported_module_name}, std::true_type{});

template<typename T>
std::false_type export_exists(...);

template<typename T>
struct make_export_modules
{
    using type = std::conditional_t<decltype(export_exists<T>(std::declval<T*>()))::value, ct::tuple<T>, ct::tuple<>>;
};

inline void export_modules(ct::tuple<>) {}

template<typename Head>
PyObject *init_module()
{
    static PyMethodDef methods[] = {{0, 0, 0, 0}};
    static PyModuleDef module = {PyModuleDef_HEAD_INIT, Head::python_exported_module_name, NULL, -1, methods};
    return PyModule_Create(&module);
}

template<typename Head, typename ...Tail>
void export_modules(ct::tuple<Head, Tail...>)
{
    PyImport_AppendInittab(Head::python_exported_module_name, init_module<Head>);
    export_modules(ct::tuple<Tail...>{});
}

template<typename T>
struct modules : detail::moddef {};

template<typename ...Tail>
struct modules<ct::tuple<Tail...>> : modules<Tail>...  {};

} // namespace detail

struct Python : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using Modules = typename Resolver::template TypesTuple<tag::Module>;
    };

    struct Export 
    {
        template<typename Base>
        struct Interface : Base
        {
            Interface(Base &&base) : Base(std::forward<Base>(base))
            {
                this->apply(tag::Module{},[this](auto module){
                    using Types = typename decltype(module)::Types;
                    if constexpr (decltype(detail::export_exists<Types>(std::declval<Types*>()))::value)
                    {
                        auto &def = static_cast<detail::modules<Types>&>(this->state.modules);
                        def.mod = python::import(Types::python_exported_module_name);
                        moddef m(def);
                        module.init(m, std::true_type{});
                    }
                });
                detail::Lock::init();
                this->state.initialized = true;
            }
        };
    };

    template<typename Types>
    struct State : detail::metacg::State
    {
        State(nlohmann::json const &json)
        : detail::metacg::State([&]() -> nlohmann::json const & {
            detail::export_modules(ct::transform_t<detail::make_export_modules, typename Types::Modules>{});
            return json;
        }())
        {
            detail::define_modules(ct::transform_t<detail::make_define_modules, typename Types::Modules>{});
        }

        ~State()
        {
            if(detail::metacg::State::initialized)
            {
                detail::Lock::done();
            }
        }

        detail::modules<typename Types::Modules> modules;
    };

    using Initializers = ct::tuple<Export>;
};

template<typename ...M>
using Modules = cg::Connect<Python, cg::Group<M...>, tag::Module>;

} // namespace pythonppe
