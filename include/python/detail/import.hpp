#pragma once
#include <python/detail/except.hpp>
#include <python/detail/object.hpp>

namespace python {

object import(const char *name);

} // namespace python
