#pragma once
#include <Python.h>

namespace python {
namespace detail {

struct Config
{
    operator PyConfig * () const;

    Config& path(char const *p);
    Config& byte(bool const);
};

} // namespace detail

extern detail::Config config;

} // namespace python
