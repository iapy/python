#pragma once
#include <python/detail/object.hpp>
#include <python/detail/result.hpp>
/**
 * python::cast is used to cast python::object to c++ class, i.e.:
 *      python::cast<std::string>(f())
 *
 * static_cast is used to cast between python::objects, i.e.:
 *      static_cast<python::str>(unclassified)
 */

namespace python {
namespace detail {

template<typename T, typename R>
R cast(object &&o)
{
    R result = detail::result<T>::ref(o.object_);
    o.object_ = nullptr;
    return result;
}

template<typename T, typename R>
R cast(object const &o)
{
    return detail::result<T>::ref(o.object_);
}

} // namespace detail

template<typename T>
auto cast(object &&o) -> decltype(detail::result<T>::ref(nullptr))
{
    return detail::cast<T, decltype(detail::result<T>::ref(nullptr))>(std::forward<object>(o));
}

template<typename T>
auto cast(object const &o) -> decltype(detail::result<T>::ref(nullptr))
{
    return detail::cast<T, decltype(detail::result<T>::ref(nullptr))>(o);
};

} // namespace
