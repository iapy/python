#pragma once
#include <Python.h>
#include <optional>
#include <memory>
#include <tuple>
#include <map>

namespace python {

class args;
class kwargs;
class object;

namespace detail {

enum class object_type;
template<typename, object_type> struct specific;

template<typename> struct is_specific : std::false_type {};
template<typename S, object_type T> struct is_specific<specific<S, T>> : std::true_type {};

template<typename> class vargs;
template<typename> class kwargs;

template<typename> struct is_vargs : std::false_type {};
template<typename T> struct is_vargs<vargs<T>> : std::true_type {};

template<typename ...T> struct args_signature {};
template<typename A, typename B> struct check_args;

// is_same_specific {{
template<typename A, typename B, bool AS = is_specific<A>::value, bool BS = is_specific<B>::value>
struct is_same_specific : std::is_same<A, B> {};

template<template <typename> typename A, typename AT, object_type AO, template <typename> typename B, typename BT, object_type BO>
struct is_same_specific<specific<A<AT>, AO>, specific<B<BT>, BO>, true, true> : std::false_type {};

template<template <typename> typename A, typename AT, object_type AO, typename BT>
struct is_same_specific<specific<A<AT>, AO>, specific<A<BT>, AO>, true, true> : is_same_specific<AT, BT> {};

template<template <typename> typename A, typename AT, object_type AO, template <typename> typename B, typename BT>
struct is_same_specific<specific<A<AT>, AO>, B<BT>, true, false>
{
    static constexpr bool value = std::is_same_v<A<AT>, B<BT>>
        || ((std::is_same_v<A<AT>, B<AT>> || std::is_same_v<A<BT>, B<BT>>) && is_same_specific<AT, BT>::value);
};

template<typename A>
struct is_same_specific<std::optional<A>, specific<A>, false, true> : std::true_type {};

template<typename A>
struct is_same_specific<specific<std::optional<A>>, specific<A>, true, true> : std::true_type {};

template<typename A>
struct is_same_specific<std::optional<A>, std::nullopt_t, false, false> : std::true_type {};

template<typename A>
struct is_same_specific<specific<std::optional<A>>, std::nullopt_t, true, false> : std::true_type {};

template<typename ...AT, typename ...BT>
struct is_same_specific<specific<std::tuple<AT...>, object_type::GENERIC>, std::tuple<BT...>, true, false>
{
    static constexpr bool value = std::conjunction_v<is_same_specific<AT, BT>...>;
};

template<typename ...AT, typename ...BT>
struct is_same_specific<specific<std::tuple<AT...>, object_type::GENERIC>, specific<std::tuple<BT...>, object_type::GENERIC>, true, true>
{
    static constexpr bool value = std::conjunction_v<is_same_specific<AT, BT>...>;
};

template<typename AK, typename AV, typename BK, typename BV>
struct is_same_specific<specific<std::map<AK, AV>, object_type::GENERIC>, std::map<BK, BV>, true, false>
{
    static constexpr bool value = is_same_specific<AK, BK>::value && is_same_specific<AV, BV>::value;
};

template<typename AK, typename AV, typename BK, typename BV>
struct is_same_specific<specific<std::map<AK, AV>, object_type::GENERIC>, specific<std::map<BK, BV>, object_type::GENERIC>, true, true>
{
    static constexpr bool value = is_same_specific<AK, BK>::value && is_same_specific<AV, BV>::value;
};

template<typename T>
struct is_same_specific<T*, std::unique_ptr<T>, false, false> : std::true_type{};

template<typename T>
struct is_same_specific<specific<T*, object_type::NATIVE>, T*, false, false> : std::true_type {};

template<typename A, object_type AO>
struct is_same_specific<A, specific<A, AO>, false, true> : std::true_type {};

template<typename A, typename B, object_type BO>
struct is_same_specific<A, specific<B, BO>, false, true> : is_same_specific<specific<B, BO>, A> {};
// }} is_same_specific

template<>
struct check_args<args_signature<>, args_signature<>> : std::true_type {};

template<>
struct check_args<args_signature<args>, args_signature<>> : std::true_type {};

template<>
struct check_args<args_signature<python::kwargs>, args_signature<>> : std::true_type {};

template<>
struct check_args<args_signature<args, python::kwargs>, args_signature<>> : std::true_type {};

template<typename A, typename B>
struct check_args<args_signature<A>, args_signature<B>>
{
    static constexpr bool value =
        std::is_same_v<A, args>
        || (is_same_specific<A, B>::value)
        || (std::is_integral_v<A> && std::is_integral_v<B>)
        || (std::is_same_v<A, object> && std::is_base_of_v<object, B>);
};

template<typename A, typename B, typename ...Bs>
struct check_args<args_signature<A>, args_signature<B, Bs...>>
{
    static constexpr bool value = (std::is_same_v<A, args>) || (
        (0 == sizeof ...(Bs)) && check_args<args_signature<A>, args_signature<B>>::value
    );
};

template<typename A, typename B, typename ...Bs>
struct check_args<args_signature<A, python::kwargs>, args_signature<B, Bs...>>
    : check_args<args_signature<A>, args_signature<B, Bs...>> {};

template<typename A, typename ...As, typename B, typename ...Bs>
struct check_args<args_signature<A, As...>, args_signature<B, Bs...>>
{
    static constexpr bool value = check_args<args_signature<A>, args_signature<B>>::value
        && check_args<args_signature<As...>, args_signature<Bs...>>::value;
};

template<typename A, typename B>
constexpr bool check_args_v = check_args<A, B>::value;

template<typename T, bool O = std::is_base_of_v<object, T>, bool I = std::is_integral_v<T>, bool F = std::is_same_v<T, double> || std::is_same_v<T, float>>
struct argument
{
    static PyObject *get(T const &value)
    {
        if(value.object_)
        {
            Py_INCREF(value.object_);
            return value.object_;
        }
        else
        {
            Py_INCREF(Py_None);
            return Py_None;
        }
    }
};

template<typename T>
struct argument<T, false, true, false>
{
    static PyObject *get(T value)
    {
        if constexpr (std::is_same_v<T, bool>)
        {
            if(value)
            {
                Py_INCREF(Py_True);
                return Py_True;
            }
            else
            {
                Py_INCREF(Py_False);
                return Py_False;
            }
        }
        else if constexpr (std::numeric_limits<T>::is_signed)
        {
            return PyLong_FromLongLong(value);
        }
        else
        {
            return PyLong_FromUnsignedLongLong(value);
        }
    }
};

template<typename T>
struct argument<T, false, false, true>
{
    static PyObject *get(T const &value)
    {
        return PyFloat_FromDouble(value);
    }
};

template<typename T>
struct argument<T, false, false, false>
{
    static PyObject *get(T const &value)
    {
        return argument<specific<T>>::get(specific<T>(value));
    }
};

template<std::size_t I>
void make_args_impl(PyObject *tuple)
{
    static_assert(0 == I);
}

template<std::size_t I>
std::size_t make_args_size()
{
    static_assert(0 == I);
    return 0;
}

template<std::size_t I, typename A>
void make_args_impl(PyObject *tuple, A&& a)
{
    if constexpr (is_vargs<std::decay_t<A>>::value)
    {
        a.set(tuple, I);
    }
    else
    {
        PyTuple_SetItem(tuple, I, argument<std::decay_t<A>>::get(a));
    }
}

template<std::size_t I, typename A>
constexpr std::size_t make_args_size(A const &a)
{
    if constexpr (is_vargs<std::decay_t<A>>::value)
    {
        return a.size();
    }
    else
    {
        return 1;
    }
}

template<std::size_t I, typename A, typename ...As>
void make_args_impl(PyObject *tuple, A&& a, As&& ...as)
{
    PyTuple_SetItem(tuple, I, argument<std::decay_t<A>>::get(a));
    make_args_impl<I + 1>(tuple, std::forward<As>(as)...);
}

template<std::size_t I, typename A, typename ...As>
constexpr std::size_t make_args_size(A const &a, As const & ...as)
{
    return 1 + make_args_size<I + 1>(as...);
}

template<typename ...As>
PyObject *make_args(As&& ...as)
{
    PyObject *t = PyTuple_New(make_args_size<0>(as...));
    make_args_impl<0>(t, std::forward<As>(as)...);
    return t;
}

} // namespace detail
} // namespace python
