#pragma once
#include <python/detail/object.hpp>
#include <python/detail/native.hpp>
#include <functional>
#include <cstring>
#include <vector>
#include <map>

namespace python {

class moddef;

namespace detail {

#define PYTHON_CHECK_NATIVE_OPERATOR(op, name) \
    template<typename Self> \
    auto has_native##name##impl(Self*) -> std::is_same<Self, decltype((std::declval<Self>() op std::declval<Self>()))>; \
    template<typename Self> \
    std::false_type has_native##name##impl(...); \
    template<typename Self> \
    constexpr bool has_native##name = decltype(has_native##name##impl<Self>(nullptr))::value;

PYTHON_CHECK_NATIVE_OPERATOR(+, __add__);
PYTHON_CHECK_NATIVE_OPERATOR(-, __sub__);
PYTHON_CHECK_NATIVE_OPERATOR(*, __mul__);
PYTHON_CHECK_NATIVE_OPERATOR(/, __truediv__);

#undef PYTHON_CHECK_NATIVE_OPERATOR

typedef PyObject*(PyCFunctionWithoutArguments)(PyObject*);

struct moddef
{
    std::vector<PyMethodDef> methods;
    python::object mod;
};

template<typename T>
class classdef
{
private:
    inline classdef(PyObject *m, char const * const doc) : mod{m}
    {
        type->tp_doc = doc;
        if constexpr(std::is_constructible_v<bool, T>)
        {
            constexpr auto to_bool = [](PyObject *self) -> int {
                return static_cast<bool>(*reinterpret_cast<native<T>*>(self)->instance);
            };
            type->tp_as_number = native<T>::numb();
            type->tp_as_number->nb_bool = to_bool;
        }
#define PYTHON_IMPL_NATIVE_OPERATOR(op, name, slot)                                                                     \
        if constexpr(detail::has_native##name<T>)                                                                       \
        {                                                                                                               \
            constexpr auto impl = [](PyObject *self, PyObject *other) -> PyObject *{                                    \
                return argument<std::unique_ptr<T>>::get(std::make_unique<T>(                                           \
                    (*reinterpret_cast<native<T>*>(self)->instance) op (*reinterpret_cast<native<T>*>(other)->instance) \
                ));                                                                                                     \
            };                                                                                                          \
            type->tp_as_number = native<T>::numb();                                                                     \
            type->tp_as_number->slot = impl;                                                                            \
        }

        PYTHON_IMPL_NATIVE_OPERATOR(+, __add__, nb_add)
        PYTHON_IMPL_NATIVE_OPERATOR(-, __sub__, nb_subtract)
        PYTHON_IMPL_NATIVE_OPERATOR(*, __mul__, nb_multiply)
        PYTHON_IMPL_NATIVE_OPERATOR(/, __truediv__, nb_true_divide)

#undef PYTHON_IMPL_NATIVE_OPERATOR
    }

    PyObject *mod;
    PyTypeObject *type = native<T>::type();
    friend class python::moddef;

public:
    classdef & operator = (const char *name)
    {
        type->tp_name = name;
        return *this;
    }

    classdef &def(const char *name, typename native<T>::getter func)
    {
        auto &properties = native<T>::properties();
        properties.emplace(std::string{name}, func);
        return *this;
    }

    classdef &def(const char *name, PyCFunctionWithKeywords func, char const * const doc = 0)
    {
        if(!std::strcmp(name, "__call__"))
        {
            type->tp_call = func;
        }
        else
        {
            auto &methods = native<T>::methods();
            methods.emplace_back(PyMethodDef{name, (PyCFunction)func, METH_VARARGS | METH_KEYWORDS, doc});
        }
        return *this;
    }

    classdef &def(const char *name, PyCFunctionWithoutArguments func, char const * const doc = 0)
    {
        if(!std::strcmp(name, "__call__"))
        {
            type->tp_call = (PyCFunctionWithKeywords)func;
        }
        else if(!std::strcmp(name, "__repr__"))
        {
            type->tp_repr = func;
        }
        else if(!std::strcmp(name, "__str__"))
        {
            type->tp_str = func;
        }
        else
        {
            auto &methods = native<T>::methods();
            methods.emplace_back(PyMethodDef{name, (PyCFunction)func, METH_NOARGS, doc});
        }
        return *this;
    }

    ~classdef()
    {
        if(auto &methods = native<T>::methods(); !methods.empty())
        {
            methods.emplace_back(PyMethodDef{0, 0, 0, 0});
            type->tp_methods = &methods[0];
        }

        PyType_Ready(type);
        PyDict_SetItem(type->tp_dict, PyUnicode_FromStringAndSize("__module__", 10), PyObject_GetAttrString(mod, "__name__"));
        PyModule_AddObject(mod, type->tp_name, reinterpret_cast<PyObject*>(type));
    }
};

} // namespace detail

class moddef
{
public:
    moddef(detail::moddef &def) : def_{def} {}

    moddef(moddef &&) = delete;
    moddef(moddef const &) = delete;
    moddef &operator = (moddef &) = delete;
    moddef &operator = (moddef const &) = delete;

    void def(const char *name, PyCFunctionWithKeywords func, char const * const doc = 0)
    {
        def_.methods.emplace_back(PyMethodDef{name, (PyCFunction)func, METH_VARARGS | METH_KEYWORDS, doc});
    }

    void def(const char *name, detail::PyCFunctionWithoutArguments func, char const * const doc = 0)
    {
        def_.methods.emplace_back(PyMethodDef{name, (PyCFunction)func, METH_NOARGS, doc});
    }

    template<typename T>
    detail::classdef<T> def(char const * const doc = 0)
    {
        return detail::classdef<T>(def_.mod.object_, doc);
    }

    PyObject *raw() const
    {
        return def_.mod.object_;
    }

    ~moddef()
    {
        def_.methods.emplace_back(PyMethodDef{0, 0, 0, 0});
        PyModule_AddFunctions(def_.mod.object_, &def_.methods[0]);
    }

private:
    detail::moddef &def_;
};

} // namespace python
