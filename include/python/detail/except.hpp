#pragma once
#include <Python.h>
#include <string>

namespace python {

class object;
class exception
{
public:
    exception();
    exception(exception &&);
    exception(exception const &);
    exception(exception const &, object);

    exception &operator = (exception &&);
    exception &operator = (exception const &);

    ~exception();

    std::string type() const;
    std::string what() const;

private:
    PyObject *e{nullptr}, *v{nullptr}, *t{nullptr};
};

} // namespace python
