#pragma once
#include <python/detail/object.hpp>
#include <python/detail/pyargs.hpp>
#include <python/detail/result.hpp>
#include <python/types/str.hpp>

#include <boost/core/demangle.hpp>
#include <boost/hana.hpp>
#include <unordered_map>
#include <functional>
#include <vector>
#include <memory>

namespace python {
namespace detail {

template<typename T>
struct native_base<T, std::enable_if_t<!boost::hana::Struct<T>::value>>
{
    PyObject_HEAD
    T *instance;
    bool managed;

    static std::unordered_map<std::string, std::function<PyObject*(T*)>> &properties()
    {
        static std::unordered_map<std::string, std::function<PyObject*(T*)>> impl;
        return impl;
    }

    static PyObject *getattr(PyObject *self, PyObject *attr)
    {
        if(auto const it = properties().find(result<str>::ref(attr)); it != properties().end())
        {
            return it->second(reinterpret_cast<native_base<T>*>(self)->instance);
        }
        return PyObject_GenericGetAttr(self, attr);
    }
    static int setattr(PyObject *self, PyObject *attr, PyObject *value)
    {
        return PyObject_GenericSetAttr(self, attr, value);
    }
};

template<typename T>
struct native_base<T, std::enable_if_t<boost::hana::Struct<T>::value>>
{
    PyObject_HEAD
    T *instance;
    bool managed;

    static PyObject *getattr(PyObject *self, PyObject *attr)
    {
        PyObject *value{NULL};
        T *cobj = reinterpret_cast<native_base<T>*>(self)->instance;

        using namespace boost::hana;
        for_each(accessors<T>(), [&value, cobj, name=result<str>::ref(attr)](auto &&accessor) mutable {
            if(std::string_view(first(accessor).c_str()) == name)
            {
                using V = std::decay_t<decltype(second(accessor)(*cobj))>;
                value = argument<V>::get(second(accessor)(*cobj));
            }
        });

        return value ? value : PyObject_GenericGetAttr(self, attr);
    }

    static int setattr(PyObject *self, PyObject *attr, PyObject *value)
    {
        int r{-1};
        T *cobj = reinterpret_cast<native_base<T>*>(self)->instance;

        using namespace boost::hana;
        for_each(accessors<T>(), [&r, value, cobj, name=result<str>::ref(attr)](auto &&accessor) mutable {
            if(std::string_view(first(accessor).c_str()) == name)
            {
                using V = std::decay_t<decltype(second(accessor)(*cobj))>;
                second(accessor)(*cobj) = result<V>::ref(value);
                r = 0;
            }
        });

        return !r ? r : PyObject_GenericSetAttr(self, attr, value);
    }
};

template<typename T>
struct native : native_base<T>
{
    typedef PyObject*(getter)(T*);

    static PyNumberMethods *numb()
    {
        static PyNumberMethods impl;
        return &impl;
    }

    static PyTypeObject *type()
    {
        static const std::string name = boost::core::demangle(typeid(T).name());
        static PyTypeObject impl{
            .ob_base = PyVarObject_HEAD_INIT(NULL, 0)
            .tp_name = name.c_str() + name.find_last_of(':') + 1,
            .tp_basicsize = sizeof(sizeof(native<T>)),
            .tp_itemsize = 0,
            .tp_dealloc = &destructor,
            .tp_getattro = &native_base<T>::getattr,
            .tp_setattro = &native_base<T>::setattr,
            .tp_flags = Py_TPFLAGS_DEFAULT
        };
        return &impl;
    }

    static std::vector<PyMethodDef> &methods()
    {
        static std::vector<PyMethodDef> impl;
        return impl;
    }

    static void destructor(PyObject *o)
    {
        native<T> *n = reinterpret_cast<native<T>*>(o);
        if(n->managed) delete n->instance;
        Py_TYPE(o)->tp_free(o);
    }
};

template<typename T>
struct argument<T*, false, false, false>
{
    static PyObject *get(T* ptr)
    {
        python::detail::native<T> *native = new python::detail::native<T>;
        native->instance = ptr;
        native->managed = false;
        native->ob_base.ob_type = python::detail::native<T>::type();
        native->ob_base.ob_refcnt = 1;
        return reinterpret_cast<PyObject*>(native);
    }
};

template<typename T>
struct argument<std::unique_ptr<T>, false, false, false>
{
    static PyObject *get(std::unique_ptr<T> &ptr, std::size_t refs = 1)
    {
        python::detail::native<T> *native = new python::detail::native<T>;
        native->instance = ptr.release();
        native->managed = true;
        native->ob_base.ob_type = python::detail::native<T>::type();
        native->ob_base.ob_refcnt = refs;
        return reinterpret_cast<PyObject*>(native);
    }

    static PyObject *get(std::unique_ptr<T> &&ptr, std::size_t refs = 1)
    {
        return get(ptr, refs);
    }
};

template<typename T>
struct result<T*, false, false, false>
{
    static T* ref(PyObject *o)
    {
        return reinterpret_cast<native<T>*>(o)->instance;
    }
};

} // namespace detail

template<typename T>
object native(T *ptr)
{
    return detail::argument<T*, false, false, false>::get(ptr);
}

template<typename T>
object native(std::unique_ptr<T> &&ptr)
{
    return detail::argument<std::unique_ptr<T>, false, false, false>::get(std::move(ptr), 0);
}

} // namespace python
