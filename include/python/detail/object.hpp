#pragma once
#include <python/detail/except.hpp>
#include <Python.h>
#include <iostream>
#include <utility>

#define PYTHON_DECLARE_OBJECT_CONVERTERS(clazz, base) \
    clazz(clazz &&) = default; \
    clazz(clazz const &) = default; \
    clazz(PyObject *o) : base(o) {} \
    clazz(base &&o) : base(std::forward<base>(o)) {} \
    clazz(base const &o) : base(o) {} \
    clazz& operator = (clazz &&) = default; \
    clazz& operator = (clazz const &) = default;

namespace python {

class moddef;
class object;

namespace detail {

enum class object_type
{
    CLASS,
    MODULE,
    NATIVE,
    GENERIC
};

template<typename T, typename Enabler = void>
struct native_base;

template<typename T>
auto get_object_type(T *) -> std::integral_constant<object_type, T::__type__>;

template<typename T>
auto get_object_type(T **) -> std::integral_constant<object_type, object_type::NATIVE>;

template<typename...>
std::integral_constant<object_type, object_type::GENERIC> get_object_type(...);

template<typename T>
constexpr object_type object_type_t = decltype(get_object_type((T*)nullptr))::value;

template<typename A, object_type T = object_type_t<A>>
struct attribute;

template<typename A, object_type T = object_type_t<A>>
struct specific;

template<typename T, bool, bool, bool>
struct argument;

template<typename T>
struct getattr_type { using type = specific<T>; };

template<>
struct getattr_type<object> { using type = object; };

template<typename A, object_type T>
struct getattr_type<specific<A, T>> { using type = specific<A,T>; };

template<typename T>
using getattr_type_t = typename getattr_type<T>::type;

template<typename T, typename R> R cast(object &&);
template<typename T, typename R> R cast(object const &);

template<typename T, bool I>
class attribute_proxy;

} // namespace detail

class object
{
public:
    object(PyObject *o = nullptr) : object_(o) {}

    object(object &&other) : object_(other.object_)
    {
        other.object_ = nullptr;
    }

    object(object const &other) : object_(other.object_)
    {
        if(object_) Py_INCREF(object_);
    }

    object& operator = (std::nullptr_t)
    {
        if(object_) Py_DECREF(object_);
        object_ = nullptr;
        return *this;
    }

    object& operator = (object &&other)
    {
        if(object_) Py_DECREF(object_);
        object_ = other.object_;
        other.object_ = nullptr;
        return *this;
    }

    object& operator = (object const &other)
    {
        if(object_) Py_DECREF(object_);
        object_ = other.object_;
        if(object_) Py_INCREF(object_);
        return *this;
    }

    bool operator ! () const
    {
        return !static_cast<bool>(object_);
    }

    ~object()
    {
        if(object_) Py_DECREF(object_);
    }

    PyObject *raw() const
    {
        return object_;
    }

    PyObject *release()
    {
        return std::exchange(object_, nullptr);
    }

    template<typename T>
    static object from(T &&);

private:
    friend bool hasattr(object const &, char const *);
    friend std::ostream &operator << (std::ostream &stream, object const &o);
    template<typename A> friend detail::getattr_type_t<A> getattr(object const &, char const *);
    template<typename A, detail::object_type> friend class detail::attribute;
    template<typename A, detail::object_type> friend class detail::specific;
    template<typename T, bool, bool, bool> friend class detail::argument;
    template<typename T, typename R> friend R detail::cast(object &&);
    template<typename T, typename R> friend R detail::cast(object const &);
    template<typename T, typename E> friend class detail::native_base;
    template<typename T, bool I> friend class detail::attribute_proxy;
    friend class moddef;

protected:
    PyObject *object_;
};

template<typename A>
detail::getattr_type_t<A> getattr(object const &o, char const *name)
{
    PyObject *a = PyObject_GetAttrString(o.object_, name);
    if(!a) throw exception();
    return a;
}

bool hasattr(object const &, char const *);
std::ostream &operator << (std::ostream &, object const &);

} // namespace python

