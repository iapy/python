#pragma once
#include <Python.h>

namespace python {

template<typename> class iter;

namespace detail {

enum class object_type;
template<typename, object_type> struct specific;

template<typename T, bool I = std::is_integral_v<T>, bool F = std::is_same_v<T, double> || std::is_same_v<T, float>, bool B = std::is_same_v<T, bool>>
struct result
{
    static specific<T> convert(PyObject *p)
    {
        return specific<T>(p);
    }
    static specific<T> ref(PyObject *p)
    {
        Py_INCREF(p);
        return specific<T>(p);
    }
};

template<>
struct result<object, false, false, false>
{
    static object convert(PyObject *p)
    {
        return object(p);
    }
    static object ref(PyObject *p)
    {
        Py_INCREF(p);
        return object(p);
    }
};


template<typename T>
struct result<iter<T>, false, false, false>
{
    static iter<T> convert(PyObject *p)
    {
        return iter<T>(p);
    }
    static iter<T> ref(PyObject *p)
    {
        Py_INCREF(p);
        return iter<T>(p);
    }
};

template<typename T, object_type O>
struct result<specific<T, O>, false, false, false>
{
    static specific<T, O> convert(PyObject *p)
    {
        return specific<T>(p);
    }
    static specific<T, O> ref(PyObject *p)
    {
        Py_INCREF(p);
        return specific<T, O>(p);
    }
};

template<typename T>
struct result<T, true, false, false>
{
    static T convert(PyObject *p)
    {
        T result = ::PyLong_AsLong(p);
        Py_DECREF(p);
        return result;
    }
    static T ref(PyObject *p)
    {
        return ::PyLong_AsLong(p);
    }
};

template<typename T>
struct result<T, false, true, false>
{
    static T convert(PyObject *p)
    {
        T result = ::PyFloat_AsDouble(p);
        Py_DECREF(p);
        return result;
    }
    static T ref(PyObject *p)
    {
        return ::PyFloat_AsDouble(p);
    }
};

template<typename T>
struct result<T, true, false, true>
{
    static T convert(PyObject *p)
    {
        return p == Py_True;
    }
    static T ref(PyObject *p)
    {
        return p == Py_True;
    }
};

} // namespace detail
} // namespace python
