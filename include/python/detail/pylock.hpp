#pragma once
#include <Python.h>

namespace python::detail {

class Lock
{
public:
    Lock();
    ~Lock();

    static void done();
    static void init();
private:
    PyGILState_STATE gstate;
};

} // namespace python::detail
