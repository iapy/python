#pragma once
#include <cg/config.hpp>
#include <optional>
#include <string>

namespace python::detail::metacg {

std::string align_code(const char *code);

class State
{
protected:
    State(nlohmann::json const &);
    State(State &&) = default;
    State(State const &) = default;
    ~State();

public:
    bool initialized{false};
};

} // namespace python::detail::metacg
