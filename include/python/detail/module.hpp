#pragma once
#include <python/detail/object.hpp>
#include <python/detail/result.hpp>
#include <python/types/optional.hpp>
#include <boost/preprocessor.hpp>
#include <boost/hana.hpp>
#include <filesystem>
#include <optional>
#include <utility>

namespace python {
namespace detail {

#define PYTHON_MODULE_HEADER_CHECK_METHOD(name) \
    template<typename Self, typename Other> \
    auto has##name##impl(Self *ptr) -> decltype(std::declval<Self>().name(std::declval<Other const &>()), std::true_type{}); \
    template<typename Self, typename Other> \
    std::false_type has##name##impl(...); \
    template<typename Self, typename Other> \
    constexpr bool has##name = decltype(has##name##impl<Self, Other>(nullptr))::value;

PYTHON_MODULE_HEADER_CHECK_METHOD(__eq__);
PYTHON_MODULE_HEADER_CHECK_METHOD(__ne__);
PYTHON_MODULE_HEADER_CHECK_METHOD(__lt__);
PYTHON_MODULE_HEADER_CHECK_METHOD(__le__);
PYTHON_MODULE_HEADER_CHECK_METHOD(__gt__);
PYTHON_MODULE_HEADER_CHECK_METHOD(__ge__);

#undef PYTHON_MODULE_HEADER_CHECK_METHOD

template<typename ...T> struct module_info : T... {};

template<typename B, typename T>
struct check_optional_attribute : std::is_same<specific<std::optional<T>>, B> {};

template<typename B>
struct check_optional_attribute<B, std::nullopt_t> : std::true_type {};

template<typename T, object_type O>
class attribute
{
public:
    attribute(std::pair<object *, const char *> &&definition) {}
};

template<typename R, typename ...As>
class attribute<R(As...), object_type::GENERIC> : public specific<R(As...), object_type::GENERIC>
{
public:
    attribute(std::pair<object *, const char *> &&definition)
    : specific<R(As...), object_type::GENERIC>(
        (Py_None == definition.first->object_) ? specific<R(As...), object_type::GENERIC>(nullptr) : getattr<R(As...)>(*definition.first, definition.second)
    ) {}

    using specific<R(As...), object_type::GENERIC>::operator ();
};

template<char... Name>
struct attribute_name
{
    static constexpr char value[] = {Name..., 0};
};

template<typename Base, bool I = std::is_integral_v<Base>>
class attribute_proxy : public Base
{
public:
    attribute_proxy(object const &o, const char *name)
    : Base(getattr<Base>(o, name)), owner(o), name{name}
    {
    }

    attribute_proxy<Base, I> &operator = (Base const &value)
    {
        Py_XINCREF(value.object_);
        PyObject_SetAttrString(owner.object_, name, value.object_);
        return *this;
    }

    template<typename T, std::enable_if_t<is_python_optional<Base>::value && check_optional_attribute<Base, std::decay_t<T>>::value, int> = 0>
    auto operator = (T &&value)
    {
        if constexpr (std::is_same_v<T, std::nullptr_t>)
        {
            PyObject_SetAttrString(owner.object_, name, Py_None);
        }
        else
        {
            auto v = object::from(value);
            Py_XINCREF(v.object_);
            PyObject_SetAttrString(owner.object_, name, v.object_);
        }
        return *this;
    }

private:
    object owner;
    const char *name{nullptr};
};

template<typename Base>
class attribute_proxy<Base, true>
{
public:
    attribute_proxy(object const &o, const char *name)
    : owner(o), name{name}
    {
    }

    operator Base () const
    {
        return result<Base>::convert(PyObject_GetAttrString(owner.object_, name));
    }

    auto operator = (Base const &value)
    {
        auto v = object::from(value);
        Py_XINCREF(v.object_);
        PyObject_SetAttrString(owner.object_, name, v.object_);
        return *this;
    }

    auto operator += (Base const other)
    {
        return *this = (static_cast<Base>(*this) + other);
    }

    auto operator -= (Base const other)
    {
        return *this = (static_cast<Base>(*this) - other);
    }

private:
    object owner;
    const char *name{nullptr};
};

} // namespace detail

template<typename Impl>
class detail::specific<Impl, detail::object_type::MODULE> : public Impl
{
public:
    PYTHON_DECLARE_OBJECT_CONVERTERS(specific, Impl)

    std::string __name__() const
    {
        return static_cast<std::string>(python::getattr<std::string>(*this, "__name__"));
    }
    std::filesystem::path __file__() const
    {
        return static_cast<std::string_view>(python::getattr<std::string>(*this, "__file__"));
    }
};

template<typename Impl>
class detail::specific<Impl, detail::object_type::CLASS> : public Impl
{
public:
    PYTHON_DECLARE_OBJECT_CONVERTERS(specific, Impl)

    template<typename T, std::enable_if_t<detail::has__eq__<Impl, T>, int> = 0>
    bool operator == (T const &other) const { return Impl::__eq__(other); }

    template<typename T, std::enable_if_t<!detail::has__eq__<Impl, T> && detail::has__ne__<Impl, T>, int> = 0>
    bool operator == (T const &other) const { return !Impl::__ne__(other); }

    template<typename T, std::enable_if_t<detail::has__ne__<Impl, T>, int> = 0>
    bool operator != (T const &other) const { return Impl::__ne__(other); }

    template<typename T, std::enable_if_t<!detail::has__ne__<Impl, T> && detail::has__eq__<Impl, T>, int> = 0>
    bool operator != (T const &other) const { return !Impl::__eq__(other); }

    template<typename T, std::enable_if_t<detail::has__lt__<Impl, T>, int> = 0>
    bool operator < (T const &other) const { return Impl::__lt__(other); }

    template<typename T, std::enable_if_t<!detail::has__lt__<Impl, T> && detail::has__ge__<Impl, T>, int> = 0>
    bool operator < (T const &other) const { return !Impl::__ge__(other); }

    template<typename T, std::enable_if_t<detail::has__le__<Impl, T>, int> = 0>
    bool operator <= (T const &other) const { return Impl::__le__(other); }

    template<typename T, std::enable_if_t<!detail::has__le__<Impl, T> && detail::has__gt__<Impl, T>, int> = 0>
    bool operator <= (T const &other) const { return !Impl::__gt__(other); }

    template<typename T, std::enable_if_t<detail::has__gt__<Impl, T>, int> = 0>
    bool operator > (T const &other) const { return Impl::__gt__(other); }

    template<typename T, std::enable_if_t<!detail::has__gt__<Impl, T> && detail::has__le__<Impl, T>, int> = 0>
    bool operator > (T const &other) const { return !Impl::__le__(other); }

    template<typename T, std::enable_if_t<detail::has__ge__<Impl, T>, int> = 0>
    bool operator >= (T const &other) const { return Impl::__ge__(other); }

    template<typename T, std::enable_if_t<!detail::has__ge__<Impl, T> && detail::has__lt__<Impl, T>, int> = 0>
    bool operator >= (T const &other) const { return !Impl::__lt__(other); }

    inline detail::attribute_proxy<object> operator [] (const char *name) const
    {
        Py_XINCREF(Impl::object_);
        return {Impl::object_, name};
    }

    template<char... Name>
    inline detail::attribute_proxy<
        decltype(Impl::python_attribute_type(std::declval<detail::attribute_name<Name...>>()))
    >operator [] (detail::attribute_name<Name...> n) const
    {
        Py_XINCREF(Impl::object_);
        return {Impl::object_, n.value};
    }

    template<char... Name>
    auto at(detail::attribute_name<Name...> n) const
    {
        return (*this)[n];
    }

    friend std::ostream& operator << (std::ostream &stream, detail::specific<Impl, detail::object_type::CLASS> const &value)
    {
        return stream << static_cast<object const &>(value);
    }
};

template<typename Impl>
using class_ = detail::specific<Impl, detail::object_type::CLASS>;

} // namespace python

#define PYTHON_MODULE_HEADER_DETAIL_UNPACK(...) __VA_ARGS__
#define PYTHON_MODULE_HEADER_DETAIL_APPLY(M,...) M(__VA_ARGS__)
#define PYTHON_MODULE_HEADER_DETAIL_REVERSE(...) BOOST_PP_SEQ_ENUM(BOOST_PP_SEQ_REVERSE(BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__)))

#define PYTHON_MODULE_HEADER_DETAIL_ATTRIBUTES_2(name, ...) \
    static decltype(python::detail::result<PYTHON_MODULE_HEADER_DETAIL_REVERSE(__VA_ARGS__)>::convert(nullptr)) python_attribute_type(decltype(#name##_python_attribute_name)); \
    python::detail::attribute<PYTHON_MODULE_HEADER_DETAIL_REVERSE(__VA_ARGS__)> name = std::make_pair((python::object*)this, #name);

#define PYTHON_MODULE_HEADER_DETAIL_ATTRIBUTES_1(N, _, ...) PYTHON_MODULE_HEADER_DETAIL_APPLY(PYTHON_MODULE_HEADER_DETAIL_ATTRIBUTES_2, PYTHON_MODULE_HEADER_DETAIL_REVERSE __VA_ARGS__)

#define PYTHON_MODULE_HEADER_2(name, functions) class name : public python::object { \
    public: \
        BOOST_PP_SEQ_FOR_EACH(PYTHON_MODULE_HEADER_DETAIL_ATTRIBUTES_1, _, BOOST_PP_VARIADIC_TO_SEQ functions) \
        static constexpr python::detail::object_type __type__ = python::detail::object_type::MODULE; \
    protected: \
        PYTHON_DECLARE_OBJECT_CONVERTERS(name, object) \
    }

#define PYTHON_MODULE_HEADER_DETAIL_ATTRIBUTES_11(...) BOOST_PP_SEQ_FOR_EACH(PYTHON_MODULE_HEADER_DETAIL_ATTRIBUTES_1, _, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))
#define PYTHON_MODULE_HEADER_DETAIL_ATTRIBUTES_10(...)

#define PYTHON_CLASS_1(name) (class name : public python::object { \
    public: \
        static void python_attribute_type(...); \
        static constexpr python::detail::object_type __type__ = python::detail::object_type::CLASS; \
    protected: \
        PYTHON_DECLARE_OBJECT_CONVERTERS(name, object) \
    }, name)

#define PYTHON_CLASS_2(name, methods) (class name : public python::object { \
    public: \
        BOOST_PP_SEQ_FOR_EACH(PYTHON_MODULE_HEADER_DETAIL_ATTRIBUTES_1, _, BOOST_PP_VARIADIC_TO_SEQ methods) \
        static constexpr python::detail::object_type __type__ = python::detail::object_type::CLASS; \
    protected: \
        PYTHON_DECLARE_OBJECT_CONVERTERS(name, object) \
    }, name)

#define PYTHON_CLASS(...) \
    BOOST_PP_OVERLOAD(PYTHON_CLASS_, __VA_ARGS__)(__VA_ARGS__)

#define PYTHON_MODULE_HEADER_DETAIL_CLASSES_2(I, declaration) \
    BOOST_PP_TUPLE_ELEM(2, 0, declaration);    \
    struct _python_module_imported_class_##I { \
        using BOOST_PP_TUPLE_ELEM(2, 1, declaration) = python::detail::specific<_python_module_definition::BOOST_PP_TUPLE_ELEM(2, 1, declaration), python::detail::object_type::CLASS>; \
    };

#define PYTHON_MODULE_HEADER_DETAIL_CLASSES_1(N, D, I, ...) PYTHON_MODULE_HEADER_DETAIL_APPLY(PYTHON_MODULE_HEADER_DETAIL_CLASSES_2, I, __VA_ARGS__)
#define PYTHON_MODULE_HEADER_DETAIL_CLASSES_0(N, D, I, ...) BOOST_PP_COMMA_IF(I) _python_module_imported_class_##I

#define PYTHON_MODULE_HEADER_3(name, classes, functions) class name : public python::object { \
        using _python_module_definition = name; \
    public: \
        BOOST_PP_SEQ_FOR_EACH_I(PYTHON_MODULE_HEADER_DETAIL_CLASSES_1, _, BOOST_PP_VARIADIC_TO_SEQ classes) \
        BOOST_PP_SEQ_FOR_EACH(PYTHON_MODULE_HEADER_DETAIL_ATTRIBUTES_1, _, BOOST_PP_VARIADIC_TO_SEQ functions) \
        using __classes__ = python::detail::module_info< \
            BOOST_PP_SEQ_FOR_EACH_I(PYTHON_MODULE_HEADER_DETAIL_CLASSES_0, _, BOOST_PP_VARIADIC_TO_SEQ classes) \
        >; \
        static constexpr python::detail::object_type __type__ = python::detail::object_type::MODULE; \
    protected: \
        PYTHON_DECLARE_OBJECT_CONVERTERS(name, object) \
    }

#define PYTHON_MODULE_HEADER(...) \
    BOOST_PP_OVERLOAD(PYTHON_MODULE_HEADER_, __VA_ARGS__)(__VA_ARGS__)

template<typename ch, ch ...values>
constexpr auto operator ""_python_attribute_name ()
{
    return python::detail::attribute_name<values...>{};
}
