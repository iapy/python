#pragma once
#include <python/detail/moddef.hpp>
#include <python/detail/pyargs.hpp>
#include <python/types/tuple.hpp>
#include <python/common.hpp>
#include <python/tags.hpp>
#include <ct/tuple.hpp>

#define PYTHON_IMPORT(name, ...) \
    PYTHON_MODULE_HEADER(python_imported_module, __VA_ARGS__); \
    static constexpr char python_imported_module_name[] = #name;

#define PYTHON_DEFINE(name, code) \
    static constexpr char python_defined_module_name[] = #name; \
    static constexpr char python_defined_module_code[] = code;

#define PYTHON_EXPORT(name) \
    static constexpr char python_exported_module_name[] = #name;

namespace python {
namespace detail {

struct argument_error {};

template<typename O>
auto is_module(O *) -> decltype(std::declval<O>().init(std::declval<python::moddef &>()), std::true_type{});

template<typename O>
std::false_type is_module(...);

template<typename O>
constexpr bool is_module_v = decltype(is_module<O>(nullptr))::value;

template<bool M, typename ...Ts>
struct shift_tuple
{
    using type = std::tuple<Ts...>;
};

template<typename T, typename ...Ts>
struct shift_tuple<true, T, Ts...>
{
    using type = std::tuple<Ts...>;
};

template<typename O, typename R, typename ...Args>
class _InvokerBase
{
private:
    template<std::size_t I, typename Tuple>
    static auto get(Tuple const &tuple, Py_ssize_t size)
        -> decltype(python::get<I>(tuple))
    {
        using T = decltype(python::get<I>(tuple));
        if(I < size)
        {
            return python::get<I>(tuple);
        }
        else if constexpr(is_python_optional<T>::value)
        {
            return T{nullptr};
        }
        else
        {
            throw argument_error{};
        }
    }

    template<typename F, std::size_t ...I>
    static auto invoke(F &&fnptr, std::conditional_t<is_module_v<O>, O, O*> p, specific<std::tuple<std::remove_reference_t<Args>...>> const &tuple, Py_ssize_t size, std::index_sequence<I...>)
    {
        return std::invoke(fnptr, p, get<I>(tuple, size)...);
    }

    template<typename F, typename Tuple, std::size_t ...I>
    static auto invoke(F &&fnptr, std::conditional_t<is_module_v<O>, O, O*> p, PyObject *self, Tuple const &tuple, Py_ssize_t size, std::index_sequence<I...>)
    {
        return std::invoke(fnptr, p, result<std::tuple_element_t<0, std::tuple<Args...>>>::ref(self), get<I>(tuple, size)...);
    }

    template<typename IsMethod>
    static auto get(PyObject *self, IsMethod) -> std::conditional_t<is_module_v<O>, O, O*>
    {
        if constexpr (IsMethod::value)
        {
            static_assert(is_module_v<O>);
            return get(PyImport_GetModule(PyObject_GetAttrString(self, "__module__")), std::false_type{});
        }
        else if constexpr(is_module_v<O>)
        {
            static_assert(sizeof(O) == sizeof(std::array<void*,2>));
            std::array<void*,2> interface = {
                PyLong_AsVoidPtr(PyObject_GetAttrString(self, "void.pointer[0]")),
                PyLong_AsVoidPtr(PyObject_GetAttrString(self, "void.pointer[1]"))
            };
            return *reinterpret_cast<O*>(&interface);
        }
        else
        {
            return reinterpret_cast<native<O>*>(self)->instance;
        }
    }

protected:
    template<typename F>
    static PyObject *invoke(F &&fnptr, PyObject *self, PyObject *args, PyObject *)
    {
        try {
            if constexpr (sizeof ...(Args))
            {
                Py_INCREF(args);

                using IsMethod = std::is_pointer<std::tuple_element_t<0, std::tuple<Args...>>>;
                specific<typename shift_tuple<IsMethod::value, std::remove_reference_t<Args>...>::type> tuple(args);

                if constexpr (std::is_same_v<R, void>)
                {
                    if constexpr (IsMethod::value)
                        invoke(std::forward<F>(fnptr), get(self, IsMethod{}), self, tuple, PyTuple_Size(args), std::make_index_sequence<sizeof ...(Args) - 1>());
                    else
                        invoke(std::forward<F>(fnptr), get(self, IsMethod{}), tuple, PyTuple_Size(args), std::make_index_sequence<sizeof ...(Args)>());
                    Py_INCREF(Py_None);
                    return Py_None;
                }
                else
                {
                    if constexpr (IsMethod::value)
                        return argument<R>::get(invoke(std::forward<F>(fnptr), get(self, IsMethod{}), self, tuple, PyTuple_Size(args), std::make_index_sequence<sizeof ...(Args) - 1>()));
                    else
                        return argument<R>::get(invoke(std::forward<F>(fnptr), get(self, IsMethod{}), tuple, PyTuple_Size(args), std::make_index_sequence<sizeof ...(Args)>()));
                }
            }
            else
            {
                if constexpr (std::is_same_v<R, void>)
                {
                    std::invoke(fnptr, get(self, std::false_type{}));
                    Py_INCREF(Py_None);
                    return Py_None;
                }
                else
                {
                    return argument<R>::get(std::invoke(fnptr, get(self, std::false_type{})));
                }
            }
        } catch(argument_error const &e) {
            PyErr_SetString(PyExc_TypeError, "Insufficient number of arguments");
            return nullptr;
        } catch(std::exception const &e) {
            PyErr_SetString(PyExc_SystemError, e.what());
            return nullptr;
        } catch(...) {
            PyErr_SetString(PyExc_SystemError, "Internal error");
            return nullptr;
        }
    }
};

template<auto P, bool F>
class _Invoker;

template<typename R, typename O, R (O::*fnptr)() const>
struct _Invoker<fnptr, true> : private _InvokerBase<O, R>
{
    static PyObject *invoke(PyObject *self)
    {
        return _InvokerBase<O, R>::invoke(fnptr, self, NULL, NULL);
    }
    using type = decltype(&invoke);
};

template<typename R, typename O, R (O::*fnptr)()>
struct _Invoker<fnptr, true> : private _InvokerBase<O, R>
{
    static PyObject *invoke(PyObject *self)
    {
        return _InvokerBase<O, R>::invoke(fnptr, self, NULL, NULL);
    }
    using type = decltype(&invoke);
};

template<typename R, typename O, typename A, typename ...Args, R (O::*fnptr)(A, Args...)>
struct _Invoker<fnptr, true> : private _InvokerBase<O, R, A, Args...>
{
    static PyObject *invoke(PyObject *self, PyObject *args, PyObject *kwargs)
    {
        return _InvokerBase<O, R, A, Args...>::invoke(fnptr, self, args, kwargs);
    }
    using type = decltype(&invoke);
};

template<typename R, typename O, typename A, typename ...Args, R (O::*fnptr)(A, Args...) const>
struct _Invoker<fnptr, true> : private _InvokerBase<O, R, A, Args...>
{
    static PyObject *invoke(PyObject *self, PyObject *args, PyObject *kwargs)
    {
        return _InvokerBase<O, R, A, Args...>::invoke(fnptr, self, args, kwargs);
    }
    using type = decltype(&invoke);
};

template<typename R, typename O, R O::*memptr>
struct _Invoker<memptr, false>
{
    static PyObject *invoke(O *self)
    {
        return argument<R>::get(self->*memptr);
    }
    using type = decltype(&invoke);
};

template<typename T>
auto moduledef_exists(T *) -> decltype(std::declval<typename T::Module*>(), std::true_type{});

template<typename T>
std::false_type moduledef_exists(...);

template<typename T>
auto imported_module_exists(T *) -> decltype(std::declval<typename T::python_imported_module::__classes__*>(), std::true_type{});

template<typename T>
std::false_type imported_module_exists(...);

template<typename Base, bool Imports = decltype(imported_module_exists<Base>(std::declval<Base*>()))::value>
struct MaybeImport : Base
{
};

template<typename Base>
struct MaybeImport<Base, true> : Base
{
    using Module = typename Base::python_imported_module::__classes__;
};

template<typename Component, bool Implements = decltype(moduledef_exists<Component>(std::declval<Component*>()))::value>
struct Module
{
    template<typename Base>
    struct Interface : Base
    {
        void init(python::moddef &, std::true_type) {}
    };
};

template<typename Component>
struct Module<Component, true>
{
    template<typename Base>
    struct Interface : Component::Module::Interface<MaybeImport<Base>>
    {
        void init(python::moddef &def, std::true_type)
        {
            static_assert(sizeof(*this) == sizeof(std::array<void*,2>));
            std::array<void*,2> interface = *reinterpret_cast<std::array<void*,2>*>(this);
            PyObject_SetAttrString(def.raw(), "void.pointer[0]", PyLong_FromVoidPtr(interface[0]));
            PyObject_SetAttrString(def.raw(), "void.pointer[1]", PyLong_FromVoidPtr(interface[1]));
            static_cast<typename Component::Module::Interface<MaybeImport<Base>>*>(this)->init(def);
        }
    };
};

} // namespace detail

template<auto F>
constexpr typename detail::_Invoker<F, std::is_member_function_pointer_v<decltype(F)>>::type bind = detail::_Invoker<F, std::is_member_function_pointer_v<decltype(F)>>::invoke;

template<typename I>
auto import(I const *interface)
{
    return python::lock([interface]{
        return python::cast<typename I::python_imported_module>(python::import(I::python_imported_module_name));
    });
}

template<typename Component>
using module = ct::pair<tag::Module, detail::Module<Component>>;

} // namespace python
