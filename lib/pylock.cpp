#include <python/detail/pylock.hpp>
#include <iostream>
#include <thread>
#include <mutex>

namespace python::detail {

#ifndef PYTHON_DISABLE_GIL_MANAGEMENT

class ThreadState
{
public:
    ThreadState() : main{nullptr == mains}
    {
        if(!main)
        {
            state = PyThreadState_New(mains->interp);
        }
        else
        {
            state = nullptr;
        }
    }

    ~ThreadState()
    {
        if(!main)
        {
            PyEval_AcquireThread(state);
            PyThreadState_Clear(state);
            PyEval_ReleaseThread(state);
            PyThreadState_Delete(state);
        }
    }

    void ensure()
    {
        if(1 == ++count)
        {
            PyEval_AcquireThread(state);
        }
    }

    void release()
    {
        if(0 == --count)
        {
            PyEval_ReleaseThread(state);
        }
    }

    void init()
    {
        count = 1;
        state = mains = PyGILState_GetThisThreadState();
    }

private:
    int count{0};
    bool const main;
    PyThreadState *state;
    static PyThreadState *mains;
};

thread_local ThreadState thread_state;
PyThreadState *ThreadState::mains{nullptr};

Lock::Lock()
{
    thread_state.ensure();
}

void Lock::done()
{
    thread_state.ensure();
}

void Lock::init()
{
    thread_state.init();
    thread_state.release();
}

Lock::~Lock()
{
    thread_state.release();
}

#else

Lock::Lock() {}

void Lock::done() {}

void Lock::init() {}

Lock::~Lock() {}

#endif

} // namespace python::detail
