#include <python/detail/import.hpp>
#include <Python.h>

python::object python::import(const char *name)
{
    PyObject *m = PyImport_ImportModule(name);
    if(!m) throw exception();
    return object{m};
}
