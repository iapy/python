#include <python/detail/config.hpp>
#include <python/detail/metacg.hpp>
#include <python/detail/pylock.hpp>
#include <Python.h>
#include <mutex>

namespace python::detail {

metacg::State::State(nlohmann::json const &json)
{
    if(json.contains("path"))
    {
        if(auto path = jsonio::from<std::string>(json["path"]); !path.empty())
        {
            config.path(path.c_str());
        }
    }

    if(json.contains("byte"))
    {
        config.byte(jsonio::from<bool>(json["byte"]));
    }

    Py_InitializeFromConfig(config);
}

metacg::State::~State()
{
    if(initialized)
    {
        Py_FinalizeEx();
    }
}

std::string metacg::align_code(const char *code)
{
    std::ostringstream ss;
    char last = '\n';
    for(; *code; ++code)
    {
        if(std::isspace(*code) && last == '\n') continue;
        ss.put(last = *code);
    }
    return ss.str();
}

} // namespace python::detail
