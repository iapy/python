#include <python/common.hpp>

namespace python {

std::ostream &operator << (std::ostream &stream, python::object const &o)
{
    return stream << "python::object[0x" << std::hex << reinterpret_cast<std::uint64_t>(o.object_) << ']';
}

std::ostream &operator << (std::ostream &stream, python::str const &o)
{
    return stream << static_cast<std::string_view>(o);
}

bool hasattr(object const &o, const char *name)
{
    return PyObject_HasAttrString(o.object_, name);
}

} // namespace python

namespace python::detail {

static_assert(is_same_specific<std::string, str>::value);
static_assert(is_same_specific<str, std::string>::value);

static_assert(!is_same_specific<int, str>::value);
static_assert(!is_same_specific<str, int>::value);

static_assert(is_same_specific<list<str>, std::vector<std::string>>::value);
static_assert(is_same_specific<std::vector<std::string>, list<str>>::value);

static_assert(!is_same_specific<set<str>, std::vector<std::string>>::value);
static_assert(!is_same_specific<list<str>, std::set<std::string>>::value);

static_assert(!is_same_specific<list<int>, std::vector<std::string>>::value);
static_assert(!is_same_specific<std::vector<std::string>, list<int>>::value);

static_assert(!is_same_specific<list<int>, std::vector<std::string>>::value);
static_assert(!is_same_specific<std::vector<std::string>, list<int>>::value);

static_assert(is_same_specific<list<list<int>>, std::vector<std::vector<int>>>::value);
static_assert(is_same_specific<std::vector<std::vector<int>>, list<list<int>>>::value);

static_assert(!is_same_specific<list<set<int>>, std::vector<std::vector<int>>>::value);
static_assert(!is_same_specific<std::vector<std::vector<int>>, list<set<int>>>::value);

static_assert(!is_same_specific<list<list<str>>, std::vector<std::vector<int>>>::value);
static_assert(!is_same_specific<std::vector<std::vector<std::string>>, list<list<int>>>::value);

static_assert(is_same_specific<list<set<str>>, std::vector<std::set<std::string>>>::value);
static_assert(is_same_specific<std::vector<std::set<std::string>>, list<set<str>>>::value);

static_assert(is_same_specific<list<std::set<std::string>>, std::vector<std::set<std::string>>>::value);
static_assert(is_same_specific<list<std::set<std::string>>, list<set<str>>>::value);
static_assert(is_same_specific<std::vector<std::set<std::string>>, list<std::set<std::string>>>::value);
static_assert(is_same_specific<list<set<str>>, list<std::set<std::string>>>::value);

static_assert(is_same_specific<tuple<int, str>, tuple<int, std::string>>::value);
static_assert(is_same_specific<tuple<int, str>, std::tuple<int, std::string>>::value);
static_assert(is_same_specific<tuple<int, str, list<str>>, tuple<int, std::string, std::vector<std::string>>>::value);
static_assert(!is_same_specific<tuple<int, str, list<str>>, tuple<int, std::string, std::set<std::string>>>::value);

static_assert(is_same_specific<tuple<int, dict<int, str>>, tuple<int, std::map<int, std::string>>>::value);
static_assert(!is_same_specific<tuple<int, dict<int, int>>, tuple<int, std::map<int, std::string>>>::value);

static_assert(is_same_specific<tuple<int, dict<int, str>>, std::tuple<int, std::map<int, std::string>>>::value);
static_assert(!is_same_specific<tuple<int, dict<int, int>>, std::tuple<int, std::map<int, std::string>>>::value);

static_assert(is_same_specific<std::tuple<int, dict<int, str>>, tuple<int, std::map<int, std::string>>>::value);
static_assert(!is_same_specific<std::tuple<int, dict<int, int>>, tuple<int, std::map<int, std::string>>>::value);

static_assert(is_same_specific<std::optional<int>, std::nullopt_t>::value);
static_assert(is_same_specific<std::optional<int>, python::optional<int>>::value);
static_assert(!is_same_specific<std::optional<int>, python::optional<python::str>>::value);

} // namespace python::detail
