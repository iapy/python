#include <python/detail/config.hpp>
#include <cstdlib>
#include <mutex>

python::detail::Config::operator PyConfig * () const
{
    static PyConfig conf;
    static std::once_flag once;
    std::call_once(once, []{
        PyConfig_InitPythonConfig(&conf);
        std::atexit([]{
            PyConfig_Clear(&conf);
        });
    });
    return &conf;
}

python::detail::Config &python::detail::Config::path(char const *p)
{
    PyConfig *c = static_cast<PyConfig*>(*this);
    PyConfig_SetBytesString(c, &c->pythonpath_env, p);
    return *this;
}

python::detail::Config &python::detail::Config::byte(bool const v)
{
    PyConfig *c = static_cast<PyConfig*>(*this);
    c->write_bytecode = static_cast<int>(v);
    return *this;
}

python::detail::Config python::config;
