#include <python/detail/except.hpp>
#include <python/detail/pycast.hpp>
#include <python/types/func.hpp>
#include <Python.h>

python::exception::exception()
{
    PyErr_Fetch(&e, &v, &t);
}

python::exception::exception(python::exception &&other)
: e(other.e), v(other.v), t(other.t)
{
    other.e = nullptr;
    other.v = nullptr;
    other.t = nullptr;
}

python::exception::exception(python::exception const &other)
: e(other.e), v(other.v), t(other.t)
{
    Py_XINCREF(e);
    Py_XINCREF(v);
    Py_XINCREF(t);
}

python::exception &python::exception::operator = (python::exception &&other)
{
    std::swap(e, other.e);
    std::swap(v, other.v);
    std::swap(t, other.t);
    return *this;
}

python::exception &python::exception::operator = (python::exception const &other)
{
    e = other.e;
    v = other.v;
    t = other.t;
    Py_XINCREF(e);
    Py_XINCREF(v);
    Py_XINCREF(t);
    return *this;
}

std::string python::exception::type() const
{
    PyObject* c_utf = PyObject_GetAttrString(e, "__name__");
    PyObject* c_str = PyUnicode_AsEncodedString(c_utf, "utf-8", "~E~"); 

    std::string result = PyBytes_AS_STRING(c_str);

    Py_XDECREF(c_str);
    Py_XDECREF(c_utf);

    return result;
}

std::string python::exception::what() const
{
    PyObject* rpr = PyObject_Repr(v);
    PyObject* str = PyUnicode_AsEncodedString(rpr, "utf-8", "~E~");

    std::string result = PyBytes_AS_STRING(str);

    Py_XDECREF(str);
    Py_XDECREF(rpr);

    return result;
}

python::exception::exception(python::exception const &other, object with) : python::exception::exception(other)
{
    Py_XINCREF(e);
    Py_XINCREF(v);
    Py_XINCREF(t);
    python::cast<void(object, object, object)>(with)(object(e), object(v), object(t));
}

python::exception::~exception()
{
    Py_XDECREF(e);
    Py_XDECREF(v);
    Py_XDECREF(t);
}
